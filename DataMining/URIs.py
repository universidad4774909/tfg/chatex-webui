
BASE_URI = "http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#"

#Classes
DEFINITION_URI = f"{BASE_URI}Definition"
THEOREM_URI = f"{BASE_URI}Theorem"
COROLLARY_URI = f"{BASE_URI}Corollary"
LEMMA_URI = f"{BASE_URI}Lemma"
SYMBOL_URI = f"{BASE_URI}Symbol"

# Object Properties
HAS_SYMBOL_OP_URI = f"{BASE_URI}hasSymbols"
REPRESENTS_OP_URI = f"{BASE_URI}represents"
HAS_NOTATION_OP_URI = f"{BASE_URI}hasNotation"
ASSUMES_THAT_OP_URI = f"{BASE_URI}assumesThat"
IMPLIES_THAT_OP_URI = f"{BASE_URI}impliesThat"


# Data Properties
HAS_LABEL_DP_URI = f"{BASE_URI}hasLabel"
LATEX_DP_URI = f"{BASE_URI}Latex"