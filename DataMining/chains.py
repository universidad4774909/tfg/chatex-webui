#Saltar Introducción, Índice.
#Identificar en que lineas se encuentra el statement completo
#Identificar el tipo de statement (Teorema, Axioma, Lema, Definición, Corolario).
#En función del tipo de enunciado cambiar el prompt e identificar sus partes básicas
#En función del tipo de enunciado comprobar hipótesis y conclusiones en KB ( Crear si no existe prueba como Lema )
#Con todas las referencias, insertar individuo en GraphDB, insertar latex en nucliadb con labels para encontrar al individuo
#Si es Teorema o Lema busca la prueba COMPLETA en las lineas siguientes
#Identifica cada paso dado en la prueba y separa las lineas de latex de cada una
#Clasifica cada paso de la prueba como (Induction, Deduction(Implicación lógica, como un enunciado anónimo),
# Reducción al absurdo, Reducción a otro problema (NP), Recursión finita, Por casos)
# Para cada tipo de paso extraemos la información relevante ( variable de inducción, contradicción que se obtiene, problema al que se reduce, etc...)
# Creamos cada uno de los individuos de MathematicalStep, Asociamos estos mediante la relacion nextStep
# Crear el individuo Prueba y asociarle el primer paso de la prueba y el enunciado que prueba
# Volver al 2 paso

from langchain_core.output_parsers import StrOutputParser
import sys
import importlib.util
#from LlamusWrapper.customLLMus import Llamus
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
#spec = importlib.util.spec_from_file_location("archivo", "../carpeta1/subcarpeta1/archivo.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
# Para invocar la rutina anteponemos el nombre del módulo
Llamus = customLLMus.Llamus
from dotenv import dotenv_values
from langchain_core.runnables import RunnableLambda
from operator import itemgetter
import re
from .promptTemplates import *
config = dotenv_values(".env")
API_KEY = config.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = config.get("LLAMUS_BASE_PATH")
from .generalisedPromptTemplates import promptIdentifyStatement, promptStatementHypothesisChecking, promptStatementConclusionsChecking, promptStatementHypothesisParsing, promptStatementConclusionParsing, promptIIF

llm = Llamus(
    n=10,
    api_key=API_KEY,
    endpoint=LLAMUS_BASE_PATH,
    model="llama3.1:8b",
)
llm2 = Llamus(
    n=10,
    api_key=API_KEY,
    endpoint=LLAMUS_BASE_PATH,
    model="llama3.1:70b",
)

chainIdentifyStatement = ( 
    promptIdentifyStatement
    | llm
    | StrOutputParser()
)

chainRewriteStatement = ( 
    promptRewriteStatement
    | llm
    | StrOutputParser()
)

chainCodeStatement = ( 
    promptCodeStatement
    | llm
    | StrOutputParser()
)

parseDefinitionStatementChain = ( 
    promptDefinitionSymbolParsing
    | llm2
    | SymbolListParser
)

parseDefinitionHypothesisChain = ( 
    promptDefinitionHypothesisParsing
    | llm2
    | MathematicalStatementListParser
)

parseStatementHypothesisChain = ( 
    promptStatementHypothesisParsing
    | llm2
    | MathematicalStatementListParser
)

checkStatementHypothesisChain = ( 
    promptStatementHypothesisChecking
    | llm2
    | StringListParser
)

checkStatementConclusionsChain = ( 
    promptStatementConclusionsChecking
    | llm2
    | StringListParser
)


#General

parseGeneralSymbols = ( 
    promptDefinitionSymbolParsing
    | llm2
    | SymbolListParser
)

parseStatementConclusionsChain = ( 
    promptStatementConclusionParsing
    | llm2
    | MathematicalStatementListParser
)

IdentifyIIFStatementsChain = ( 
    promptIIF
    | llm
    | StrOutputParser()
)



if __name__ == "__main__":
    from ontologyWrapper import RDFOntologyUtils
    import json

    rdf_file_path = "mutableOntology.rdf"

    ontology_utils = RDFOntologyUtils(rdf_file_path)

    from nucliaDB import VectorKB

    knowledge_base = VectorKB('localhost',8080)



    # Prueba definición completa:




    #Prueba símbolos definición
    classes_uri = ontology_utils.get_subclasses('http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Object')
    previousSymbols = []
    result = [{"a":"b"}]

    original_statement = "Si $x\in\mathlit{R}$, entonces $x\gt0$ o $x\eq0$ o $x\lt0$."

    preparsedStatement = chainRewriteStatement.invoke({
        "StatementType": "Definition",
        "prevText":"",
        "text": original_statement,
        "nextText": ""})
    if ("\n" in preparsedStatement):
        preparsedStatement = preparsedStatement.split("\n")[-1]

    while len(result)==0:
        try:
            result = parseDefinitionStatementChain.invoke({
                "query": preparsedStatement,
                "mathematicalObjects": json.dumps(classes_uri)
            })
        except Exception as e:
            import re
            print(e)
            regex = r"```json\\n(.+)```"
            if re.match(regex,e.llm_output):
                new_str = re.sub(regex,"\g<1>",e.llm_output)
                try:
                    result = json.loads(new_str)
                except:
                    print(e)
                    continue
            else:
                print(e)
                continue
        clases_context = [c.split("#")[-1] for c in classes_uri]

        if not all({symbol["represents"] in clases_context for symbol in result}):
            continue
        print(result)

    symbols = result



    def parseCustomType(parsedStatement: dict):
        if parsedStatement['type']=="Definition":
            return parseDefinitionStatementChain.invoke({'parsedStatement': parsedStatement['latex']})
        else:
            raise Exception("No valid Statement type")
    parseGeneralStatementChain = (
            {
                "parsedStatement": itemgetter("parsedStatement") | RunnableLambda(parseCustomType),
            }
            | StrOutputParser()
    )
    parseGeneralStatementChain.invoke({'parsedStatement':{
        'type': 'Definition',
        'latex': "If $P$ is a regular polygon, then all sides and angles of $P$ are congruent."
    }})


    print(chainCodeStatement.invoke({
        "StatementType": "Definition",
        "prevText":"Definición 9: Un número real es irracional si no puede expresarse como una fracción de dos números enteros. ",
        "text": "Por ejemplo, $\sqrt{{2}}$ y $\pi$ son números irracionales.",
        "nextText": ""}))

    print(chainRewriteStatement.invoke({
        "StatementType": "Definition",
        "prevText":"Definición 9: Un número real es irracional si no puede expresarse como una fracción de dos números enteros. ",
        "text": "Por ejemplo, $\sqrt{{2}}$ y $\pi$ son números irracionales.",
        "nextText": ""}))

    print(chainIdentifyStatement.invoke({"text": "A continuación hablaremos de los principales resultados del tema 1 de cálculo infinitesimal:"}))