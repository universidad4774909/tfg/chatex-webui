import importlib
import json
import os
import time
from langchain_core.language_models.llms import LLM
from langchain.prompts import PromptTemplate
from langchain_core.output_parsers import StrOutputParser, BaseTransformOutputParser
import pandas as pd
import matplotlib.pyplot as plt
from .cotPromptTemplatesLlamaModels import promptStatementConclusionsCoT, promptStatementHypothesesCoT
from pydantic import BaseModel
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
#spec = importlib.util.spec_from_file_location("archivo", "../carpeta1/subcarpeta1/archivo.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
# Para invocar la rutina anteponemos el nombre del módulo

Llamus = customLLMus.Llamus

from dotenv import dotenv_values
import re
config = dotenv_values(".env")
API_KEY = config.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = config.get("LLAMUS_BASE_PATH")




# Define Pydantic models for the output
class CoTResponse(BaseModel):
    thinking: str
    output: str


def cot_reflection(llm: LLM, prompt_template: PromptTemplate, output_parser: BaseTransformOutputParser, input_vars: dict, return_full_response: bool = False, verbose=False) -> CoTResponse:
    chain = (
        prompt_template 
        | llm
        | output_parser
    )
    # Make the API call to the language model
    full_response = chain.invoke(input_vars)

    if verbose:
        print(f"CoT with Reflection :\n{full_response}")

    # Extract content within <thinking> and <output> tags using regex
    thinking_match = re.search(r'<thinking>(.*?)</thinking>', full_response, re.DOTALL)
    output_match = re.search(r'<output>(.*?)(?:</output>|$)', full_response, re.DOTALL)

    thinking = thinking_match.group(1).strip() if thinking_match else "No thinking process provided."
    output = output_match.group(1).strip() if output_match else full_response



    if return_full_response:
        return full_response
    else:
        # Return as a pydantic model
        return CoTResponse(thinking=thinking, output=output)

def main():
    # LLM setup
    llm = Llamus(
        n=10,
        api_key=API_KEY,
        endpoint=LLAMUS_BASE_PATH,
        model="llama3.1:70b",
    )

    models = [
        'MiquMaid:v2-2x70B-DPO-Q5_K_M',
        "llama3.1:70b",
        'llama3:70b-instruct',
        'llama2:70b-chat',
        'mixtral:8x7b-instruct-v0.1-fp16'
    ]

    # Output parser (replace with actual implementation)
    output_parser = StrOutputParser()

    # Load existing results or create a new DataFrame
    if os.path.exists('results.csv'):
        results_df = pd.read_csv('results.csv')
    else:
        # Create columns dynamically based on models
        columns = ['statement', 'definitions']
        for model in models:
            columns.extend([f'hypotheses_{model}', f'conclusions_{model}', f'hypotheses_time_{model}', f'conclusions_time_{model}'])
        results_df = pd.DataFrame(columns=columns)

    with open("DataMining/testMetrics.json", "r") as f:
        statements = json.load(f)["examples"]

    for index, case in enumerate(statements):
        # Check if the current statement exists in the DataFrame
        if case['statement'] in results_df['statement'].values:
            row_idx = results_df.index[results_df['statement'] == case['statement']].tolist()[0]
        else:
            # Prepare row dictionary for this statement if not present
            row_idx = None
            row = {'statement': case['statement'], 'definitions': case['definitions']}
            results_df = results_df._append(row, ignore_index=True)
            row_idx = results_df.index[-1]

        for model in models:
            # Check if this model's hypotheses and conclusions are already computed
            if pd.notna(results_df.at[row_idx, f'hypotheses_{model}']) and pd.notna(results_df.at[row_idx, f'conclusions_{model}']):
                print(f"Results for model {model} and case {index + 1} already processed. Skipping.")
                continue

            print(f"\n--- Processing Model: {model}, Case: {index + 1} ---")

            # Timing the hypotheses identification
            start_time_hypotheses = time.time()
            hypotheses = cot_reflection(
                llm=llm,
                prompt_template=promptStatementHypothesesCoT,
                output_parser=output_parser,
                input_vars={
                    "definitions": case["definitions"],
                    "statement": case["statement"]
                },
                verbose=True
            )
            end_time_hypotheses = time.time()
            hypotheses_time = end_time_hypotheses - start_time_hypotheses

            # Timing the conclusions identification
            start_time_conclusions = time.time()
            conclusions = cot_reflection(
                llm=llm,
                prompt_template=promptStatementConclusionsCoT,
                output_parser=output_parser,
                input_vars={
                    "definitions": case["definitions"],
                    "statement": case["statement"]
                },
                verbose=True
            )
            end_time_conclusions = time.time()
            conclusions_time = end_time_conclusions - start_time_conclusions

            # Save model-specific results to the DataFrame
            results_df.at[row_idx, f'hypotheses_{model}'] = hypotheses
            results_df.at[row_idx, f'conclusions_{model}'] = conclusions
            results_df.at[row_idx, f'hypotheses_time_{model}'] = hypotheses_time
            results_df.at[row_idx, f'conclusions_time_{model}'] = conclusions_time

            # Save to CSV to persist results after processing each statement
            results_df.to_csv('results.csv', index=False)

    # Once all processing is done, visualize the time comparisons
    plot_time_comparisons(results_df, models)

def plot_time_comparisons(df, models):
    plt.figure(figsize=(10, 6))

    for model in models:
        # Plot hypotheses execution time
        plt.plot(df.index, df[f'hypotheses_time_{model}'], label=f'{model} - Hypotheses', marker='o')

        # Plot conclusions execution time
        plt.plot(df.index, df[f'conclusions_time_{model}'], label=f'{model} - Conclusions', marker='x')

    plt.title('Execution Time Comparison Between Models')
    plt.xlabel('Statement Index')
    plt.ylabel('Execution Time (seconds)')
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    main()
