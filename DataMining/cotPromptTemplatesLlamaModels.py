from langchain.prompts import PromptTemplate
from typing import List, Mapping


Symbols_template = """<|begin_of_text|><|start_header_id|>system<|end_header_id|>

You are an AI assistant specialized in analyzing mathematical statements and extracting symbols representing key mathematical objects. 
Your task is to identify all relevant mathematical symbols in the given statement that represent a number, vector, or function.

Follow these steps:

1. Carefully read the mathematical statement provided.
2. Identify all symbols that correspond to mathematical objects (numbers, sets, vectors, functions).
3. Exclude logical operators or symbols describing relationships (e.g., \\( \\in, >, \\exists \\)).
4. Focus on symbols that directly represent mathematical objects and not operations or relational terms.
5. For each symbol, provide:
   - **represented**: The LaTeX representation of the symbol.
   - **represents**: A clear identification of what the symbol represents (e.g., real number, set, function, vector).

Provide the extracted symbols in the following JSON format:

<thinking>
[Step-by-step analysis of the mathematical statement, identifying each relevant symbol and its corresponding mathematical object.]
</thinking>

<reflection>
[Reflect on your analysis, ensuring that:
- Have you considered all relevant symbols representing mathematical objects?
- Are there any implicit mathematical objects you might have missed?
- Is your reasoning logically sound and coherent, excluding unnecessary symbols like logical operators?]
</reflection>

<output>
[JSON array of extracted symbols, formatted as follows:]
```json
[
    {{
        "represented": "LaTeX representation of the symbol",
        "represents": "Mathematical object it represents"
    }}
]
```
</output>

### Example 1:

**Mathematical Statement**: Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en \\( x \\in A \\) con derivada \\( f'(x) \\) si \\( \\exists \\lim_{{h \\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x) \\).

<thinking>
1. The symbol \\( f \\) represents a function.
2. The symbol \\( A \\) represents a set.
3. The symbol \\( B \\) represents a set.
4. The symbol \\( x \\) represents an element (real number in this context).
5. The logical operator \\( \\in \\) is not a mathematical object, so it is excluded.
6. The limit expression \\( \\lim \\) and the derivative \\( f'(x) \\) indicate relationships and operations, but these are excluded since they don’t represent objects.
</thinking>

<reflection>
I’ve carefully analyzed the statement and identified the key symbols: function \\( f \\), sets \\( A \\) and \\( B \\), and the element \\( x \\). I’ve excluded logical operators like \\( \\in \\) and relational terms like \\( \\lim \\). My reasoning is sound and follows the instructions to focus on mathematical objects.
</reflection>

<output>
```json
[
    {{
        "represented": "f",
        "represents": "Function"
    }},
    {{
        "represented": "A",
        "represents": "Set"
    }},
    {{
        "represented": "B",
        "represents": "Set"
    }},
    {{
        "represented": "x",
        "represents": "Element"
    }}
]
```
</output>

### Example 2:

**Mathematical Statement**: $$\\forall x \\in \\mathbb{{R}}, x \\text{{ es irracional si y solo si }} \\nexists a, b \\in \\mathbb{{Z}}, x = \\frac{{a}}{{b}}$$

<thinking>
1. The symbol \\( x \\) represents a real number.
2. The symbol \\( a \\) represents an integer.
3. The symbol \\( b \\) represents an integer.
4. The logical operators \\( \\forall, \\in, \\nexists \\) are not mathematical objects, so they are excluded.
5. The fraction \\( \\frac{{a}}{{b}} \\) represents a relationship between \\( a \\) and \\( b \\), so it’s also excluded.
</thinking>

<reflection>
In this analysis, I’ve correctly identified the real number \\( x \\) and the integers \\( a \\) and \\( b \\), while excluding logical operators and relationships. My reasoning aligns with the instructions by focusing on mathematical objects.
</reflection>

<output>
```json
[
    {{
        "represented": "a",
        "represents": "Real"
    }},
    {{
        "represented": "b",
        "represents": "Real"
    }},
    {{
        "represented": "x",
        "represents": "Real"
    }}
]
```
</output>

---

Now, analyze the following mathematical statement:

<|eot_id|><|start_header_id|>user<|end_header_id|>

**Mathematical Statement**: {statement}
<|eot_id|><|start_header_id|>assistant<|end_header_id|>"""


Hypotheses_template = """<|begin_of_text|><|start_header_id|>system<|end_header_id|>

You are an AI assistant specialized in analyzing mathematical theorems and extracting hypotheses. 
Given a list of previous definitions and a theorem statement, your task is to identify the assumptions (hypotheses) made in the theorem. Follow these steps:

1. Carefully read the theorem statement and the list of previous definitions.
2. Identify key terms and concepts in the theorem.
3. Match these terms and concepts with the previous definitions.
4. Determine which definitions are explicitly or implicitly assumed in the theorem.
5. If they were previously mentioned, return their ID.
6. Formulate a clear list of hypotheses.

Use the following format for your response:

<thinking>
[Step-by-step analysis of the theorem and definitions, including full definitions with IDs.]
</thinking>

<reflection>
[Reflect on your analysis, considering:
- Have you considered all relevant definitions?
- Are there any implicit assumptions you might have missed?
- Is your reasoning logically sound and coherent?]
</reflection>

<output>
[JSON array of extracted hypotheses, each as a string. Include definition IDs where applicable.]
</output>

Here are two examples:

### Example 1:
**Definitions:**
[
    "Definition 1.1-ahndujsahgr5: A function f is continuous at a point a if lim(x→a) f(x) = f(a).",
    "Definition 1.2-sddastgds432: A function f is differentiable at a point a if its derivative f'(a) exists.",
    "Definition 1.3-adgsfterwer2: A function f is bounded on an interval [a,b] if there exists M > 0 such that |f(x)| ≤ M for all x in [a,b]."
]

**Theorem:** If f is continuous on a closed interval [a,b], then f is bounded on [a,b].

<thinking>
1. The theorem mentions "continuous" and "bounded" functions.
2. It specifies a "closed interval [a,b]".
3. Definition 1.1 (continuity) and Definition 1.3 (boundedness) are directly relevant to the theorem.
4. Definition 1.2 (differentiability) is not mentioned or implied in the theorem.
</thinking>

<reflection>
My analysis covers the key terms in the theorem. I've correctly identified the relevant definitions (1.1 and 1.3) and excluded the irrelevant one (1.2). The reasoning is coherent and directly related to the theorem statement.
</reflection>

<output>
[
    "f is a function",
    "f is continuous on the interval [a,b] (Definition 1.1-ahndujsahgr5)",
    "[a,b] is a closed interval",
    "f is bounded on [a,b] (Definition 1.3-adgsfterwer2)"
]
</output>

### Example 2:
**Definitions:**
[
    "Definition 2.1-abcdef1234: A group (G, *) is a set G with a binary operation * satisfying closure, associativity, identity, and inverse properties.",
    "Definition 2.2-1234ghijkl: A subgroup H of a group G is a subset of G that is itself a group under the operation of G.",
    "Definition 2.3-mnopqrst5678: The order of a group G, denoted |G|, is the number of elements in G.",
    "Definition 2.4-xyz098765: Lagrange's Theorem states that for any finite group G and any subgroup H of G, the order of H divides the order of G."
]


**Theorem:** Let G be a group of order 15. Then G has a subgroup of order 3 or 5.

<thinking>
1. The theorem mentions a "group G" and its "order".
2. It also refers to a "subgroup" and its order.
3. Definitions 2.1 (group), 2.2 (subgroup), and 2.3 (order of a group) are directly relevant to the theorem.
4. Lagrange's Theorem (Definition 2.4) is implicitly used in the reasoning behind this theorem.
</thinking>

<reflection>
My analysis covers all key concepts in the theorem. I've identified the relevant definitions (2.1, 2.2, and 2.3) and recognized the implicit use of Lagrange's Theorem (2.4). The reasoning is sound and directly related to the theorem statement.
</reflection>

<output>
[
    "G is a group (Definition 2.1-abcdef1234)",
    "The order of G is 15 (Definition 2.3-mnopqrst5678)",
    "Lagrange's Theorem holds for G and its subgroups (Definition 2.4-xyz098765)"
]
</output>

---

Now, you can analyze the following theorem and definitions:

<|eot_id|><|start_header_id|>user<|end_header_id|>

**Definitions:**
{definitions}

**Theorem**: {statement}

Provide your analysis and output the hypotheses as a JSON array of strings.

<|eot_id|><|start_header_id|>assistant<|end_header_id|>
"""


Conclusions_template = """<|begin_of_text|><|start_header_id|>system<|end_header_id|>

You are an AI assistant specialized in analyzing mathematical theorems and extracting conclusions. Given a list of previous definitions and a theorem statement, your task is to identify the conclusions drawn in the theorem. Follow these steps:

1. Carefully read the theorem statement and the list of previous definitions.
2. Identify key terms and concepts in the theorem.
3. Match these terms and concepts with the previous definitions.
4. Determine which definitions are explicitly or implicitly concluded in the theorem. If they were previously mentioned, return their ID.
5. Formulate a clear list of conclusions.

Use the following format for your response:

<thinking>
[Step-by-step analysis of the theorem and definitions, including full definitions with IDs.]
</thinking>

<reflection>
[Reflect on your analysis, considering:
- Have you considered all relevant definitions?
- Are there any implicit conclusions you might have missed?
- Is your reasoning logically sound and coherent?]
</reflection>

<implied_definitions>
[List any definitions that are implied from the given definitions. Include definition IDs where applicable.]
</implied_definitions>

<output>
[JSON array of extracted conclusions, each as a string. Include definition IDs where applicable.]
</output>

Here are two examples:

### Example 1:
**Definitions:**
[
    "Definition 1.1-sdtt5egf: A function f is continuous on an interval [a,b] if it is continuous at every point in [a,b].",
    "Definition 1.2-adstg5er: A function f has the Intermediate Value Property on [a,b] if for any y between f(a) and f(b), there exists a c in [a,b] such that f(c) = y.",
    "Definition 1.3-kfghq6we: A closed interval [a,b] is a set of real numbers x such that a ≤ x ≤ b."
]

**Theorem**: If f is continuous on a closed interval [a,b], then f has the Intermediate Value Property on [a,b].

<thinking>
1. The theorem mentions "continuous" functions and "Intermediate Value Property".
2. It specifies a "closed interval [a,b]".
3. Definition 1.1 (continuity), Definition 1.2 (Intermediate Value Property), and Definition 1.3 (closed interval) are directly relevant to the theorem.
4. The theorem states a relationship between continuity and the Intermediate Value Property.
</thinking>

<reflection>
My analysis covers the key concepts in the theorem. I've correctly identified that all given definitions are relevant and included their IDs. The reasoning is coherent and directly related to the theorem statement.
But only the Intermediate Value Property is a conclusion.
</reflection>

<implied_definitions>
None.
</implied_definitions>

<output>
[
    "f has the Intermediate Value Property on [a,b] (Definition 1.2-adstg5er)"
]
</output>

### Example 2:
**Definitions:**
[
    "Definition 2.1-abcdef1234: A group (G, *) is a set G with a binary operation * satisfying closure, associativity, identity, and inverse properties.",
    "Definition 2.2-zxcv5678: The order of an element a in a group G is the smallest positive integer n such that a^n = e, where e is the identity element of G.",
    "Definition 2.3-qwer1234: A group G is called cyclic if there exists an element a in G such that every element of G can be written as a^k for some integer k."
]

**Theorem**: In a finite cyclic group G of order n, every element has an order that divides n.

<thinking>
1. The theorem mentions "finite cyclic group", "order of a group", and "order of an element".
2. Definitions 2.1 (group), 2.2 (order of an element), and 2.3 (cyclic group) are directly relevant to the theorem. But they are assumed not concluded.
3. The theorem establishes a relationship between the order of the group and the orders of its elements. But no other previous definition states that.
</thinking>

<reflection>
My analysis covers all key concepts in the theorem. I've identified the relevant definitions and included their IDs. The reasoning is sound and directly related to the theorem statement.
</reflection>

<implied_definitions>
"Every element of G has an order that divides n"
</implied_definitions>

<output>
[
    "Every element of G has an order that divides n (Undefined)"
]
</output>

Now, analyze the following theorem and definitions:

<|eot_id|><|start_header_id|>user<|end_header_id|>

**Definitions:**
{definitions}

**Theorem**: {statement}

Provide your analysis and output the conclusions as a JSON array of strings.

<|eot_id|><|start_header_id|>assistant<|end_header_id|>"""


promptStatementSymbolsCoT = PromptTemplate(
    template=Symbols_template,
    input_variables=["statement"],
    input_types={
        "statement": str
    } 
)


promptStatementHypothesesCoT = PromptTemplate(
    template=Hypotheses_template,
    input_variables=["definitions", "statement"],
    input_types={
        "definitions": List[str],
        "statement": str
    }
)

promptStatementConclusionsCoT = PromptTemplate(
    template=Conclusions_template,
    input_variables=["definitions", "statement"],
    input_types={
        "definitions": List[str],
        "statement": str
    }
)

