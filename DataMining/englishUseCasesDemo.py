# Definitions list
import json
from cotChains import cot_reflection
from nucliaDB import VectorKB
from chains import llm2 as llm
from llmOutputErrorFixer import LLMOutputFixer
from symbolSubstituter import MathSymbolSubstituter
from langchain_core.output_parsers import StrOutputParser
from cotPromptTemplatesLlamaModels import  promptStatementSymbolsCoT


definitions = [
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Anonymous_Definition", r"The polynomial $P(x)$ has no roots in the set $A$."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Continuous_Function", r"A function $f: \mathbb{R} \to \mathbb{R}$ is continuous if $\forall \epsilon > 0, \exists \delta > 0$ such that $|x - c| < \delta \implies |f(x) - f(c)| < \epsilon$."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Rational_Function", r"A rational function is defined as $f(x) = \frac{P(x)}{Q(x)}$, where $P(x)$ and $Q(x)$ are polynomials."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Real_Function", r"A real function $f$ is a mapping $f: \mathbb{R} \to \mathbb{R}$."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Bounded_Function_f_A_R", r"A function $f$ is bounded if $\exists M > 0$ such that $|f(x)| \leq M$ for all $x \in A \subset \mathbb{R}$."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Compact_Interval", r"An interval $[a, b] \subset \mathbb{R}$ is compact if it is closed and bounded."),
    ("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Definition_Polynomial_Roots", r"The roots of a polynomial $P(x)$ are the values of $x$ such that $P(x) = 0$.")
]


def preProcessStatement(statement):
    try:
        symbols = cot_reflection(llm, promptStatementSymbolsCoT, StrOutputParser(), {"statement":statement}).output
        symbolsFixed = LLMOutputFixer.fix_output(symbols)
        symbolsParsed = json.loads(symbolsFixed)
        symbolSubstituter = MathSymbolSubstituter(symbolsParsed)
        preparsedStatement = symbolSubstituter.substitute(statement)
        return preparsedStatement  
    except Exception as e:
        print(e)
    return statement



ontology_kb = VectorKB('localhost', 8080, class_kb="latex_classes_demo", individuals_kb="latex_individuals_demo", reset_kb=False)

# Adding definitions to the vector DB
for definition, latex_statement in definitions:
    statement = {
        "statement": latex_statement,  # Include the LaTeX statement here
        "name": definition.split("#")[-1]  # Extract the name from the URI
    }
    key = definition.split("#")[-1]  # Use the name as the key
    statement_uri = definition  # The URI of the definition
    labels = ["Definition"]  # Add a label for the type

    # Call the function to add the resource
    preprocessed_statement = preProcessStatement(statement["statement"])
    ontology_kb.add_resource(
        ontology_kb.individuals_kb,
        key,
        statement["name"],
        preprocessed_statement,
        labels=[statement_uri]
    )

# Print the simulated vector DB contents to verify
print(ontology_kb.individuals_kb)
