#Extracted from https://www.overleaf.com/read/vkjxpjpwnjpm#961563
definitions = [
    "\\begin{definition}Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b\\end{definition}",
               """\\begin{definition}
    Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{x\\to a} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$
\\end{definition}""",
"""\\begin{definition}Diremos que una función $$f:\\mathbb{R}\\to B\\subseteq\\mathbb{R}$$ es continua si $$\\lim_{x\\to a} f(x) = f(a)$$
\\end{definition}""",
"""\\begin{definition}Diremos que una función $$f:\\mathbb{R}\\to B\\subseteq\\mathbb{R}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{h\\to 0} \\frac{f(x+h)-f(x)}{x+h-x} = f'(x)$$
\\end{definition}"""]

from ontologyWrapper import RDFOntologyUtils
import json
from chains import *
from langchain_core.exceptions import OutputParserException

from URIs import *

rdf_file_path = "latexOntology.rdf"
owl_file_path = "latexOntology.owx"
def create_mutable_files(files):
    for filename in files:
        with open(filename, "rb") as f:
            name, extension = tuple(filename.split("."))
            with open(f"mutable{name.title()}.{extension}", "wb") as mutable_file:
                mutable_file.write(f.read())
create_mutable_files([rdf_file_path,owl_file_path])
rdf_file_path = "mutableLatexOntology.rdf"
owl_file_path = "mutableLatexOntology.rdf"
ontology_utils = RDFOntologyUtils(rdf_file_path, owl_file_path)
mathematicalObjectsURI = ontology_utils.get_subclasses('http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Object')

from nucliaDB import VectorKB

ontology_kb = VectorKB('localhost',8080,class_kb="latex_classes", individuals_kb="latex_individuals", reset_kb=True)
definitionClassOntology = ontology_kb.class_kb.get(DEFINITION_URI.split("#")[-1])
if definitionClassOntology==None:
    ontology_kb.add_resource(ontology_kb.class_kb,key=DEFINITION_URI.split("#")[-1], title=f"Definition", text=f"Definition", labels=["MathematicalStatement"])
    definitionClassOntology = ontology_kb.class_kb.get(DEFINITION_URI.split("#")[-1])
class DefinitionModel(MathematicalStatementModel):
    parent_uri = DEFINITION_URI
#DefinitionModel(name="Hola",statement="Pepe", symbols=[SymbolModel(represents="r",represented="p")])

def preparseStatement(original_statement):
    try:
        preparsedStatement = chainRewriteStatement.invoke({
            "StatementType": "Definition",
            "prevText":"",
            "text": original_statement,
            "nextText": ""})
    except Exception as e:
        print(e)
    if ("\n" in preparsedStatement):
        preparsedStatement = preparsedStatement.split("\n")[-1]
    return preparsedStatement


def getSymbols(preparsedStatement: str,classes_uri: List[str]):
    result = []
    while len(result)==0:
        try:
            result = parseDefinitionStatementChain.invoke({
                "query": preparsedStatement,
                "mathematicalObjects": json.dumps(classes_uri)
            })
        except OutputParserException as e:
            import re
            print(e)
            regex = r"```json\\n(.+)```"
            if re.match(regex,e.llm_output):
                new_str = re.sub(regex,"\g<1>",e.llm_output)
                try:
                    result = json.loads(new_str)
                except:
                    print(e)
                    continue
            else:
                print(e)
                continue
        clases_context = [c.split("#")[-1] for c in classes_uri]

        if not all({symbol["represents"] in clases_context for symbol in result}):
            result = [r for r in result if r["represents"] in clases_context]
        #print(result)
    return result



definitionsWithURI = [{
    'URI': f"Definición {i}",
    'text': d
} for i,d in enumerate(definitions)]

def getPreviousDefinitions(i,d):

    definitions = ontology_kb.find_most_similar(ontology_kb.individuals_kb,d,topk=8)
    return definitions
    if i<=2:
        return definitionsWithURI[:i]
    else:
        return definitionsWithURI[i-1]



import time

for i,d in enumerate(definitions):
    start_time = time.time()  # Record start time
    print(f"Processing Definición {i}")
    print(f"Original Statement: \n{d}")
    preparsedDef = d # As we are using latex we don't need preparsing -> preparseStatement(d)
    print(f"Preparsed: \n{preparsedDef}")
    symbols = getSymbols(preparsedDef,mathematicalObjectsURI)
    print(f"Symbols: \n{symbols}")
    definitionInstance = DefinitionModel(name=f"Definición {i}",statement=preparsedDef,symbols=symbols)
    
    definition_uri = add_statement(definitionInstance)
    prevDefinitions = getPreviousDefinitions(i,d)
    hypothesis = parseDefinitionHypothesisChain.invoke({
        "symbols": symbols,
        "statement": d,
        "previousDefinitions": json.dumps(prevDefinitions)
    })
    print(f"Definición {i} assumes {', '.join(hypothesis)}")

    conclusions = parseDefinitionHypothesisChain.invoke({
        "symbols": symbols,
        "statement": d,
        "previousDefinitions": json.dumps(prevDefinitions)
    })
    print(f"Definición {i} implies {', '.join(conclusions)}")
    try:
        ontology_utils.reason()
        ontology_utils.onto.inconsistent_classes()
    except Exception as e:
        print(e)
    ontology_utils.save_changes()
    elapsed_time = time.time() - start_time  # Calculate elapsed time
    print(f"Time elapsed for iteration {i}: {elapsed_time} seconds")








