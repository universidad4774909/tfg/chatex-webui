import importlib
import json
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
#spec = importlib.util.spec_from_file_location("archivo", "../carpeta1/subcarpeta1/archivo.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
# Para invocar la rutina anteponemos el nombre del módulo
from dotenv import dotenv_values
import re
config = dotenv_values(".env")
API_KEY = config.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = config.get("LLAMUS_BASE_PATH")

Llamus = customLLMus.Llamus
# LLM setup
llm = Llamus(
    n=10,
    api_key=API_KEY,
    endpoint=LLAMUS_BASE_PATH,
    model="llama3.1:70b",
)

data = json.load(open("DataMining/testMetrics.json"))
print("\n".join([f"3.{i}. {e['statement']}" for i,e in enumerate(data["examples"],start=1)]))