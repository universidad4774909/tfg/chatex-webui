
from typing import List, Mapping
from langchain_core.prompts import PromptTemplate

from .promptTemplates import MathematicalStatementListParser, StringListParser
from .models import MathematicalStatementListModel, SymbolListModel, SymbolModel



promptIdentifyStatement = PromptTemplate.from_template(
        """Given the mathematical text below, classify it as either one of this mathematical statements {statementTypes}.
Answer None if not of them were found and be really careful of not returning as a Definition something that is None.
Do not respond with more than one word.

<examples>
<text>Definición 4: Un número primo es un número natural mayor que 1 que tiene exactamente dos divisores distintos: 1 y él mismo.</text>
Classification: Definition
<text>Definición 5: Una matriz es una colección rectangular de números dispuestos en filas y columnas.</text>
Classification: Definition
<text>Definición 6: Una función es biyectiva si es tanto inyectiva como sobreyectiva.</text>
Classification: Definition
<text>Definición 1.2: Diremos que T (a,b,c) es un triángulo rectángulo si tiene un ángulo recto y denotaremos por a,b a sus catetos ( lados que comparten el ángulo recto ) y c a la hipotenusa</text>
Classification: Definition
<text>Teorema 6: En todo triángulo isósceles, los ángulos opuestos a los lados iguales son congruentes entre sí.</text>
Classification: Theorem
<text>Teorema 7: Si un número es divisible por 4, entonces también es divisible por 2.</text>
Classification: Theorem
<text>Teorema 8: El número de combinaciones posibles de n elementos tomados de k en k, denotado como C(n,k), es igual a n! / (k! * (n-k)!).</text>
Classification: Theorem
<text>Axioma 5: La suma de dos números reales es conmutativa.</text>
Classification: Axiom
<text>Axioma 6: Para cada par de puntos distintos, existe una línea recta única que pasa por ambos puntos.</text>
Classification: Axiom
<text>Axioma 7: Si dos líneas son paralelas y una tercera línea intersecta una de ellas, entonces también intersecta la otra línea.</text>
Classification: Axiom
<text>Lema 5: Si un número es divisible por 6, entonces también es divisible por 2 y por 3.</text>
Classification: Lemma
<text>Lema 6: Si un polinomio tiene coeficientes reales y una raíz compleja, entonces su conjugado también es raíz del polinomio.</text>
Classification: Lemma
<text>Lema 7: Si dos ángulos son complementarios entre sí, entonces la suma de sus medidas es igual a 90 grados.</text>
Classification: Lemma
<text>Corolario 4: De la existencia de un número primo entre n y 2n, se sigue que siempre hay infinitos números primos.</text>
Classification: Corollary
<text>Corolario 5: Si un conjunto A está incluido en un conjunto B y B está incluido en un conjunto C, entonces A está incluido en C.</text>
Classification: Corollary
<text>Corolario 6: De la definición de una función inyectiva, se sigue que una función inyectiva no asigna dos elementos distintos del dominio al mismo elemento del codominio.</text>
Classification: Corollary
<text>Ahora introduciremos los principales resultados</text>
Classification: None
<text>El resultado anterior es de especial importacia</text>
Classification: None
<text>En la literatura matemática moderna, se hace un uso extensivo de la teoría de conjuntos para fundamentar rigurosamente el resto de las matemáticas.</text>
Classification: None
<text>Los resultados presentados en este capítulo son fundamentales para comprender la estructura de los espacios vectoriales de dimensión finita.</text>
Classification: None
<text>La noción de continuidad es esencial en análisis matemático y tiene numerosas aplicaciones en diversas ramas de las ciencias.</text>
Classification: None
</examples>
Answer only one word, the corresponding classification
Query:
<text>{text}</text>
Classification: """)

statementHypothesisParsing = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer alway in the format described in style section JSON.

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify hypothesis.
Given a mathematical statement and its most similar definitions you should be able to obtains its hypothesis in JSON format

All symbols will have the following structure:
    - Represented: latex representation of the symbol
    - Represents: One of the given mathematicalObjects (Ej: x\\in R -> Real) (return URI as it is a owl ontology)

Elements to identify:    
    - Other statements names used in this statement
    - In case an hypothesis wasn't previously defined you will just return its latex representation

Instructions:
    1. Identify which of the previousDefinitions are assumed to know in the query statement from the list of previousStatements
    2. Return a list with the URI of them or an empty list if any of them is required and there are no hypothesis

Example 1:
    symbols:
        [
        {{
        "represented": "f",
        "represents": "Function"
        }},
        {{
        "represented": "A",
        "represents": "Set"
        }},
        {{
        "represented": "B",
        "represents": "Set"
        }},
        {{
        "represented": "x",
        "represents": "element"
        }}
    ]
    previousStatements: []
    query: "Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b"
    
    Hyphothesis:[]

    
Example 2:
    symbols:
        [
        {{
        "represented": "f",
        "represents": "Function"
        }},
        {{
        "represented": "A",
        "represents": "Set"
        }},
        {{
        "represented": "B",
        "represents": "Set"
        }},
        {{
        "represented": "x",
        "represents": "element"
        }}
    ]
    previousStatements: [
    {{
        "text":"Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b",
        "name":"Definicion1.1.1.1-nsaduh5287"
    }},
    {{
        "text":"Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{{x\\to a}} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$\\end{{definition}}",
        "name":"Definicion1.1.1.2-safgf5423"
    }},
    {{
        "text":"Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$",
        "name":"Definicion1.1.1.3-ssaafs33421"
    }},
    ]
    query: "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    
    Hyphothesis:["Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$"]
END OF EXAMPLES
# User Preamble
## Task and Context
Symbols:
{symbols}

Previous Statements:
{previousStatements}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of hypothesis that you identify in the text. You should return an empty list if it is an independent statement. The list of hypothesis you identify must be formatted as a list of strings with hypothesis' latex expressions, for example:

Hypothesis:```json
[
   "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$", 
   "Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{{x\\to a}} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$\\end{{definition}}"
]<|END_OF_TURN_TOKEN|>"""


statementHypothesisParsing2 = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer alway in the format described in style section JSON.

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify hypothesis.
Given a mathematical statement and its most similar statements you should be able to obtains its hypothesis in JSON format

Elements to identify:    
    - Other statements assumed in this statement

Instructions:
    1. Identify which of the previousStatements are assumed to know in the query statement from the list of previousStatements
    2. Return a list with the URI of them or an empty list if any of them is required and there are no hypothesis

Example 1:
    symbols:
        [
        {{
        "represented": "f",
        "represents": "Function"
        }},
        {{
        "represented": "A",
        "represents": "Set"
        }},
        {{
        "represented": "B",
        "represents": "Set"
        }},
        {{
        "represented": "x",
        "represents": "element"
        }}
    ]
    previousStatements: []
    query: "Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b"
    
    Hyphothesis:[]

    
Example 2:
    symbols:
        [
        {{
        "represented": "f",
        "represents": "Function"
        }},
        {{
        "represented": "A",
        "represents": "Set"
        }},
        {{
        "represented": "B",
        "represents": "Set"
        }},
        {{
        "represented": "x",
        "represents": "element"
        }}
    ]
    previousStatements: [
    {{
        "text":"Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b",
        "name":"Definicion1.1.1.1-nsaduh5287"
    }},
    {{
        "text":"Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{{x\\to a}} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$\\end{{definition}}",
        "name":"Definicion1.1.1.2-safgf5423"
    }},
    {{
        "text":"Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$",
        "name":"Definicion1.1.1.3-ssaafs33421"
    }},
    ]
    query: "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    
    Hyphothesis:["Definicion1.1.1.3-ssaafs33421"]

END OF EXAMPLES
# User Preamble
## Task and Context
Symbols:
{symbols}

Previous Statements:
{previousStatements}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of hypothesis that you identify in the text. You never return an empty list, you should always assume definitions of concepts involved in the statement. The list of hypothesis you identify must be formatted as a list of strings with hypothesis' latex expressions, for example:
Don't return symbols I want statements names from previos Statements
Hypothesis:```json
["Definicion1.1.1.3-ssaafs33421", "Teorema2.3.1.4-adbsjdjj3323" ]<|END_OF_TURN_TOKEN|>"""
promptStatementHypothesisParsing = PromptTemplate(
    template= statementHypothesisParsing,
    input_variables=["symbols","statement","previousStatements"],
    input_types={
        "symbols": SymbolListModel,
        "statement": str,
        "previousStatements": List[str]
    },
    partial_variables={"format_instructions": StringListParser.get_format_instructions()}

)

# statementHypothesisParsing2 = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
# <|SYSTEM_TOKEN|> # Safety Preamble
# The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer alway in the format described in style section JSON.

# # System Preamble
# ## Basic Rules
# You are a powerful conversational AI trained by some experts mathematicians to identify hypothesis.
# Given a mathematical statement and its most similar definitions you should be able to obtains its hypothesis in JSON format

# All symbols will have the following structure:
#     - Represented: latex representation of the symbol
#     - Represents: One of the given mathematicalObjects (Ej: x\\in R -> Real) (return URI as it is a owl ontology)

# Elements to identify:    
#     - Other statements names used in this statement
#     - In case an hypothesis wasn't previously defined you will just return its latex representation

# Instructions:
#     1. Identify which of the previousDefinitions are assumed to know in the query statement from the list of previousStatements
#     2. Return a list with the URI of them or an empty list if any of them is required and there are no hypothesis

# Example 1:
#     symbols:
#         [
#         {{
#         "represented": "f",
#         "represents": "Function"
#         }},
#         {{
#         "represented": "A",
#         "represents": "Set"
#         }},
#         {{
#         "represented": "B",
#         "represents": "Set"
#         }},
#         {{
#         "represented": "x",
#         "represents": "element"
#         }}
#     ]
#     previousStatements: []
#     query: "Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b"
    
#     Hyphothesis:[]

    
# Example 2:
#     symbols:
#         [
#         {{
#         "represented": "f",
#         "represents": "Function"
#         }},
#         {{
#         "represented": "A",
#         "represents": "Set"
#         }},
#         {{
#         "represented": "B",
#         "represents": "Set"
#         }},
#         {{
#         "represented": "x",
#         "represents": "element"
#         }}
#     ]
#     previousStatements: [
#     {{
#         "text":"Diremos que $$f:A\\to B$$ es una función si f es un conjunto de pares ordenados tal que $$(a, b),(a,c) \\in A\\times B \\implies b=c$$ Si $(a,b) \\in f$ entonces denotaremos por $f(a)$ a b",
#         "name":"Definicion1.1.1.1-nsaduh5287"
#     }},
#     {{
#         "text":"Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{{x\\to a}} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$\\end{{definition}}",
#         "name":"Definicion1.1.1.2-safgf5423"
#     }},
#     {{
#         "text":"Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$",
#         "name":"Definicion1.1.1.3-ssaafs33421"
#     }},
#     ]
#     query: "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    
#     Hyphothesis:["Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$"]
# END OF EXAMPLES
# # User Preamble
# ## Task and Context
# Symbols:
# {symbols}

# Previous Statements:
# {previousStatements}

# ## Style Guide
# {format_instructions} 

# <|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
# {statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of hypothesis that you identify in the text. You should return an empty list if it is an independent statement. The list of hypothesis you identify must be formatted as a list of strings with hypothesis' latex expressions, for example:

# Hypothesis:```json
# [
#    "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$", 
#    "Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como: $$\\lim_{{x\\to a}} f(x) = L \\Leftrightarrow \\forall \\epsilon > 0, \\exists \\delta > 0, \\forall x \\in A : 0 < |x-a| < \\delta \\implies |f(x) - L| < \\epsilon$$\\end{{definition}}"
# ]<|END_OF_TURN_TOKEN|>"""


catchAllHypothesis = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer alway in the format described in style section JSON.

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify hypothesis.
Given a mathematical statement and its hypothesis you should be able to identify if any of them is missing

Elements to identify:    
    - Other statements assumed in this statement and not included in hypothesis

Instructions:
    1. Identify if any of the statement assumptions is missing in hypothesis list
    2. Return a list with latex of assumptions ignored if there is one

Example 1:
    hypothesis: [{{"name":"Definicion1-ajmsajmdoako","text":"We say that a function \\( f: \\mathbb{{R}} \\rightarrow B \\subseteq \\mathbb{{R}} \\) is continuous if \\( \\lim_{{x \\to a}} f(x) = f(a) \\)."}}]
    query: "\\begin{{theorem}}[Teorema de Bolzano]
    Sea $f: [a,b] \\to \\mathbb{{R}}$\\\\
    $$
    \\begin{{cases}}
        \\text{{ f is a continuous function}} \\\\
        $f(a)f(b)<0$ 
    \\end{{cases}}
    $$
    $\\implies$
    $$
    \\begin{{cases}}
        \\exists c \\in [a,b] : f(c)=0
    \\end{{cases}}
    $$
\\end{{teorema}}"
    
    Missing:["$f(a)f(b)<0$"]

    
Example 2:
    
    hypothesis: [
    {{
        "text":"Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$",
        "name":"Definicion1.1.1.3-ssaafs33421"
    }},
    ]
    query: "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    
    Missing:[]

END OF EXAMPLES
# User Preamble
## Task and Context

hypothesis:
{hypothesis}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of hypothesis that you identify in the text. You should return an empty list if it is an independent statement. The list of hypothesis you identify must be formatted as a list of strings with hypothesis' latex expressions, for example:
Missing:```json
["$f(a)f(b)<0$" ]<|END_OF_TURN_TOKEN|><|START_OF_TURN_TOKEN|>Missing:
"""
promptStatementHypothesisChecking = PromptTemplate(
    template= catchAllHypothesis,
    input_variables=["statement","hypothesis"],
    input_types={
        "statement": str,
        "hypothesis": List[Mapping[str,str]]
    },
    partial_variables={"format_instructions": StringListParser.get_format_instructions()}

)

statementConclusionsParsing = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer always in the format described in style section JSON.
Return a valid PYTHON string with backlash escaped with \\
# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify conclusions of mathematical statements.
Given a mathematical statement, symbols that appear in it and its most similar statements you should be able to obtain what of the given statements can be implied as a conclusion.

All symbols will have the following structure:
     - Represented: latex representation of the symbol
     - Represents: One of the given mathematicalObjects (Ej: x\\in R -> Real) (return URI as it is a owl ontology)

Elements to identify:    
     - The conclusion derived from the statement
     - You must return the name of the given statement that is concluded

Instructions:
    1. Identify which conclusion can be derived from the statement
    2. Return the name of the conclusion or an empty list if no conclusion can be derived

Examples:
**Example 1**
        symbols:
        [
        {{"represented": "f", "represents": "\\mathrm{{Function}}"}},
        {{"represented": "A", "represents": "\\mathbb{{Set}}"}},
        {{"represented": "B", "represents": "\\mathbb{{R}}"}},
        {{"represented": "x", "represents": "\\mathrm{{element}}"}}
        ]
        previousStatements: [{{
            "name": "Definition1.1.1-asasjh5211",
            "text": "f attains both a maximum and a minimum value in [a, b], i.e., there exist c1 and c2 in [a, b] such that f(c1) is the maximum and f(c2) is the minimum."
            }},
            {{
            "name": "Definition1.2.3-sdniaje122",
            "text": "If f is differentiable on the open interval (a, b), then the points where the derivative f'(x) vanishes or does not exist are critical points of f. These points can be maxima, minima, or points of inflection."
            }},
            {{
            "name": "Definition1.4.1-uyasdhfu21",
            "text": "A sequence $\\[{{a_n\\}}]_{{n=1}}^{{\\infty}}$ converges to a limit $L$ if for every $\\varepsilon > 0$, there exists an $N \\in \\mathbb{{N}}$ such that for all $n \\geq N$, $|a_n - L| < \\varepsilon$."
            }}]
        query: "\\begin{{theorem}}[Extreme Value Theorem]
            Let \\( f: [a, b] \\to \\mathbb{{R}} \\) be a function continuous on the closed and bounded interval \\( [a, b] \\).
            
            \\begin{{enumerate}}
                \\item Existence of extreme values: \\( f \\) attains both a maximum and a minimum value in \\( [a, b] \\), i.e., there exist \\( c_1 \\) and \\( c_2 \\) in \\( [a, b] \\) such that \\( f(c_1) \\) is the maximum and \\( f(c_2) \\) is the minimum.
                
                \\item Critical points: If \\( f \\) is differentiable on the open interval \\( (a, b) \\), then the points where the derivative \\( f'(x) \\) vanishes or does not exist are critical points of \\( f \\). These points can be maxima, minima, or points of inflection.
                
                \\item Characterization of extremum: If \\( f \\) attains a local maximum or minimum at a critical point \\( c \\) in the interval \\( (a, b) \\), then \\( f'(c) = 0 \\) or the derivative does not exist at \\( c \\).
            \\end{{enumerate}}
        \\end{{theorem}}"
        Conclusions: ["Definition1.1.1-asasjh5211", "Definition1.2.3-sdniaje122"]
    **Example 2**
        symbols:
        [
        {{"represented": "n", "represents": "natural number"}},
        {{"represented": "p", "represents": "prime number"}},
        {{"represented": "k", "represents": "integer"}}
        ]
        previousDefinitions: [
            {{"text":"Definimos el límite de una función $f:A\\to B$ cuando x tiende a un punto $a \\in A$ como:", "name":"Definicion1.1.1.2-nsaduh5"}},
            {{"text":"Diremos que una función $$f:\\mathbb{{R}}$$ es continua si $$\\lim_{{x\\to a}} f(x) = f(a)$$", "name":"Definicion1.1.1.3-ssaafs33421"}}
        ]
        query: "\\begin{{Theorem}}[The fundamental theorem of arithmetic]
            Each natural number admits a representation as a product 
            \\begin{{equation}}
                n = p_1 \\cdots p_k \\nonumber
            \\end{{equation}}
            where $ p_1,..., p_k $ are prime numbers. This representation is unique except for the order of the factors.
        \\end{{Theorem}}"
        Conclusions: [] # None of the prevStatements is concluded
        
    **Example 3**
        symbols: [
            {{
                "represented": "f",
                "represents": "Function"
            }},
            {{
                "represented": "X",
                "represents": "Set"
            }},
            {{
                "represented": "Y",
                "represents": "Set"
            }},
            {{
                "represented": "x",
                "represents": "Element"
            }}
        ]
        previousDefinitions: []
        query: "We say that there is a \\textit{{function}} defined on $ X $ with values in $ Y $ if, by virtue of some fule $ f $, to each element $ x\\in X $ there corresponds an element $ y \\in Y $.\\n\\begin{{equation}}\\nf(X) \\coloneqq \\{{y \\in Y | \\exists x((x\\in X)\\land(y = f(x)))\\}} \\nonumber\\n\\end{{equation}}\\nX is called the \\textit{{domain of definition}} and Y is called \\textit{{set of values}} or \\textit{{range}} of the function.\\n"
        Conclusions: []

# User Preamble
## Task and Context
Symbols: 
{symbols}
Previous Statements:
{previousStatements}
DON'T RETURN THE SYMBOLS, 
## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>
Write a json-formatted list of conclusions that you identify in the text. You hardly ever return an empty list, you have to be really sure any of the previous statements match your statement. The list of conclusions you identify must be formatted as a list of statements names, for example:
Conclusions:```json
[
    "Definition1.1.1-asasjh5211", "Definition1.2.3-sdniaje122", "Corollary1.4.2.3-adbgahdi21"
]<|END_OF_TURN_TOKEN|><|START_OF_TURN_TOKEN|>Conclusions:
"""

promptStatementConclusionParsing = PromptTemplate(
    template= statementConclusionsParsing,
    input_variables=["symbols","statement","previousStatements"],
    input_types={
        "symbols": SymbolListModel,
        "statement": str,
        "previousStatements": List[Mapping[str,str]]
    },
    partial_variables={"format_instructions": StringListParser.get_format_instructions()}

)


catchAllConclusions = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer always in the format described in the style section JSON.

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify conclusions.
Given a mathematical statement, you should be able to identify if any conclusions are present in the text that are not explicitly mentioned.

Elements to identify:    
    - Conclusions derived or implied from the text but not explicitly stated

Instructions:
    1. Identify any conclusions derived or implied from the text but not explicitly stated.
    2. Return a list with the latex of conclusions that are not explicitly mentioned.

Example 1:
    conclusions: [{{
        "name": "Definition1.1.1-asasjh5211",
        "text": "f attains both a maximum and a minimum value in [a, b], i.e., there exist c1 and c2 in [a, b] such that f(c1) is the maximum and f(c2) is the minimum."
        }},
        {{
        "name": "Definition1.2.3-sdniaje122",
        "text": "If f is differentiable on the open interval (a, b), then the points where the derivative f'(x) vanishes or does not exist are critical points of f. These points can be maxima, minima, or points of inflection."
        }}]
    query: "\\begin{{theorem}}[Extreme Value Theorem]
        Let \\( f: [a, b] \\to \\mathbb{{R}} \\) be a function continuous on the closed and bounded interval \\( [a, b] \\).
        
        \\begin{{enumerate}}
            \\item Existence of extreme values: \\( f \\) attains both a maximum and a minimum value in \\( [a, b] \\), i.e., there exist \\( c_1 \\) and \\( c_2 \\) in \\( [a, b] \\) such that \\( f(c_1) \\) is the maximum and \\( f(c_2) \\) is the minimum.
            
            \\item Critical points: If \\( f \\) is differentiable on the open interval \\( (a, b) \\), then the points where the derivative \\( f'(x) \\) vanishes or does not exist are critical points of \\( f \\). These points can be maxima, minima, or points of inflection.
            
            \\item Characterization of extremum: If \\( f \\) attains a local maximum or minimum at a critical point \\( c \\) in the interval \\( (a, b) \\), then \\( f'(c) = 0 \\) or the derivative does not exist at \\( c \\).
        \\end{{enumerate}}
    \\end{{theorem}}"
    
    Missing:["If f attains a local maximum or minimum at a critical point c in the interval (a, b), then f'(c) = 0 or the derivative does not exist at c."]

    
Example 2:
    
    conclusions: [
    {{
        "text":"$$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$",
        "name":"Unamed1.1.1.3-ssaafs33421"
    }},
    ]
    query: "Diremos que una función $$f:\\mathbb{{R}}\\to B\\subseteq\\mathbb{{R}}$$ es derivable en $x\\in A$ con derivada $f'(x)$ si $$\\exists\\lim_{{h\\to 0}} \\frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    
    Missing:[]

END OF EXAMPLES
# User Preamble
## Task and Context

conclusions:
{conclusions}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of conclusions that you identify in the text. You should return an empty list if it is an independent statement. The list of conclusions you identify must be formatted as a list of strings with conclusions' latex expressions, for example:
Missing:```json
["$f(a)f(b)<0$" ]<|END_OF_TURN_TOKEN|><|START_OF_TURN_TOKEN|>Hypothesis:
"""

promptStatementConclusionsChecking = PromptTemplate(
    template= catchAllConclusions,
    input_variables=["statement","conclusions"],
    input_types={
        "statement": str,
        "conclusions": List[Mapping[str,str]]
    },
    partial_variables={"format_instructions": StringListParser.get_format_instructions()}

)





promptIIFTemplate = """Task Description:
You are an AI agent specialized in processing mathematical statements, determining if they represent a bi-conditional (if and only if) relationship.
If a statement is a bi-conditional, answer yes.
If the statement is not a bi-conditional, answer no.
Input:
    Mathematical statement as a string.
Output:
    One string indicating:
    If bi-conditional:
        Yes
    If not bi-conditional:
        No
Examples:

    Bi-conditional Statement Example:

    Input: "A triangle with sides a,b,c where c is the biggest is a right triangle if and only if $a^2 + b^2 = c^2$"
    Output:Yes
                                            
    Statement with only one implication Example:

    Input: "If x is an even integer, then x^2 is an even integer."
    Output:No

    Input: "f(x) is derivable  \implies f(x) is continuous."
    Output:No

Format Instruction:
JUST RETURN THE ANSWER
Query:
    Only answer to this query, don't include any explanations
    Input: {input}
    output:"""

promptIIF =  PromptTemplate(
    template= promptIIFTemplate,
    input_variables=["input"],
    input_types={
        "input": str
    }
)


