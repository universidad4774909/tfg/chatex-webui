import re
from typing import List, Tuple

class LLMOutputFixer:
    # Regular expression patterns and substitutions
    llamaEnd_pattern = r"<\|im_end\|>"  # Match llama special end token
    json_pattern = r".*```json\n+((.|\n)+)```\n*"  # Remove configuration for code block interfaces
    back_pattern = r"(?<!\\)(\\+)(?!\\)"  # Match consecutive backslashes
    dollarScaped_pattern = r"(?<!\\)\\+\$"  # Match escaped dollar signs
    missingComma_pattern = r"(?<!\")\"\n\"(?!\")"  # Match consecutive strings without a comma
    badStrList_pattern = r"\[(\n|\ )*\{((\"[^\"]*\",(\n|\ )*)*\"[^\"]*\"(\n|\ )*(\n|\ )*)\}\]"  # Match lists with braces in strings
    badStart_pattern = r"^(Missing|Hypothesis|Hypotheses|Conclusions):\ *\["  # Match malformed beginning of String List Prompt
    
    # List of patterns and substitutions
    patterns_subs = [
        (llamaEnd_pattern, ''),  # Remove llama end token
        (json_pattern, r"\g<1>"),  # Remove the json block indicators but keep the content
        (back_pattern, r"\\\\"),  # Double unescaped backslashes
        (dollarScaped_pattern, "$"),  # Remove the escaping for dollar signs
        (missingComma_pattern, "\",\n\""),  # Add missing commas between strings
        (badStrList_pattern, r"[\g<2>]"),  # Fix bad lists in JSON
        (badStart_pattern, "[")  # Correct malformed list beginnings
    ]

    @staticmethod
    def substitute_regex(patterns_subs: List[Tuple[str, str]], text: str) -> Tuple[str, bool]:
        """
        Applies a list of regex substitutions to a string.

        Args:
            patterns_subs: List of tuples containing regex patterns and their replacements.
            text: The string to process.

        Returns:
            A tuple containing the modified string and a boolean indicating if changes were made.
        """
        result = text
        changes_made = False
        for pattern, substitution in patterns_subs:
            new_result = re.sub(pattern, substitution, result)
            if new_result != result:
                changes_made = True
            result = new_result
        return result, changes_made

    @staticmethod
    def close_unbalanced_tags(text: str) -> str:
        """
        Ensures all opened tags in the text are properly closed.
        If a tag is opened multiple times without being closed, the previous ones will be closed.

        Args:
            text: The string to process for unbalanced tags.

        Returns:
            The text with all unbalanced tags closed.
        """
        # Regular expression to match HTML-like tags (e.g., <tag> or </tag>)
        tag_pattern = r"<(/?)(\w+)>"

        # Stack to track open tags
        open_tags = []

        def replacement(match):
            tag_type, tag_name = match.groups()
            if tag_type == "":  # Opening tag
                if tag_name in open_tags:
                    # Close the previous instance of this tag if it's already open
                    open_tags.remove(tag_name)
                    return f"</{tag_name}>"
                open_tags.append(tag_name)
                return f"<{tag_name}>"
            else:  # Closing tag
                if tag_name in open_tags:
                    open_tags.remove(tag_name)
                    return f"</{tag_name}>"
                else:
                    return ""  # Ignore closing tags that don't have an opening tag

        # Apply the replacement function to all tags in the text
        fixed_text = re.sub(tag_pattern, replacement, text)

        # Close any remaining unclosed tags
        while open_tags:
            fixed_text += f"</{open_tags.pop()}>"

        return fixed_text

    

    @staticmethod
    def fix_output(text: str) -> str:
        """
        Fixes common errors in LLM output using predefined regex patterns and ensures that
        unbalanced tags are closed, while closing repeated tags without removing them.

        Args:
            text: The LLM-generated output string to be fixed.

        Returns:
            The fixed string.
        """
        # Apply the regular expression patterns and substitutions
        fixed_text, _ = LLMOutputFixer.substitute_regex(LLMOutputFixer.patterns_subs, text)

        # Fix repeated and unbalanced tags
        fixed_text = LLMOutputFixer.close_unbalanced_tags(fixed_text)

        # Handle specific custom rules like removing "<output>" tag
        if fixed_text.endswith("<output>"):
            fixed_text = fixed_text[:-len("<output>")] + "</output>"

        return fixed_text


if __name__ == "__main__":
    # Sample complex LLM output with multiple issues
    complex_llm_output = """
    <output>
    {
        "data": [
            {"name": "Example 1"},
            {"name": "Example 2"}
        ]
    }<|im_end|>
    ```json
    {
        "key": "value",
        "description": "This is a test with some \backslashes\\ and an escaped dollar sign \\$ and missing commas"
        "extra": "Another value"
    }
    ```
    Hypothesis: [
        "First hypothesis
        "Second hypothesis"
    ]
    <output>
    """

    print("Original LLM Output:\n")
    print(complex_llm_output)

    # Use LLMOutputFixer to clean the output
    fixed_llm_output = LLMOutputFixer.fix_output(complex_llm_output)

    print("\nFixed LLM Output:\n")
    print(fixed_llm_output)