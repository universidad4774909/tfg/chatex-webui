import json
import logging
import re
import shortuuid
import time
import urllib.parse
import random
from typing import List, Tuple
from functools import wraps
from langchain_core.output_parsers import StrOutputParser
from .cotChains import cot_reflection
from .llmOutputErrorFixer import LLMOutputFixer
from .symbolSubstituter import MathSymbolSubstituter
from .chains import llm2 as llm
from .cotPromptTemplatesLlamaModels import promptStatementConclusionsCoT, promptStatementHypothesesCoT, promptStatementSymbolsCoT
from .URIs import (
    BASE_URI, HAS_LABEL_DP_URI, HAS_SYMBOL_OP_URI, IMPLIES_THAT_OP_URI, 
    LATEX_DP_URI, REPRESENTS_OP_URI, SYMBOL_URI, ASSUMES_THAT_OP_URI
)
from .models import MathematicalStatementModel
from .chains import (
    IdentifyIIFStatementsChain, checkStatementConclusionsChain, 
    checkStatementHypothesisChain, parseGeneralSymbols, chainIdentifyStatement, 
    parseStatementHypothesisChain, parseStatementConclusionsChain
)
from .ontologyWrapper import RDFOntologyUtils
from .nucliaDB import VectorKB
from langchain_core.exceptions import OutputParserException

# Set up logging
logging.basicConfig(level=logging.INFO)


# File paths
rdf_file_path = "mutableLatexOntology.rdf"

# Function to create mutable copies of files
def create_mutable_files(files):
    """Creates mutable copies of the given files."""
    for filename in files:
        with open(filename, "rb") as f:
            name, extension = tuple(filename.split("."))
            with open(f"mutable{name.title()}.{extension}", "wb") as mutable_file:
                mutable_file.write(f.read())

# Initialize ontology and KB
ontology_utils = RDFOntologyUtils(rdf_file_path)
ontology_kb = VectorKB('localhost', 8080, class_kb="latex_classes", individuals_kb="latex_individuals", reset_kb=False)

# Retrieve mathematical objects from ontology
mathematicalObject = ontology_utils.onto['Object']
mathematicalObjectsURI = ontology_utils.onto.get_instances_of(mathematicalObject)
mathematicalObjectsNames = [c.name for c in mathematicalObjectsURI]

# Regular expression patterns for string substitution
json_pattern = r".*```json\n+((.|\n)+)```\n*"  # Remove configuration for code block interfaces
code_pattern = r".*```\n*((.|\n)+)```\n*"  # Remove configuration for code block interfaces
back_pattern = r"(?<!\\)(\\+)(?!\\)"  # Match consecutive backslashes
dollarScaped_pattern = r"(?<!\\)\\+\$"  # Match escaped dollar signs
missingComma_pattern = r"(?<!\")\"\n\"(?!\")"  # Match consecutive strings without a comma
badStrList_pattern = r"\[(\n|\ )*\{((\"[^\"]*\",(\n|\ )*)*\"[^\"]*\"(\n|\ )*(\n|\ )*)\}\]"  # Match lists with braces in strings
badStart_pattern = r"^(Missing|Hypothesis|Hypotheses|Conclusions):\ *\["  # Match malformed beggining of String List Prompt

# List of patterns and substitutions
patterns_subs = [
    (json_pattern, "\g<1>"), (back_pattern, r"\\\\"), (dollarScaped_pattern, "$"), 
    (missingComma_pattern, "\",\n\""), (badStrList_pattern, r"[\g<2>]"),
    (badStart_pattern,"["), (code_pattern, "\g<1>")
]



def preProcessStatement(statement):
    try:
        symbols = cot_reflection(llm, promptStatementSymbolsCoT, StrOutputParser(), {"statement":statement}).output
        symbolsFixed = LLMOutputFixer.fix_output(symbols)
        symbolsParsed = json.loads(symbolsFixed)
        symbolSubstituter = MathSymbolSubstituter(symbolsParsed)
        preparsedStatement = symbolSubstituter.substitute(statement)
        return preparsedStatement  
    except Exception as e:
        print(e)
    return statement


# Retry decorator for handling exceptions
def retry(max_retries=5, initial_backoff=0.1):
    """
    Decorator to retry a function in case of an exception, with exponential backoff.
    
    Args:
        max_retries: Maximum number of retries.
        initial_backoff: Initial backoff time in seconds.
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            retries = 0
            while retries < max_retries:
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    logging.error(f"Error: {e}. Retrying in {initial_backoff * 2 ** retries} seconds.")
                    time.sleep(initial_backoff * 2 ** retries)
                    retries += 1
            raise Exception("Could not complete the operation after several retries")
        return wrapper
    return decorator

# Retry decorator for adding individuals and triples to the ontology
@retry(max_retries=5, initial_backoff=0.1)
def add_individual(uri, rdf_type):
    """Adds an individual to the ontology."""
    ontology_utils.add_individual(uri, rdf_type)

@retry(max_retries=5, initial_backoff=0.1)
def add_triple(subj, pred, obj, dataProperty=False):
    """Adds a triple to the ontology."""
    ontology_utils.add_triple(subj, pred, obj, data=dataProperty)

# Function to add a mathematical statement to the ontology
def add_statement(statement: MathematicalStatementModel, labels, parentClass):
    """
    Adds a mathematical statement to the ontology.
    
    Args:
        statement: MathematicalStatementModel containing the statement data.
        labels: List of labels associated with the statement.
        parentClass: The class of the statement (e.g., Definition, Theorem).
        
    Returns:
        The URI of the added statement.
    """
    key = shortuuid.uuid()
    statement_uri = urllib.parse.quote(f"{statement.name}-{key}")

    @retry(max_retries=5, initial_backoff=0.1)
    def add_resource():
        preprocessedStatement = preProcessStatement(statement.statement)
        ontology_kb.add_resource(ontology_kb.individuals_kb, key, statement.name, preprocessedStatement, labels=labels+[statement_uri])

    try:
        if parentClass in ["Definition", "UndefinedStatement"]:
            add_resource()
    except Exception as e:
        logging.error(e)
        return None

    symbols_uris = set()

    try:
        add_individual(statement_uri, parentClass)
    except Exception as e:
        logging.error(e)

    try:
        add_triple(statement_uri, LATEX_DP_URI, statement.statement, dataProperty=True)
    except Exception as e:
        logging.error(e)

    for l in labels:
        try:
            add_triple(statement_uri, HAS_LABEL_DP_URI, l, dataProperty=True)
        except Exception as e:
            logging.error(e)

    for symbol in statement.symbols:
        symbol_uri = urllib.parse.quote(f"{BASE_URI}{symbol.represented.lower()}{symbol.represents.title()}")
        symbols_uris.add(symbol_uri)

        try:
            add_individual(symbol_uri, SYMBOL_URI)
        except Exception as e:
            logging.error(e)

        try:
            add_triple(symbol_uri, REPRESENTS_OP_URI, f"{BASE_URI}{symbol.represents}")
        except Exception as e:
            logging.error(e)

        try:
            add_triple(statement_uri, HAS_SYMBOL_OP_URI, symbol_uri)
        except Exception as e:
            logging.error(e)

    try:
        ontology_utils.save_changes()
    except Exception as e:
        logging.error(e)

    return statement_uri

# Helper function to get the key of a dictionary by its value
def get_key_by_value(my_dict, target_value):
    """Returns the key corresponding to a given value in a dictionary."""
    for key, value in my_dict.items():
        if value == target_value:
            return key
    return None

# Function to get similar statements from the ontology
@retry(initial_backoff=1)
def getSimilarStatements(statement, topK=10, currentFile="", confidence=0.6):
    """
    Finds similar statements in the ontology.
    
    Args:
        statement: The statement to compare.
        topK: Number of top similar statements to retrieve.
        currentFile: Current file context.
        confidence: Minimum confidence score for similarity.
        
    Returns:
        A list of similar statements.
    """
    definitions = ontology_kb.find_most_similar(ontology_kb.individuals_kb, preProcessStatement(statement), topk=8, confidence_score=confidence, labels=[currentFile])
    return definitions

# Function to apply regex substitutions to a string
def substitute_regex(patterns_subs: List[Tuple[str,str]], cadena: str):
    """
    Applies a list of regex substitutions to a string.
    
    Args:
        patterns_subs: List of tuples containing patterns and their substitutions.
        cadena: The string to modify.
        
    Returns:
        A tuple containing the modified string and a boolean indicating if changes were made.
    """
    result = cadena
    for pattern, substitution in patterns_subs:
        result = re.sub(pattern, substitution, result)
    return result, result != cadena


def extractIds(statementList: List[str]):
    anonymousStatements = []
    pattern = r"((D|d)efinition|(U|u)ndefinedStatement)(\s|%20)*\d+(\.\d+)*-{0,1}[a-zA-Z0-9]*"
    extracted_ids = set()
    for statement in statementList:
        match = re.search(pattern, statement)
        if match:
            extracted_ids.add(match.group(0))
        else:
            anonymousStatements.append(statement)
    return extracted_ids,anonymousStatements

# Function to extract conclusions and hypothesis from a statement
def extract_conclusions_and_hypothesis(symbols, statementText, similarStatements, statementToName, parentClass):
    max_retries = 5
    retry_delay = 0.5  # seconds

    def execute_cot_reflection(prompt_template, input_vars):
        """Utility function to execute CoT reflection with retries."""
        for attempt in range(max_retries):
            try:
                result = cot_reflection(
                    llm=llm,
                    prompt_template=prompt_template,
                    output_parser=StrOutputParser(),
                    input_vars=input_vars,
                    verbose=True
                )
                return json.loads(result.output)
            except json.decoder.JSONDecodeError as e:
                print(f"Error in CoT reflection attempt {attempt + 1}/{max_retries}: {e}")
                new_str, changed = substitute_regex(patterns_subs, result.output)
                if changed:
                    try:
                        return json.loads(new_str)
                    except json.decoder.JSONDecodeError as e2:
                        print(f"JSON decoding error after regex substitution: {e2}")
                time.sleep(retry_delay)
        return []

    # Parse hypotheses
    hypothesis_input_vars = {
        "definitions": json.dumps([{'statement': ss['text'], 'name': statementToName[ss['text']]} for ss in similarStatements if ss['text'] in statementToName and ss['text'] != statementText]),
        "statement": statementText
    }
    hypothesis = execute_cot_reflection(promptStatementHypothesesCoT, hypothesis_input_vars)

    # Parse conclusions if the statement is not a definition
    conclusions = []
    if parentClass != "Definition":
        conclusions_input_vars = hypothesis_input_vars
        conclusions = execute_cot_reflection(promptStatementConclusionsCoT, conclusions_input_vars)

    hyptoheses,anonymousHypotheses = extractIds(hypothesis)
    conclusions, anonymousConclusions = extractIds(conclusions)

    return hyptoheses, anonymousHypotheses, conclusions, anonymousConclusions


# Function to process child statements within a parent statement
def processChildrenStatements(statement_instance, statementList, anonymousStatementList, statement_uri, objective_objects, statement_name, labels, statement_to_name, relation=IMPLIES_THAT_OP_URI, print_word="implies"):
    if any(isinstance(x, dict) for x in statementList):
        new_statements = []
        for s in statementList:
            if isinstance(s, str):
                new_statements.append(s)
            elif isinstance(s, dict) and "statement" in s:
                new_statements.append(s["name"])
            elif isinstance(s, dict) and any(isinstance(x, str) for x in s.values()):
                longest_value = max(s.values(), key=lambda x: len(x))
                new_statements.append(longest_value)
        statementList = new_statements.copy()

    if any(not isinstance(x, str) for x in statementList):
        print("Malformed statement")

    print(f"{statement_instance.name} {print_word} {', '.join(statementList)}")

    statements_parsed = []

    if len(statementList) == 0:
        print("Something is wrong")
    else:
        
        for s in statementList:
            encoded_statement = urllib.parse.quote(s)
            ontology_utils.add_triple(statement_uri, relation, encoded_statement)
            statements_parsed.append(s)

    if len(anonymousStatementList) == 0:
        print("Something is wrong")
    else:
        for s in anonymousStatementList:
            if isinstance(s, str):
                print("Undefined Statement Alert ", s)
                statement_number = anonymousStatementList.index(s)
                undefined_statement_instance, _, _ = processGeneralStatement(
                    s, f"{statement_name}.{statement_number}", ["UndefinedStatement"], labels, statement_to_name
                )
                ontology_utils.add_triple(statement_uri, relation, undefined_statement_instance.name)
                statements_parsed.append(undefined_statement_instance.name)


    return statements_parsed



def processGeneralStatement(statementText: str, statementName:str, objectiveObjects: List[str], labels: List[str] = [],statementToName: dict={}) -> MathematicalStatementModel:
    start_time = time.time()  # Record start time
    if isinstance(objectiveObjects,str):
        objectiveObjects = [objectiveObjects]
    if len(objectiveObjects)>1:
        #Cadena que decide el tipo de objeto a usar
        objectiveObject = None
        iterations = 0
        parsedPossibilities = ', '.join([f"`{c}`" for c in objectiveObjects])
        while not objectiveObject and iterations<10:
            result = chainIdentifyStatement.invoke({
                "text": statementText,
                "statementTypes": parsedPossibilities
            })
            if result in objectiveObjects:
                objectiveObject = result
            iterations+=1
        
    elif len(objectiveObjects)==1:
        objectiveObject = objectiveObjects[0]
    else:
        objectiveObject = None
    if objectiveObject!=None:
        symbols = getGeneralSymbols(statementText,mathematicalObjectsURI)
        print(symbols)
        
        if objectiveObject=="UndefinedStatement":
                almostSameStatement = getSimilarStatements(statementText,currentFile=labels[-1],topK=1,confidence=0.95)
                if len(almostSameStatement)>0:
                    referencedStatement = almostSameStatement[0]
                    print(referencedStatement)
                    return MathematicalStatementModel(name=referencedStatement["labels"][-1], statement=referencedStatement["text"],symbols=symbols), None, None
                
        statementInstance = MathematicalStatementModel(name=f"{objectiveObject} {statementName}",statement=statementText,symbols=symbols)
        
        
        similarStatements = getSimilarStatements(statementInstance.statement,currentFile=labels[-1])
        statementToName = {s["text"]:s["labels"][-1] for s in similarStatements}
        
        statement_uri = add_statement(statementInstance,labels,objectiveObject )
        statementInstance.name = statement_uri
        if objectiveObject=="UndefinedStatement":
            return statementInstance, [], []

        hypothesis, hypothesisAnonymous, conclusions, conclusionsAnonymous = extract_conclusions_and_hypothesis(symbols, statementInstance.statement, similarStatements, statementToName,objectiveObject)
        hypothesis = processChildrenStatements(statementInstance,hypothesis,hypothesisAnonymous, statement_uri, objectiveObjects,statementName,labels,statementToName,relation=ASSUMES_THAT_OP_URI, print_word="assumes")
        conclusions = processChildrenStatements(statementInstance,conclusions,conclusionsAnonymous, statement_uri, objectiveObjects,statementName,labels,statementToName,relation=IMPLIES_THAT_OP_URI,print_word="implies")
        
        
        iif = False and objectiveObject in ["Theorem","Corollary", "Lemma"] and IIFStatement(statementText)
        if iif:
            # If iif we create the other implication
            invertedStatementUri = add_statement(MathematicalStatementModel(name=statementInstance.name+"-Inverted",statement=statementText,symbols=symbols),labels,objectiveObject)
            for h in hypothesis:
                ontology_utils.add_triple(invertedStatementUri,IMPLIES_THAT_OP_URI,urllib.parse.quote(h))
            for c in conclusions:
                ontology_utils.add_triple(invertedStatementUri,ASSUMES_THAT_OP_URI,urllib.parse.quote(c))


        try:
            ontology_utils.save_changes()
            ontology_utils.reason()
            ontology_utils.onto.inconsistent_classes()
            ontology_utils.save_changes()
        except Exception as e:
            print(e)
        # ontology_utils.save_changes()
        elapsed_time = time.time() - start_time  # Calculate elapsed time
        print(f"Time elapsed for parsing statement: {elapsed_time} seconds")
        if "1.2.3.3" in statementInstance.name:
            print("para")
        return statementInstance, hypothesis, conclusions
    return None
# conclusions = parseDefinitionHypothesisChain.invoke({
#     "symbols": symbols,
#     "statement": d,
#     "previousDefinitions": json.dumps(prevDefinitions)
# })
# print(f" implies {', '.join(conclusions)}")

def getGeneralSymbols(statementText, mathematicalObjectsURI, max_retries=1):
    result = []
    classes_uri = [c.name for c in mathematicalObjectsURI]
    retries = 0
    while len(result) == 0 and retries < max_retries:
        try:
            result = parseGeneralSymbols.invoke({
                "query": statementText,
                "mathematicalObjects": json.dumps(classes_uri)
            })
        except OutputParserException as e:
            import re
            print(e)
            regex = r"```json\n+((.|\n)+)```"
            new_str, changed = substitute_regex(patterns_subs, e.llm_output)
            if changed:
                try:
                    result = json.loads(new_str)
                except:
                    print(e)
                    retries += 1
                    continue
            else:
                print(e)
                retries += 1
                continue
        valid_symbols = []
        invalidPatterns = [r"\$", r"^\\+$"]
        import re
        for symbol in result:
            if "represents" in symbol and isinstance(symbol["represents"],str) and "represented" in symbol and symbol["represents"].lower() in mathematicalObjectsNames and not any([bool(re.match(patt,symbol["represented"])) for patt in invalidPatterns]):
                valid_symbols.append({
                    "represents": symbol["represents"].lower(),
                    "represented":symbol["represented"]
                })
            else:
                print(f"Symbol malo {symbol}")
        result = valid_symbols
        retries += 1
    return result


@retry(max_retries=5, initial_backoff=0.1)
def IIFStatement(tagContent):
    result = IdentifyIIFStatementsChain.invoke({"input":tagContent}).strip().lower()
    print("IIF Original:")
    print(result)
    if isinstance(result,list) and len(result)>0 and isinstance(result[0],str):
        result = result[0]
    if isinstance(result,str) and not result in ["yes","no"]:
        new_str, changed = substitute_regex(patterns_subs, result)
        if changed:
            try:
                result = json.loads(new_str)
            except Exception as e:
                print(e)
        
    if isinstance(result,list) and "yes" in result and not "no" in result:
        result = "yes"
    elif isinstance(result,list) and "no" in result and not "yes" in result:
        result = "no"
    if result in ["yes","no"]:
        return result=="yes"
    if isinstance(result,str) and "yes" in result:
        return True
    else:
        return False
    

if __name__=="__main__":

    statementText = """\\begin{definition}[Continuidad]
        We say that a function \\( f: \\mathbb{R} \\rightarrow B \\subseteq \\mathbb{R} \\) is continuous if \\( \\lim_{{x \\to a}} f(x) = f(a) \\).
    \\end{definition}"""
    statementUri,_,_= processGeneralStatement(statementText, "testDef",["Definition"],["text"]) 

    statementText = """\\begin{teorema}[Teorema de Bolzano]
    Sea $f: [a,b] \\to \\mathbb{R}$\\\\
    $$
    \\begin{cases}
        \\text{ f is a continuous function} \\\\
        $f(a)f(b)<0$ 
    \\end{cases}
    $$
    $\\implies$
    $$
    \\begin{cases}
        \\exists c \\in [a,b] : f(c)=0
    \\end{cases}
    $$
\\end{teorema}"""
    result,_,_ =processGeneralStatement(statementText, "test1", ["Theorem"],labels=["prueba"])
    print(result)