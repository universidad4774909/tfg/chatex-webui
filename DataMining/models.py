from langchain.pydantic_v1 import BaseModel, Field
from typing import List, Dict, Mapping, ClassVar
#from owlready2 import *

class Thing():
    pass

class ObjectProperty():
    pass

class DataPropertyModel(BaseModel):
    name: str = Field(description="Name of the property")
    value: str = Field(description="Value of he property")
    type: str = Field(description="Type of the property")



class MathematicalObject(BaseModel,Thing):
    type: str = Field(description="Type of object")
    dataProperties: List[DataPropertyModel] = Field(description="Data Properties of the Object")


class SymbolModel(BaseModel):
    represented: str = Field(description="Representation in Latex")
    represents: str | int | float = Field(description="Kind of the MathematicalObject that represents")


class SymbolListModel(BaseModel):
    __root__: List[SymbolModel]

    def __init__(self, *args, **kwargs):
        super().__init__(__root__=args, **kwargs)

    def __getitem__(self, key):
        return self.__root__[key]

    def __iter__(self):
        return iter(self.__root__)

    def __next__(self):
        return next(self.__root__)
    
class StringListModel(BaseModel):
    __root__: List[str]

    def __init__(self, *args, **kwargs):
        super().__init__(__root__=args, **kwargs)

    def __getitem__(self, key):
        return self.__root__[key]

    def __iter__(self):
        return iter(self.__root__)

    def __next__(self):
        return next(self.__root__)




class ParseStatementInput(BaseModel):
    text: str = Field(description="text containing only a mathematical statement")
    symbols: Mapping[str,SymbolModel] = Field(description="text containing only a mathematical statement")
    
class MathematicalStatementModel(BaseModel):
    name: str = Field(description="Name of the theorem, if unnamed use Index number like Teorema 2.1")
    statement: str = Field(description="Latex form of the theorem")
    symbols: List[SymbolModel] = Field(description="Symbols involved in the statement")


class MathematicalStatementListModel(BaseModel):
    __root__: List[str]

    def __init__(self, *args, **kwargs):
        super().__init__(__root__=args, **kwargs)

    def __getitem__(self, key):
        return self.__root__[key]

    def __iter__(self):
        return iter(self.__root__)

    def __next__(self):
        return next(self.__root__)
    