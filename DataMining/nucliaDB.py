
import json
from typing import List
from nucliadb_sdk import get_or_create, delete_kb
from nucliadb_sdk.knowledgebox import KnowledgeBox
from sentence_transformers import SentenceTransformer

class VectorKB():
    def __init__(self, host, port, embedding_model = 'BAAI/bge-m3', class_kb = "class_kb", individuals_kb = "individuals_kb", reset_kb=False):
        if reset_kb:
            try:
                delete_kb(class_kb,nucliadb_base_url=f"http://{host}:{port}")
            except:
                pass
            try:
                delete_kb(individuals_kb,nucliadb_base_url=f"http://{host}:{port}")
            except:
                pass
        self.class_kb = get_or_create(class_kb,nucliadb_base_url=f"http://{host}:{port}")
        self.individuals_kb = get_or_create(individuals_kb,nucliadb_base_url=f"http://{host}:{port}")
        self.embedding_model = embedding_model
        


    def add_resource(self,my_kb:KnowledgeBox,key:str, title:str, text: str, labels: List[str | float | int]):
        model = SentenceTransformer(self.embedding_model)
        vectors = model.encode([text])
        # Upload the document to the knowledge base
        a = my_kb.upload(
            key=key,
            text=text,
            labels=labels,
            vectors={"bge": vectors[0]},
        )
        #print(a)
        """ndb.create_resource(kbid=kbid, content=content)(
                key=f"mykey{i}",
                text=file.page_content,
                labels=[file.metadata["source"],f"chunk-{i}"],
                vectors={"bge": vectors[0]},
            )
        """
    def find_most_similar(self, my_kb: KnowledgeBox, input, knowledge_base="individuals_kb", confidence_score=0.6, topk=5,labels=[]):
        model = SentenceTransformer(self.embedding_model)
        query_vectors = model.encode([input])
        results = my_kb.search(
            vector=query_vectors[0],
            vectorset="bge",
            min_score=confidence_score,
            page_size=topk,filter=labels)
        data = [{"key": result.key,"text": result.text, "score": result.score, "labels": result.labels} for result in results]
        return data

if __name__=="__main__":
    prueba = VectorKB('localhost',8080,class_kb="latex_classes", individuals_kb="latex_individuals", reset_kb=True)
    # prueba.add_resource(prueba.individuals_kb,"key-2", 'Prueba 2', "Texto2", ["3","page 6"])
    # print(json.dumps(prueba.find_most_similar(prueba.individuals_kb, "Texto1", confidence_score=0.01, topk=5),indent=2))