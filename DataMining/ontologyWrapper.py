import functools
import time
from rdflib import OWL, Graph, URIRef, Literal, RDF, RDFS, Namespace
from rdflib.plugins.sparql import prepareQuery
import owlready2 as owr
owr.JAVA_EXE = "C:\\Program Files\\Java\\jre-1.8\\bin\\java.exe"


def retry_fixed(max_attempts=3, delay=1):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            attempt = 0
            while attempt < max_attempts:
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(f"Error {func.__name__}: {e}. Retrying in {delay} seconds...")
                    time.sleep(delay)
                    attempt += 1
            print("Max attempts reached. Giving up.")
        return wrapper
    return decorator


class RDFOntologyUtils:
    def __init__(self, rdf_file, sqlite_file="sqlite.db"):
        """
        Initialize the RDFOntologyUtils object with the RDF file.
        
        Args:
        - rdf_file (str): Path to the RDF file.
        """
        self.rdf_file = rdf_file
        self.rdf_graph = Graph()
        self.rdf_graph.parse(rdf_file, format='xml')
        self.owl_file = rdf_file
        self.onto =  owr.get_ontology(f"file://{rdf_file}").load()
        #self.rdf_graph = self.onto.graph
        

    def get_classes(self):
        """
        Get all the classes defined in the ontology.
        
        Returns:
        - list: A list of URIs representing classes in the ontology.
        """
        return list(set(self.rdf_graph.subjects(predicate=RDF.type, object=URIRef('http://www.w3.org/2002/07/owl#Class'))))
    def get_subclasses(self, class_uri):
        """
        Retrieve all subclasses of the given class URI in the ontology.
        
        Args:
        - class_uri (str): URI of the class for which to find subclasses.
        
        Returns:
        - list: List of subclasses URIs.
        """
        # Define the namespaces used in the ontology
        rdf = Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
        rdfs = Namespace("http://www.w3.org/2000/01/rdf-schema#")

        # Define the SPARQL query to retrieve subclasses
        query = prepareQuery(
            """
            SELECT ?subClass
            WHERE {
                ?subClass rdfs:subClassOf ?class .
            }
            """,
            initNs={"rdf": rdf, "rdfs": rdfs}
        )

        # Execute the query and collect results
        subclasses = []
        if type(class_uri)!= URIRef:
            class_uri = URIRef(class_uri)
        for row in self.rdf_graph.query(query, initBindings={"class": class_uri}):
            subSubClasses = self.get_subclasses(row.subClass)
            subclasses += subSubClasses
            subclasses.append(row.subClass)

        return subclasses
    def get_object_properties(self):
        """
        Get all the Object Properties defined in the ontology.
        
        Returns:
        - list: A list of URIs representing OP in the ontology.
        """
        return list(set(self.rdf_graph.subjects(predicate=RDF.type, object=URIRef('http://www.w3.org/2002/07/owl#ObjectProperty'))))

    def get_data_properties(self):
        """
        Get all the Data Properties defined in the ontology.
        
        Returns:
        - list: A list of URIs representing DP in the ontology.
        """
        return list(set(self.rdf_graph.subjects(predicate=RDF.type, object=URIRef('http://www.w3.org/2002/07/owl#DatatypeProperty'))))


    def get_properties(self):
        """
        Get all the properties defined in the ontology.
        
        Returns:
        - list: A list of URIs representing properties in the ontology.
        """
        return list(set(self.rdf_graph.predicates()))

    def reason(self):
        try:
            owr.sync_reasoner_pellet([self.onto], infer_property_values=True, infer_data_property_values=True, debug=2)
            return True
        except owr.base.OwlReadyInconsistentOntologyError as e:
            if len(list(self.onto.inconsistent_classes()))==0:
                return True
            else:
                print(e)
                return False

    def get_individuals(self):
        """
        Get all the properties defined in the ontology.
        
        Returns:
        - list: A list of URIs representing individuals in the ontology.
        """
        return list(set(self.rdf_graph.subjects(predicate=RDF.type, object=URIRef('http://www.w3.org/2002/07/owl#NamedIndividual'))))
    def get_individuals_by_class(self, class_uri):
        """
        Get individuals belonging to a specific class in the ontology using Owlready2.

        Args:
        - class_uri (str): URI of the class.

        Returns:
        - list: A list of individuals belonging to the specified class.
        """
        class_entity = self.onto[class_uri]
        individuals = list(class_entity.instances())
        return individuals

    def execute_sparql_query(self, query):
        """
        Execute a SPARQL query on the RDF graph.

        Args:
        - query (str): SPARQL query string.

        Returns:
        - rdflib.query.Result: Result of the SPARQL query.
        """
        result = self.rdf_graph.query(query)
        return result
    
    def add_baseURI(self,local:str):
        self.onto.base
        if "#" in local:
            return local
        else:
            return f"{self.onto.get_base_iri()}{local}"
    
    def get_dot(self, filename="temp.dot",return_code = True):
        from rdflib.tools.rdf2dot import rdf2dot

        with open(filename,"w", encoding="utf-8") as f:
            rdf2dot(self.rdf_graph,f)
        if return_code:
            with open(filename,"r", encoding="utf-8") as f:
                dot_code = f.read()
            return dot_code


    def add_individual(self, individual_uri, class_uri):
        """
        Add a named individual of a certain class to the RDF graph.

        Args:
        - individual_uri (rdflib.term.URIRef): URI of the individual.
        - class_uri (rdflib.term.URIRef): URI of the class.
        """
        try:
            if (type(individual_uri)==str):
                if not "#" in individual_uri:
                    individual_uri = URIRef(f"{self.onto.get_base_iri()}{individual_uri}")
                else:
                    individual_uri = URIRef(self.add_baseURI(individual_uri))
            if (type(class_uri)==str):
                if not "#" in class_uri:
                    class_uri = URIRef(f"{self.onto.get_base_iri()}{class_uri}")
                else:
                    class_uri = URIRef(self.add_baseURI(class_uri))

            
            self.rdf_graph.add((individual_uri, RDF.type, class_uri))

            parentClass = self.onto[class_uri.split("#")[-1]]
            individual = parentClass(individual_uri.split("#")[-1])



            return True
        except Exception as e:
            print(e)
            return False

    def get_class_hierarchy(self, class_uri):
        """
        Get the hierarchy of a given class in the ontology.
        
        Args:
        - class_uri (URIRef): URI of the class.
        
        Returns:
        - list: A list of URIs representing the hierarchy of the class.
        """
        hierarchy = set()
        for parent in self.rdf_graph.transitive_objects(subject=class_uri, predicate=URIRef('http://www.w3.org/2000/01/rdf-schema#subClassOf')):
            
            hierarchy.add(parent)
            if parent!=class_uri:
                hierarchy.update(self.get_class_hierarchy(parent))
        return list(hierarchy)

    def get_property_domain_range(self, property_uri):
        """
        Get the domain and range of a given property in the ontology.
        
        Args:
        - property_uri (URIRef): URI of the property.
        
        Returns:
        - tuple: A tuple containing domain and range URIs of the property.
        """
        domain = self.rdf_graph.value(subject=property_uri, predicate=RDFS.domain)
        range_ = self.rdf_graph.value(subject=property_uri, predicate=RDFS.range)
        return domain, range_

    def add_class(self, class_uri, superclass_uri=None):
        """
        Add a new class to the ontology.
        
        Args:
        - class_uri (URIRef): URI of the new class.
        - superclass_uri (URIRef): URI of the superclass. (Optional)
        """
        
        # owl2ready
        if superclass_uri:
            local_URI = superclass_uri.split("#")[-1]
            parentClass = self.onto[local_URI]
            if not parentClass:
                parentClass = owr.Thing
        else:
            parentClass = owr.Thing
        local_class_uri = class_uri.split("#")[-1]
        owr.types.new_class(local_class_uri,parentClass)
            
        # RDFlib
        self.rdf_graph.add((class_uri, RDF.type, OWL.Class))
        if superclass_uri:
            self.rdf_graph.add((class_uri, RDFS.subClassOf, superclass_uri))

    def add_property(self, property_uri, domain_uri, range_uri):
        """
        Add a new property to the ontology.
        
        Args:
        - property_uri (URIRef): URI of the new property.
        - domain_uri (URIRef): URI of the domain class of the property.
        - range_uri (URIRef): URI of the range class of the property.
        """
        self.rdf_graph.add((property_uri, RDF.type, RDF.Property))
        self.rdf_graph.add((property_uri, RDFS.domain, domain_uri))
        self.rdf_graph.add((property_uri, RDFS.range, range_uri))
    @retry_fixed(max_attempts=3, delay=0.1)
    def add_triple(self, subject_uri, predicate_uri, object_, subjectClassURI= None, objectClassURI= None, data=False,):
        """
        Add a new triple to the ontology.
        
        Args:
        - subject_uri (URIRef): URI of the subject.
        - predicate_uri (URIRef): URI of the predicate.
        - object_ (URIRef or Literal): URI of the object or Literal value.
        """
        print((subject_uri, predicate_uri, object_))

        if (type(subject_uri)==str):
            if not "#" in subject_uri:
                subject_uri = URIRef(f"{self.onto.get_base_iri()}{subject_uri}")
            else:
                subject_uri = URIRef(f"{subject_uri}")

        if (type(predicate_uri)==str):
            if not "#" in predicate_uri:
                predicate_uri = URIRef(f"{self.onto.get_base_iri()}{predicate_uri}")
            else:
                predicate_uri = URIRef(predicate_uri)

        if (type(object_)==str and not data):
            if not "#" in object_:
                object_ = URIRef(f"{self.onto.get_base_iri()}{object_}")
            else:
                object_ = URIRef(object_)
        
        subjectClass = self.onto[subject_uri.split("#")[-1]]
        objectClass = self.onto[object_.split("#")[-1]] if not data else object_
        
        # Create an object property instance
        object_property = self.onto[predicate_uri.split("#")[-1]]
        if None in [subjectClass, object_property, objectClass]:
            print("Invalid Class")
            return False
        else:
            object_property[subjectClass].append(objectClass)
            return True
        

    def save_changes(self):
        """
        Save the changes made to the ontology to a new RDF file.
        
        Args:
        - output_file (str): Path to the output RDF file.
        """
        """with open(self.rdf_file, 'w', encoding="utf-8") as f:
            f.write(self.rdf_graph.serialize(format='xml'))"""
        self.onto.save(file=self.rdf_file)
        

    def get_subgraph_with_properties(self, property_uris):
        """
        Get a subgraph containing only triples with specified object properties.
        
        Args:
        - property_uris (list): List of URIRef representing object properties.
        
        Returns:
        - rdflib.Graph: Subgraph containing only triples with specified object properties.
        """
        subgraph = Graph()
        for s, p, o in self.rdf_graph:
            if p in property_uris:
                subgraph.add((s, p, o))
        return subgraph
    
    def get_object_properties_dict(self, individual):
        """
        Get a dictionary where keys are properties and values are objects related to the given individual.
        
        Args:
        - individual (URIRef): URI of the individual.
        
        Returns:
        - dict: A dictionary where keys are properties and values are objects related to the given individual.
        """
        object_properties_dict = {}
        for predicate, object_ in self.rdf_graph.predicate_objects(subject=individual):
            if predicate not in object_properties_dict:
                object_properties_dict[predicate] = []
            object_properties_dict[predicate].append(object_)
        return object_properties_dict

    def get_subject_properties_dict(self, individual):
        """
        Get a dictionary where keys are properties and values are subjects related to the given individual.
        
        Args:
        - individual (URIRef): URI of the individual.
        
        Returns:
        - dict: A dictionary where keys are properties and values are subjects related to the given individual.
        """
        subject_properties_dict = {}
        for subject, predicate in self.rdf_graph.subject_predicates(object=individual):
            if predicate not in subject_properties_dict:
                subject_properties_dict[predicate] = []
            subject_properties_dict[predicate].append(subject)
        return subject_properties_dict

if __name__=="__main__":
    # Example usage:
    rdf_file_path = "latexOntology.rdf"
    ontology_utils = RDFOntologyUtils(rdf_file_path)

    classes = ontology_utils.get_classes()
    properties = ontology_utils.get_object_properties()
    individuals = ontology_utils.get_individuals()
    for cls in classes:
        print("Class:", cls)
        print("Hierarchy:", ontology_utils.get_class_hierarchy(cls))

    for prop in properties:
        print("Property:", prop)
        print("Domain and Range:", ontology_utils.get_property_domain_range(prop))
    
    for individual in individuals:
        print("Individual: ", individual)
        print("Subject of: ", ontology_utils.get_object_properties_dict(individual))
        print("Object of: ", ontology_utils.get_subject_properties_dict(individual))


    integer_class = URIRef("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#Integer")
    odd_class = URIRef("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#odd")
    even_class = URIRef("http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#even")
    disjoint_relation = URIRef("http://www.w3.org/2002/07/owl#disjointWith")
    ontology_utils.add_class(odd_class,integer_class)
    ontology_utils.add_class(even_class,integer_class)
    ontology_utils.add_triple(odd_class,disjoint_relation,even_class)
    ontology_utils.save_changes("modified_ontology.rdf")
    # Define the object properties of interest
    object_properties = properties[:2]
    print(list(map(lambda x: str(x),object_properties)))
    # Get the subgraph with specified object properties
    subgraph = ontology_utils.get_subgraph_with_properties(object_properties)

    # Print the triples in the subgraph
    for s, p, o in subgraph:
        print(f"Subject: {s}, Predicate: {p}, Object: {o}")
