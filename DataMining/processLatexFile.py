# -*- coding: utf-8 -*-
import importlib.util
import re

class LatexProcessor():

    def __init__(self, file_path: str, direct_text=False):
        if direct_text:
            self.fileContent = file_path
        else:
            with open(file_path,encoding="utf-8") as f:
                self.fileContent = f.read()

    def getHierarchy(self):
        hierarchy = {"outside": {}}
        chapter_pattern = r'(?ism)\\chapter\*?\s*\{([^\}]*)\}((.|\n)+?)(\\chapter|\\end\{document\})'  # match \chapter or \chapter*
        section_pattern = r'(?ism)\\section\*?\s*\{([^\}]*)\}((.|\n)+?)(\\chapter|\\section|\\end\{document\})'  # match \section or \section*
        subsection_pattern = r'(?ism)\\subsection\*?\s*\{([^\}]*)\}((.|\n)+?)(\\chapter|\\section|\\subsection|\\end\{document\})'  # match \subsection or \subsection*

        chapter_match = re.search(chapter_pattern, self.fileContent)
        if chapter_match:
            endOfChapter = 0
        while chapter_match:
            chapter_name = chapter_match.group(1).strip()
            print(f"Processing Chapter {chapter_name}:\n")
            
            hierarchy[chapter_name] = {}
            section_match = re.search(section_pattern, chapter_match.group(2)+"\\chapter")
            
            if section_match:
                endOfSection = 0
                while section_match:
                    section_name = section_match.group(1).strip()
                    
                    print(f"Processing Section {section_name}:\n")
                    hierarchy[chapter_name][section_name] = {}
                    subsection_match = re.search(subsection_pattern, section_match.group(2)+"\\section")
                    if subsection_match:
                        endOfSubsection = 0
                        while subsection_match:

                            subsection_name = subsection_match.group(1).strip()
                            print(f"Processing Subsection {subsection_name}:\n")
                            hierarchy[chapter_name][section_name][subsection_name] = subsection_match.group(2).strip()
                            endOfSubsection += subsection_match.end()-len(subsection_match.group(4))
                            subsection_match = re.search(subsection_pattern, section_match.group(2)[endOfSubsection:]+"\\section")
                    else: # No subsections
                        hierarchy[chapter_name][section_name] = section_match.group(2).strip()
                    endOfSection += section_match.end()-len(section_match.group(4))
                    section_match = re.search(section_pattern, chapter_match.group(2)[endOfSection:]+"\\chapter")
            else: # No sections
                hierarchy[chapter_name] = chapter_match.group(2).strip()


            endOfChapter += chapter_match.end()-len(chapter_match.group(4))
            chapter_match = re.search(chapter_pattern, self.fileContent[endOfChapter:], re.MULTILINE)
        outside_text = re.split(r'(?i)\\(chapter|section|subsection)\*?', self.fileContent, re.S)[0]
        hierarchy["outside"] = {"outside": []}
        for line in outside_text.splitlines():
            if line.strip():  # ignore empty lines
                hierarchy["outside"]["outside"].append((line.strip(), len(outside_text) - len(line)))

        return hierarchy 

    def findTag(self, tag):
        pattern = r'(?is)\\begin{'+tag+r'}((.|\n)*?)\\end{'+tag+r'}'
        found  = re.findall(pattern , self.fileContent, re.S)
        return found
    
    def listTags(self):
        pattern = r'(?is)\\begin{([^}]+)}'
        found  = re.findall(pattern , self.fileContent, re.S)
        return set(found)



if __name__ == "__main__":
    import json
    instance = LatexProcessor("ApuntesLatex/Mathematical Analysis/Mathematical Analysis.tex")
    print('tags'+ '\n'.join(instance.listTags()))
    #print(instance.findTag("definition"))
    hierarchy = instance.getHierarchy()
    with open("hierarchy.json", 'w', encoding='utf-8') as f:
        f.write(json.dumps(hierarchy, indent=2, ensure_ascii=False))