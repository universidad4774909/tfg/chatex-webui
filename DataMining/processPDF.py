
import os
from pdf2docx import Converter
def convert_pdf_to_docx(pdf_folder, docx_folder, filename):
    # Check if PDF file exists
    pdf_file = os.path.join(pdf_folder, f"{filename}.pdf")
    if not os.path.exists(pdf_file):
        raise FileNotFoundError(f"PDF file '{filename}' not found in '{pdf_folder}'")

    # Check if DOCX file already exists
    docx_file = os.path.join(docx_folder, f"{filename}.docx")
    if os.path.exists(docx_file):
        raise FileExistsError(f"DOCX file '{filename}.docx' already exists in '{docx_folder}'")

    # Convert PDF to Word
    cv = Converter(pdf_file)
    cv.convert(docx_file, start=0, end=None)
    cv.close()
    return docx_file
def convert_folder(input_folder, output_folder):
    files = os.listdir(input_folder)
    for i,file in enumerate(files):
        if file.endswith(".pdf"):
            try:
                route = convert_pdf_to_docx(input_folder,output_folder,file.split(".")[0])
                print(f"Succesfully converted {file}")
            except FileExistsError as e:
                print(f"{file} already converted")
            except FileNotFoundError as e:
                print(f"{file} deleted while converting")
            print(f"Processed: {round((i+1)/len(files),2)}%")
            
if __name__=="__main__":
    pdf_folder = "Apuntes"
    docx_folder = "ApuntesConvertidos"
    convert_folder(pdf_folder,docx_folder)
