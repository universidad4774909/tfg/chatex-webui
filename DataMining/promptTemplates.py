
from typing import List
from langchain_core.prompts import PromptTemplate
from .models import MathematicalStatementListModel, StringListModel, SymbolModel




promptRewriteStatement = PromptTemplate.from_template("""<start_of_turn>user\nActivity Prompt: Mathematical Statement Translation

Objective: Given mathematical statements of type {StatementType}, the task is to translate them into English using simplified LaTeX syntax, ensuring accuracy in mathematical notation and grammar.

Instructions:

Read Carefully: Review the provided mathematical statement along with any preceding or succeeding text for context.
Translate & Correct: Translate the statement into English, correcting any errors in accents or grammar.
Formulate Hypothesis & Conclusions: Identify the hypothesis and conclusions within the statement, rewriting them with the structure: If [hypothesis] then [conclusions] for [conditions].
Generate LaTeX Syntax: Use LaTeX syntax to represent the translated statement accurately, incorporating mathematical notation where appropriate.
                                                       
Think step by step and take a while to think your answer.
You should only return the statement in latex format
                                                                                                                                                             
Examples:
                                                      
<prevText>Definición 4: Un número primo es un n´umero \n</prevText>
<text>natural mayor que 1 que tiene exactamente dos divisores distintos: \n</text>
<nextText>1 y él mismo.</nextText>
Statement: '''If $p \in \mathbb{{N}}, p > 1$ is prime, then $\forall n \in \mathbb{{N}}$, if $n | p$, then $n \in \{{1, p\}}$.'''

<prevText>Definición 1: Un número par es un número entero que es divisible por 2. \n</prevText>
<text>Por ejemplo, 4 y 10 son números pares.</text>
<nextText></nextText>
Statement: If $n \in \mathbb{{Z}}$ is even, then $n$ is divisible by $2$.

<prevText>Una función $f(x)$ es continua en un intervalo si para todo $c$ en el intervalo, $\lim_{{x \to c}} f(x) = f(c)$. \n</prevText>
<text>Por ejemplo, la función $f(x) = \sin(x)$ es continua en todo $\mathbb{{R}}$.</text>
<nextText></nextText>
Statement: If $f(x)$ is a function, then $f(x)$ is continuous on an interval if for every $c$ in the interval, $\lim_{{x \to c}} f(x) = f(c)$.

<prevText>Teorema 1: La suma de los ángulos interiores de un triángulo es 180 grados. \n</prevText>
<text>Por ejemplo, en un triángulo equilátero, cada ángulo interior mide 60 grados.</text>
<nextText></nextText>
Statement: If $\triangle ABC$ is a triangle, then the sum of its interior angles is $180$ degrees.

<prevText>Proposición 2: Si dos líneas son paralelas y una tercera línea las corta, entonces los ángulos correspondientes son iguales. \n</prevText>
<text>Por ejemplo, si $l$ y $m$ son líneas paralelas y $n$ es una línea que las corta, entonces el ángulo $a$ es igual al ángulo $b$.</text>
<nextText></nextText>
Statement: If $l$ and $m$ are parallel lines and $n$ is a transversal, then corresponding angles are congruent.

<prevText>Corolario 3: Si un cuadrilátero es un paralelogramo, entonces los lados opuestos son iguales. \n</prevText>
<text>Por ejemplo, en un rectángulo, los lados opuestos son congruentes.</text>
<nextText></nextText>
Statement: If $ABCD$ is a parallelogram, then $AB \cong CD$ and $BC \cong AD$.

<prevText>Lema 6: Si un número es divisible por 6 y 8, entonces también es divisible por 24. \n</prevText>
<text>Por ejemplo, si $n$ es divisible por 6 y 8, entonces $n$ es divisible por 24.</text>
<nextText></nextText>
Statement: If $n$ is divisible by $6$ and $8$, then $n$ is divisible by $24$.

<prevText>Definición 7: Un polígono regular es un polígono con todos los lados y ángulos iguales. \n</prevText>
<text>Por ejemplo, un cuadrado es un polígono regular.</text>
<nextText></nextText>
Statement: If $P$ is a regular polygon, then all sides and angles of $P$ are congruent.

<prevText>Teorema 8: La suma de los cuadrados de los catetos es igual al cuadrado de la hipotenusa en un triángulo rectángulo. \n</prevText>
<text>Por ejemplo, en un triángulo rectángulo con catetos de longitud $a$ y $b$, y la hipotenusa de longitud $c$, $a^2 + b^2 = c^2$.</text>
<nextText></nextText>
Statement: If $\triangle ABC$ is a right triangle with legs of length $a$ and $b$, and hypotenuse of length $c$, then $a^2 + b^2 = c^2$.


                                                      
Query:
YOU ONLY HAVE TO ANSWER TO THIS WITH THE DESIRED STATEMENT IN ENGLISH IN A SINGLE LINE:
<prevText>{prevText}</prevText>
<text>{text}</text>
<nextText>{nextText}</nextText><end_of_turn>
<start_of_turn>model
Statement: """
)


promptCodeStatement = PromptTemplate.from_template("""Activity Prompt: Mathematical Statement Code Identification

Objective: Given mathematical statements of type {StatementType}, the task is to identify the code this notation uses for it.

Instructions:

Read Carefully: Review the provided mathematical statement along with any preceding or succeeding text for context.

Think step by step and take a while to think your answer.
You should only return the statement code in original language. Do not answer to the given examples.
                                                                                                                                                             
Examples:
                                                      
<prevText>Definición 4: Un número primo es un n´umero \n</prevText>
<text>natural mayor que 1 que tiene exactamente dos divisores distintos: \n</text>
<nextText>1 y él mismo.</nextText>
Code: Definición 4
<prevText>Definición 1: Un número par es un número entero que es divisible por 2. \n</prevText>
<text>Por ejemplo, 4 y 10 son números pares.</text>
<nextText></nextText>
Code: Definición 1

<prevText>Definición 3.2</prevText>
<text>Una función $f(x)$ es continua en un intervalo si para todo $c$ en el intervalo, $\lim_{{x \to c}} f(x) = f(c)$. \n</text>
<nextText>Por ejemplo, la función $f(x) = \sin(x)$ es continua en todo $\mathbb{{R}}$.</nextText>
Code: Definición 3.2

<prevText>Teorema 8: La suma de los cuadrados de los catetos es igual al cuadrado de la hipotenusa en un triángulo rectángulo. \n</prevText>
<text>Por ejemplo, en un triángulo rectángulo con catetos de longitud $a$ y $b$, y la hipotenusa de longitud $c$, $a^2 + b^2 = c^2$.</text>
<nextText></nextText>
Code: Teorema 8

Query:
YOU ONLY HAVE TO ANSWER TO THIS WITH THE DESIRED CODE:                 
<prevText>{prevText}</prevText>
<text>{text}</text>
<nextText>{nextText}</nextText>
Code: """
)
 
from .models import MathematicalStatementModel, SymbolListModel
from langchain_core.output_parsers import JsonOutputParser
# Set up a parser + inject instructions into the prompt template.
SymbolListParser = JsonOutputParser(pydantic_object=SymbolListModel)
StringListParser = JsonOutputParser(pydantic_object=StringListModel)
MathematicalStatementListParser = JsonOutputParser(pydantic_object=MathematicalStatementListModel)
#print(MathematicalStatementParser.get_format_instructions())

definitionSymbolsTemplateLlama = """Activity Prompt: Mathematical Symbol Extraction into JSON

Objective: Given a mathematical definition you should be able to obtains its symbols in a JSON format:

Elements to identify:
All symbols must have the following structure:
    - Represented: latex representation of the symbol
    - Represents: One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)

mathematicalObjects:
{mathematicalObjects}

Instructions:
Represents should always be contained in the mathematicalObjects given.
Read Carefully: Review the provided mathematical Definition and proceed to identify one missing symbol if there is anyone.
I just want symbols that represents mathematical objects as a number, a vector or a function. 
You musn't answer with symbols like logical operators or describing relationshiops such as the 'Si', >, \in, \lt, etc...
Think step by step and take a while to think your answer.
You should only return a JSON with ALL symbols which were not previouslyObtained. Do not answer to the given examples.



Examples:
query: "Diremos que una función $$f:\mathbb{{R}}\to B\subseteq\mathbb{{R}}$$ es derivable en $x\in A$ con derivada $f'(x)$ si $$\exists\lim_{{h\to 0}} \frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
Symbols:
[
{{
"represented": "f",
"represents": "Function"
}},
{{
"represented": "A",
"represents": "Set"
}},
{{
"represented": "B",
"represents": "Set"
}},
{{
"represented": "x",
"represents": "element"
}}
].

query: "'$$\\forall x \\in \\mathbb{{R}}, x \\text{{ es irracional si y solo si }} \\nexists a, b \\in \\mathbb{{Z}}, x = \\frac{{a}}{{b}}$$'"
Symbols:
[
{{
"represented": "a",
"represents": "Real"
}},
{{
"represented": "b",
"represents": "Real"
}},
{{
"represented": "x",
"represents": "Real"
}}
]

END OF EXAMPLES



Format instruction:
{format_instructions} 

Query:
{query}
Symbols:"""

#Command-r Template:
definitionSymbolsTemplate = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer a list of JSON objects

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify symbols in a mathematical text:
Instructions:
Represents should always be contained in the mathematicalObjects given.
Read Carefully: Review the provided mathematical Definition and proceed to identify one missing symbol if there is anyone.
I just want symbols that represents mathematical objects as a number, a vector or a function. 
You musn't answer with symbols like logical operators or describing relationshiops such as the 'Si', >, \in, \lt, etc...
Think step by step and take a while to think your answer.
You should only return a JSON with ALL symbols which were not previouslyObtained. Do not answer to the given examples.

Elements to identify:
All symbols must have the following structure:
    - Represented: latex representation of the symbol
    - Represents: One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)

# User Preamble
## Task and Context
Examples:
query: "Diremos que una función $$f:\mathbb{{R}}\to B\subseteq\mathbb{{R}}$$ es derivable en $x\in A$ con derivada $f'(x)$ si $$\exists\lim_{{h\to 0}} \frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
Symbols:
[
{{
"represented": "f",
"represents": "Function"
}},
{{
"represented": "A",
"represents": "Set"
}},
{{
"represented": "B",
"represents": "Set"
}},
{{
"represented": "x",
"represents": "element"
}}
]

query: "'$$\\forall x \\in \\mathbb{{R}}, x \\text{{ es irracional si y solo si }} \\nexists a, b \\in \\mathbb{{Z}}, x = \\frac{{a}}{{b}}$$'"
Symbols:
[
{{
"represented": "a",
"represents": "Real"
}},
{{
"represented": "b",
"represents": "Real"
}},
{{
"represented": "x",
"represents": "Real"
}}
]

mathematicalObjects: {mathematicalObjects}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{query}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>
Write a json-formatted list of symbols that you identify in the text. You can only return symbols that represent mathematicalObjects given by the user.  The list of symbols you identify must be formatted as a list of json objects, for example:

```json
[
    {{
        "represented": latex representation of the symbol,
        "represents": One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)
    }}
]
```<|END_OF_TURN_TOKEN|>"""

promptDefinitionSymbolParsing =  PromptTemplate(
    template=definitionSymbolsTemplate,
    input_variables=["query", "mathematicalObjects"],
    partial_variables={"format_instructions": SymbolListParser.get_format_instructions()},
)


#Command-r Template:
tool_use_prompt = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral.

# System Preamble
## Basic Rules
given a mathematical latex document should be able to obtains its symbols in a JSON format:

Elements to identify:
All symbols must have the following structure:
    - Represented: latex representation of the symbol
    - Represents: One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)

Be careful to not include latex symbols as $ or \\coloneq which are just part of the latex notation

# User Preamble
## Task and Context
You help people answer their questions and other requests interactively. You will be asked a very wide array of requests on all kinds of topics. You will be equipped with a wide range of search engines or similar tools to help you, which you use to research your answer. You should focus on serving the user's needs as best you can, which will be wide-ranging.

## Style Guide
Unless the user asks for a different style of answer, you should answer in full sentences, using proper grammar and spelling.

## Available Tools
Here is a list of tools that you have available to you:

```python
def internet_search(query: str) -> List[Dict]:
     \"""Returns a list of relevant document snippets for a textual query retrieved from the internet
     Args:
         query (str): Query to search the internet with
     \"""
     pass
```
```python
def directly_answer() -> List[Dict]:
    \"""Calls a standard (un-augmented) AI chatbot to generate a response given the conversation history
    \"""
    pass
```
<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
What's the biggest penguin in the world?<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>
Write 'Action:' followed by a json-formatted list of actions that you want to perform in order to produce a good response to the user's last input. You can use any of the supplied tools any number of times, but you should aim to execute the minimum number of necessary actions for the input. You should use the `directly-answer` tool if calling the other tools is unnecessary. The list of actions you want to call should be formatted as a list of json objects, for example:  

```json
[
    {
        "tool_name": title of the tool in the specification,
        "parameters": a dict of parameters to input into the tool as they are defined in the specs, or {} if it takes no parameters
    }
]
```
<|END_OF_TURN_TOKEN|>"""

definitionHypothesisTemplate = """<BOS_TOKEN> <|START_OF_TURN_TOKEN|>
<|SYSTEM_TOKEN|> # Safety Preamble
The instructions in this section override those in the task description and style guide sections. Don't answer questions that are harmful or immoral. Answer alway in the format described in style section JSON.

# System Preamble
## Basic Rules
You are a powerful conversational AI trained by some experts mathematicians to identify hypothesis.
Given a mathematical definition and its most similar definitions you should be able to obtains its hypothesis in JSON format

All symbols will have the following structure:
    - Represented: latex representation of the symbol
    - Represents: One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)

Elements to identify:    
    - Other definitions used in this definition
    - They should always be in the given list of possible previous definitions

Instructions:
    1. Identify which of the previousDefinitions are assumed to know in the statement from the list
    2. Return a list with the URI of them or an empty list if any of them is required

Examples:
    query: "Diremos que una función $$f:\mathbb{{R}}\to B\subseteq\mathbb{{R}}$$ es derivable en $x\in A$ con derivada $f'(x)$ si $$\exists\lim_{{h\to 0}} \frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
    symbols:
    [
    {{
    "represented": "f",
    "represents": "Function"
    }},
    {{
    "represented": "A",
    "represents": "Set"
    }},
    {{
    "represented": "B",
    "represents": "Set"
    }},
    {{
    "represented": "x",
    "represents": "element"
    }}
    ]
    Hyphothesis:["Definición 3"]

# User Preamble
## Task and Context
Symbols:
{symbols}

Previous Definitions:
{previousDefinitions}

## Style Guide
{format_instructions} 

<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|USER_TOKEN|>
{statement}<|END_OF_TURN_TOKEN|> <|START_OF_TURN_TOKEN|><|SYSTEM_TOKEN|>Write a json-formatted list of hypothesis that you identify in the text. You can only return hypothesis given by the user.  The list of hypothesis you identify must be formatted as a list of strings with its names, for example:

```json
[
   "Definición 1", "Teorema 4", "Hipótesis 1.2.3", "Corolario de Cauchy"
]<|END_OF_TURN_TOKEN|>"""
# definitionHypothesisTemplate = """Activity Prompt: Mathematical Hypothesis Extraction into JSON

# Objective: Given a mathematical definition you should be able to obtains its hypothesis in a JSON format:

# All symbols will have the following structure:
#     - Represented: latex representation of the symbol
#     - Represents: One of the given mathematicalObjects (Ej: x\in R -> Real) (return URI as it is a owl ontology)

# Elements to identify:    
#     - Other definitions used in this definition
#     - They should always be in the given list of possible previous definitions

# Instructions:
#     1. Identify which of the previousDefinitions are assumed to know in the statement from the list
#     2. Return a list with the URI of them or an empty list if any of them is required

# Examples:
# query: "Diremos que una función $$f:\mathbb{{R}}\to B\subseteq\mathbb{{R}}$$ es derivable en $x\in A$ con derivada $f'(x)$ si $$\exists\lim_{{h\to 0}} \frac{{f(x+h)-f(x)}}{{x+h-x}} = f'(x)$$"
# symbols:
# [
# {{
# "represented": "f",
# "represents": "Function"
# }},
# {{
# "represented": "A",
# "represents": "Set"
# }},
# {{
# "represented": "B",
# "represents": "Set"
# }},
# {{
# "represented": "x",
# "represents": "element"
# }}
# ]
# Hyphothesis:["Definición 3"]

# END OF EXAMPLES

# Format instruction:
# {format_instructions} 

# Symbols are:
# {symbols}

# Some previous definitions are:
# {previous_definitions}

# Query:
# {statement}
# Hypothesis:"""
promptDefinitionHypothesisParsing = PromptTemplate(
    template=definitionHypothesisTemplate,
    input_variables=["symbols","statement","previousDefinitions"],
    input_types={
        "symbols": SymbolListModel,
        "statement": str,
        "previousDefinitions": List[str]
    },
    partial_variables={"format_instructions": MathematicalStatementListParser.get_format_instructions()}

)


