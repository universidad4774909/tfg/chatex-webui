import re
from collections import defaultdict

class MathSymbolSubstituter:
    def __init__(self, symbols):
        # Initialize mappings for multiple categories
        self.symbol_to_semantic = {}
        self.semantic_to_symbol = {}
        self.category_counter = defaultdict(int)  # To keep track of numbering per category
        
        # Generate mappings from the symbols list
        for symbol in symbols:
            represented = symbol["represented"]
            category = symbol["represents"].lower()  # Example: 'Real', 'Function', etc.
            
            # Increment the counter for this category and create the representation
            self.category_counter[category] += 1
            semantic_rep = f"{category}_{self.category_counter[category]}"
            
            # Add mappings
            self.symbol_to_semantic[represented] = semantic_rep
            self.semantic_to_symbol[semantic_rep] = represented
    
    def substitute(self, statement):
        # Use regular expressions to substitute only exact matches
        def replace_symbol(match):
            symbol = match.group(0)
            return self.symbol_to_semantic.get(symbol, symbol)  # Default to original if no match
        
        # Create a regex pattern to match any of the symbols
        pattern = re.compile(r'\b(' + '|'.join(map(re.escape, self.symbol_to_semantic.keys())) + r')\b')
        substituted_statement = pattern.sub(replace_symbol, statement)
        
        return substituted_statement
    
    def undo_substitution(self, statement):
        # Use regular expressions to revert the semantic representation to original symbols
        def replace_semantic(match):
            semantic = match.group(0)
            return self.semantic_to_symbol.get(semantic, semantic)  # Default to original if no match
        
        # Create a regex pattern to match any of the semantic symbols
        pattern = re.compile(r'\b(' + '|'.join(map(re.escape, self.semantic_to_symbol.keys())) + r')\b')
        original_statement = pattern.sub(replace_semantic, statement)
        
        return original_statement

if __name__ == "__main__":
    # Example usage
    symbols = [
        {"represented": "a", "represents": "Real"},
        {"represented": "b", "represents": "Real"},
        {"represented": "x", "represents": "Real"},
        {"represented": "f", "represents": "Function"}
    ]

    # Preparsed statement
    preparsed_statement = "$$\\forall x \in \mathbb{R}, f(x) \\text{ es irracional si y solo si } \\exists a, b \\in \\mathbb{Z}, x = \\frac{a}{b}$$"

    # Create substituter instance
    substituter = MathSymbolSubstituter(symbols)

    # Substitute symbols with semantic representation
    parsed_statement = substituter.substitute(preparsed_statement)
    print("Substituted Statement:")
    print(parsed_statement)

    # Undo substitution
    original_statement = substituter.undo_substitution(parsed_statement)
    print("\nOriginal Statement:")
    print(original_statement)
