import streamlit as st
import matplotlib.pyplot as plt
import mpld3
import networkx as nx
from io import BytesIO
import streamlit.components.v1 as components
from agents.simple_agent import agent_executor, extract_all_properties, create_property_graph

class DBpediaQueryPage:
    def __init__(self):
        self.agent = agent_executor

    def create_knowledge_graph(self, properties):
        return create_property_graph(properties)

    def visualize_graph(self, G, representation_method):
        plt.figure(figsize=(12, 8))
        
        if representation_method == "Spring":
            pos = nx.spring_layout(G, k=0.5, iterations=50)
        elif representation_method == "Shift":
            pos = nx.fruchterman_reingold_layout(G)
        
        # Draw edges with different colors based on edge labels
        edge_labels = nx.get_edge_attributes(G, 'label')
        unique_labels = set(edge_labels.values())
        colors = plt.cm.tab10.colors[:len(unique_labels)]
        color_map = dict(zip(unique_labels, colors))
        
        for edge in G.edges():
            nx.draw_networkx_edges(G, pos, edgelist=[edge], edge_color=color_map[G.edges[edge]['label']], width=1.5, alpha=0.7)
        
        # Draw nodes
        nx.draw_networkx_nodes(G, pos, node_size=300, node_color='skyblue')
        nx.draw_networkx_labels(G, pos, font_size=8)
        
        # Draw edge labels
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_size=6)
        
        # Create legend
        legend_handles = [plt.Line2D([0], [0], color=color_map[label], lw=2, label=label) for label in unique_labels]
        plt.legend(handles=legend_handles, loc='upper right', bbox_to_anchor=(1.3, 1))
        
        plt.title("DBpedia Knowledge Graph")
        plt.axis('off')
        
        # Convert plot to interactive HTML
        html_fig = mpld3.fig_to_html(plt.gcf())
        components.html(html_fig, height=600)

    def convert_graph_to_gexf(self, graph):
        gexf_io = BytesIO()
        nx.write_gexf(graph, gexf_io)
        gexf_io.seek(0)
        return gexf_io

    def get_representation_method(self):
        return st.selectbox("Select a representation algorithm:", ["Spring", "Shift"])

    def render(self):
        st.title("DBpedia Query Assistant")

        question = st.text_input("Enter your question:")
        representation_method = self.get_representation_method()

        if st.button("Submit"):
            with st.spinner("Processing your question..."):
                result = self.agent.invoke({"question": question, "context": ""})

            st.subheader("Answer:")
            st.write(result['output'])

            st.subheader("Knowledge Graph:")
            all_properties = extract_all_properties(result["intermediate_steps"])
            G = self.create_knowledge_graph(all_properties)
            self.visualize_graph(G, representation_method)

            st.download_button(
                label="Download Graph (GEXF)",
                data=self.convert_graph_to_gexf(G),
                file_name="dbpedia_knowledge_graph.gexf",
                mime="application/octet-stream"
            )

def main():
    page = DBpediaQueryPage()
    page.render()

if __name__ == "__main__":
    main()