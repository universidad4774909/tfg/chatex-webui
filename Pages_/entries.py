import streamlit as st
import pymysql
from dotenv import dotenv_values
import os

config = dotenv_values()

class EntryPage:
    def __init__(self, table_name):
        self.table_name = table_name
        self.db_host = config.get('DB_HOST')
        self.db_user = config.get('DB_USER')
        self.db_password = config.get('DB_PASSWORD')
        self.db_name = config.get('DB_NAME')

    # Function to connect to the MySQL database
    def connect_to_db(self):
        return pymysql.connect(
            host=self.db_host,
            user=self.db_user,
            password=self.db_password,
            database=self.db_name,
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )

    # Function to fetch entries from the specified table
    def fetch_entries(self):
        connection = self.connect_to_db()
        with connection.cursor() as cursor:
            cursor.execute(f"SELECT * FROM {self.table_name};")
            entries = cursor.fetchall()
        connection.close()
        return entries

    # Function to add an entry to the specified table
    def add_entry(self, entry):
        connection = self.connect_to_db()
        with connection.cursor() as cursor:
            cursor.execute(f"INSERT INTO {self.table_name} (name) VALUES ('{entry}');")
            connection.commit()
        connection.close()
        st.rerun()

    # Function to update an entry in the specified table
    def update_entry(self, entry_id, new_entry):
        connection = self.connect_to_db()
        with connection.cursor() as cursor:
            cursor.execute(f"UPDATE {self.table_name} SET name='{new_entry}' WHERE `id`={entry_id};")
            connection.commit()
        connection.close()
        st.rerun()

    # Function to delete an entry from the specified table
    def delete_entry(self, entry_id):
        connection = self.connect_to_db()
        with connection.cursor() as cursor:
            cursor.execute(f"DELETE FROM {self.table_name} WHERE `id`={entry_id};")
            connection.commit()
        connection.close()
        st.rerun()

    # Main function to run the Streamlit application
    def run(self):
        st.title(f"{self.table_name.title()} Manager in the Database")
        if self.table_name:
            st.header('View Entries')
            entries = self.fetch_entries()
            if entries:
                st.table(entries)
            else:
                st.write('No entries in the specified table.')

            st.header('Add New Entry')
            new_entry = st.text_input('Enter new entry (comma-separated values):')
            if st.button('Add'):
                if new_entry:
                    self.add_entry(new_entry)
                    st.success('Entry successfully added!')
                else:
                    st.warning('Please enter values for the new entry.')

            st.header('Update Entry')
            id_update = st.text_input('Enter the ID of the entry to update:')
            new_entry_update = st.text_input('Enter the new entry (comma-separated values):')
            if st.button('Update'):
                if id_update and new_entry_update:
                    self.update_entry(id_update, new_entry_update)
                    st.success('Entry successfully updated!')
                else:
                    st.warning('Please enter both the ID and the new entry to update.')

            st.header('Delete Entry')
            id_delete = st.text_input('Enter the ID of the entry to delete:')
            if st.button('Delete'):
                if id_delete:
                    self.delete_entry(id_delete)
                    st.success('Entry successfully deleted!')
                else:
                    st.warning('Please enter the ID of the entry you wish to delete.')

if __name__ == '__main__':
    entry_page = EntryPage()
    entry_page.run()
