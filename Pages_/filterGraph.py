import random
import time
from typing import List, Set, Tuple, Dict
import networkx as nx
from matplotlib import pyplot as plt
from networkx import Graph
from DataMining.nucliaDB import VectorKB
from DataMining.ontologyWrapper import RDFOntologyUtils
from DataMining.URIs import ASSUMES_THAT_OP_URI, COROLLARY_URI, IMPLIES_THAT_OP_URI, LEMMA_URI, THEOREM_URI
import streamlit as st
import networkx as nx
from rdflib.plugins.sparql import prepareQuery
from graphs.sugiyama import nodes_to_coordinates, plotGraphWithLevels, sugiyama


@st.cache_resource
def get_kb():
    ontology_kb = VectorKB('localhost', 8080, class_kb="latex_classes_demo", individuals_kb="latex_individuals_demo", reset_kb=False)
    return ontology_kb
kb = get_kb()

@st.cache_data
def get_similar(enunciado: str,topK: int,confidence: float):
    result = kb.find_most_similar(kb.individuals_kb, enunciado, topk=topK, confidence_score=confidence)
    
    return [{
            "statement": r["text"],
            "labels": r["labels"],
            "score": r["score"]
        } for r in result]
    

def optimizeHypothesis(conclusions_uris: List[str], ontologyUtils: RDFOntologyUtils, assumed_hypothesis_uris: List[str]=[], visited: set =set(),original=False) -> Set[str]:
    """
    Optimiza las hipótesis necesarias para validar las conclusiones en un razonamiento basado en ontologías.

    :param conclusions_uris: Lista de URI de las conclusiones.
    :param assumed_hypothesis_uris: Lista de URI de las hipótesis asumidas.
    :param ontologyUtils: Objeto para acceder a funciones de utilidad de la ontología RDF.
    :param visited: Conjunto de hipótesis visitadas (por defecto, vacío).

    :return: Una tupla que contiene dos conjuntos:
             - El primer conjunto contiene las hipótesis necesarias optimizadas para validar las conclusiones.
             - El segundo conjunto contiene todas las hipótesis visitadas durante el proceso.
    """
    # Función para obtener las hipótesis asociadas a una declaración
    def get_hypothesis(conclusions):
        query = f"""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX tfgALS: <http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#>

            SELECT DISTINCT ?individual ?hypothesis ?conclusion
            WHERE {{
                ?individual rdf:type ?type .
                ?individual tfgALS:assumesThat ?hypothesis .
                FILTER (?type IN (tfgALS:Theorem, tfgALS:Corollary, tfgALS:Lemma)) .
                ?individual tfgALS:impliesThat ?conclusion .
                FILTER (?conclusion IN ({", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", conclusions))})) .
                FILTER (?individual NOT IN ({", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", visited))})) .
                FILTER (?hypothesis NOT IN ({", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", assumed_hypothesis_uris))}))
            }}
            """
                   
        hypothesis_uris = ontologyUtils.execute_sparql_query(query)
        return [(str(s["individual"]), str(s["conclusion"]), str(s["hypothesis"])) for s in hypothesis_uris.bindings]

    def getRealHypothesis(hypotheses: List[str]):
        H_n = set()
        previousH_n = -1

        while hypotheses:
            if len(H_n) != previousH_n:
                previousH_n = len(H_n)
                for h in hypotheses.copy():
                    # Existe un camino entre el resto de hipótesis y esta?
                    _, found = shortest_path_between_hypothesis_and_conclusion(ontologyUtils,[h1 for h1 in hypotheses if h1!=h],[h])
                    if not found:
                        H_n.add(h)
                        hypotheses.remove(h)
            else:
                # Todas las hipótesis son dependientes
                h = random.choice(list(hypotheses))
                H_n.add(h)
                hypotheses.remove(h)

        hypotheses = list(H_n)
        return hypotheses
        
        
    
    
    conclusions_uris = [c for c in conclusions_uris if not c in assumed_hypothesis_uris]
    # Caso base: si las conclusiones pertenecen a las hipótesis devolvemos el conjunto vacío ( No hay que añadir hipótesis )
    if len(conclusions_uris)==0:
        return set()
    neededHypothesis = get_hypothesis(conclusions_uris)
    # No hay resultados que impliquen nuestras conclusiones
    if len(neededHypothesis) == 0:
        return set(conclusions_uris)
    realHypotheses = getRealHypothesis(assumed_hypothesis_uris)
    groupedHypothesis = {}
    groupedConclusions = {}
    # Vemos cuantas hipótesis hay que añadir por cada resultado que no estén asumidas
    for result,conclusion,hypothesis in neededHypothesis:
        if hypothesis in assumed_hypothesis_uris:
            continue
        visited.add(result)
        if result in groupedHypothesis:
            groupedHypothesis[result].add(hypothesis)
            groupedConclusions[result].add(conclusion)
        else:
            groupedHypothesis[result] = {hypothesis}
            groupedConclusions[result] = {conclusion}
    
    hypothesesToAdd = {}
    for r in groupedHypothesis:
        rNeeds = optimizeHypothesis(groupedHypothesis[r], ontologyUtils, realHypotheses, visited)
        hypothesesToAdd[r] = rNeeds
        
    
    conclusionsSatisfied = set()
    result = set()
    while len(conclusionsSatisfied)<len(conclusions_uris) and len(hypothesesToAdd.keys())>0:
        resultMinimumHypotheses = min(hypothesesToAdd, key=lambda x: len(hypothesesToAdd[x]))
        del hypothesesToAdd[resultMinimumHypotheses]
        conclusionsSatisfied.update(groupedConclusions[resultMinimumHypotheses])
        result.update(groupedHypothesis[resultMinimumHypotheses])

    if len(conclusionsSatisfied)<len(conclusions_uris):
        result.update({c for c in conclusions_uris if c not in conclusionsSatisfied})
    return result

def maximize_conclusions(hypothesis_uri: List[str], ontologyUtils: RDFOntologyUtils, visited: set = set(), implicacionesUsadas=0) -> nx.DiGraph:
    """
    Esta función maximiza las conclusiones alcanzadas a partir de una lista de hipótesis.

    Argumentos:
        - hypothesis_uri: Lista de URI de hipótesis.
        - ontologyUtils: Utilidades de la ontología RDF.
        - visited: Conjunto de nodos visitados (por defecto, vacío).
        - implicacionesUsadas: Número de implicaciones utilizadas (por defecto, 0).

    Retorna:
        - nx.DiGraph: Grafo dirigido con las conclusiones maximizadas.
    """
    def getHypotheses(individual: str):
        query = f"""
            PREFIX tfgALS: <http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#>
            SELECT DISTINCT ?hypothesis
            WHERE {{
                ?individual tfgALS:assumesThat ?hypothesis .
                FILTER (?individual = tfgALS:{individual.split('#')[-1]})
            }}
        """
        statements_uris = ontologyUtils.execute_sparql_query(query)
        return [s["hypothesis"] for s in statements_uris.bindings]
    def get_factible_statements(hypothesis: List[str], visited: set):
        """
        Obtiene las declaraciones factibles basadas en las hipótesis proporcionadas.

        Argumentos:
            - hypothesis: Lista de URI de hipótesis.
            - visited: Conjunto de nodos visitados.

        Retorna:
            - List: Lista de tuplas (URI de individuo, URI de conclusión).
        """
        # Preparar la consulta SPARQL
        query = prepareQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX tfgALS: <http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#>

            SELECT DISTINCT ?individual ?conclusion
            WHERE {
                ?individual rdf:type ?type .
                ?individual tfgALS:assumesThat ?hypothesis .
                FILTER (?type IN (tfgALS:Theorem, tfgALS:Corollary, tfgALS:Lemma)) .
                ?individual tfgALS:impliesThat ?conclusion .
                FILTER (?hypothesis IN (%s)).
                FILTER NOT EXISTS {
                    ?individual tfgALS:assumesThat ?h2.
                    FILTER (?h2 NOT IN (%s))
                }
                FILTER (?individual NOT IN (%s))
            }
            """ % (", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", hypothesis)),
                   ", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", hypothesis)),
                   ", ".join(map(lambda x: f"tfgALS:{x.split('#')[-1]}", visited))))

        # Ejecutar la consulta SPARQL
        statements_uris = ontologyUtils.execute_sparql_query(query)
        
        # Retornar los resultados como una lista de tuplas
        return [(str(s["individual"]), str(s["conclusion"])) for s in statements_uris.bindings]

    # Inicializar variables
    distance = {}  # Diccionario para almacenar las distancias de cada nodo
    predecessor = {}  # Diccionario para almacenar los predecesores de cada nodo

    # Definir distancias iniciales
    for hypothesis in hypothesis_uri:
        distance[hypothesis] = 0
        predecessor[hypothesis] = ["inicio"]

    implicacionesUsadas = 0

    # Algoritmo de BFS modificado
    while True:
        resultadosFactibles = get_factible_statements(hypothesis_uri, visited)

        # Si no podemos concluir ningún otro resultado, terminamos
        if not resultadosFactibles:
            break

        implicacionesUsadas += 1

        # Procesar las conclusiones factibles
        for result, conclusion in resultadosFactibles:
            visited.add(result)
            if conclusion not in distance:
                distance[conclusion] = implicacionesUsadas
                predecessor[conclusion] = [result]
            else:
                predecessor[conclusion].append(result)

            # Actualizar la lista de hipótesis con las nuevas conclusiones
            hypothesis_uri.append(conclusion)

    # Crear un grafo dirigido
    path = nx.DiGraph()

    

    # Agrupar nodos por distancia
    grouped_by_distance = {}
    for node, dist in distance.items():
        grouped_by_distance.setdefault(dist, []).append(node)

    # Obtener las distancias en orden ascendente
    sorted_distances = sorted(grouped_by_distance.keys())

    finished = False
    currentLayerNumber = max(distance.values())
    currentLayer = {definition for definition in distance.keys() if distance[definition]==currentLayerNumber}
    alreadyAdded = set()
    while not finished:
        nextLayer = set()
        for node in currentLayer:
            if node=="inicio" or node in set(map(lambda x: x.split("#")[-1],hypothesis_uri)) or node not in predecessor or node in alreadyAdded:
                continue
            if node not in path.nodes():
                path.add_node(node)
                alreadyAdded.add(node)
                
            if predecessor[node][0] not in path.nodes() and predecessor[node]!=["inicio"]:
                for pred in predecessor[node]:
                    path.add_node(pred)
            
            if predecessor[node]!=["inicio"]:
                for pred in predecessor[node]:
                    path.add_edge(pred, node)
                    for h in getHypotheses(pred):
                        
                        if h not in path.nodes():
                            path.add_node(h)
                        path.add_edge(h.split("#")[-1], pred)
                        nextLayer.add(h.split("#")[-1])
        
        finished = len(nextLayer)==0
        currentLayerNumber-=1
        currentLayer = nextLayer.copy()
        currentLayer.update({definition for definition in distance.keys() if distance[definition]==currentLayerNumber})

    # Renombrar nodos del grafo
    new_labels = {node: node.split('#')[-1] for node in path.nodes()}
    nx.relabel_nodes(path, new_labels, copy=False)

    return path


def shortest_path_between_hypothesis_and_conclusion(ontologyUtils: RDFOntologyUtils, hypothesis_uri: List[str], conclusion_uri: List[str]) -> nx.DiGraph:
    ogConclusions = conclusion_uri.copy()
    """
    Encuentra el camino más corto entre una hipótesis y una conclusión en una ontología.

    Parámetros:
        - ontologyUtils: Una instancia de la clase RDFOntologyUtils que proporciona métodos para interactuar con la ontología.
        - hypothesis_uri: Lista de URI que representan las hipótesis.
        - conclusion_uri: Lista de URI que representan las conclusiones.

    Retorna:
        Un grafo dirigido (DiGraph) de NetworkX que representa el camino más corto entre la hipótesis y la conclusión.
    """

    def getStatements():
        """
        Obtiene las definiciones de la ontología.
        """
        return ontologyUtils.get_individuals_by_class("Definition")

    def getHypotheses(individual: str):
        query = f"""
            PREFIX tfgALS: <http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#>
            SELECT DISTINCT ?hypothesis
            WHERE {{
                ?individual tfgALS:assumesThat ?hypothesis .
                FILTER (?individual = tfgALS:{individual.split('#')[-1]})
            }}
        """
        statements_uris = ontologyUtils.execute_sparql_query(query)
        return [s["hypothesis"] for s in statements_uris.bindings]
    
    def get_factible_statements(hypothesis: List[str], visited: set):
        """
        Obtiene las definiciones factibles basadas en las hipótesis y las visitadas.
        """
        # Consulta SPARQL para obtener las declaraciones factibles
        query = f"""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX tfgALS: <http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#>

            SELECT DISTINCT ?individual ?conclusion
            WHERE {{
                ?individual rdf:type ?type .
                ?individual tfgALS:assumesThat ?hypothesis .
                FILTER (?type IN (tfgALS:Theorem, tfgALS:Corollary, tfgALS:Lemma)) .
                ?individual tfgALS:impliesThat ?conclusion .
                FILTER (?hypothesis IN ({", ".join(map(lambda x: "tfgALS:"+x.split("#")[-1], hypothesis))})).
                FILTER NOT EXISTS {{
                    ?individual tfgALS:assumesThat ?h2.
                    FILTER (?h2 NOT IN ({", ".join(map(lambda x: "tfgALS:"+x.split("#")[-1], hypothesis))}))
                }}
                FILTER (?conclusion NOT IN ({", ".join(map(lambda x: "tfgALS:"+x.split("#")[-1], hypothesis))}))
                FILTER (?individual NOT IN ({", ".join(map(lambda x: "tfgALS:"+x.split("#")[-1], visited))}))
            }}
            """
        # Ejecutar la consulta SPARQL
        statements_uris = ontologyUtils.execute_sparql_query(query)
        return [(str(s["individual"]), str(s["conclusion"])) for s in statements_uris.bindings]

    # Inicializar variables
    distance = {}       # Diccionario para almacenar las distancias de cada nodo
    predecessor = {}    # Diccionario para almacenar los predecesores de cada nodo
    visited = set()     # Conjunto para almacenar los nodos visitados

    # Definir distancias iniciales
    for hypothesis in hypothesis_uri:
        distance[hypothesis] = 0
        predecessor[hypothesis] = "inicio"
    
    # Obtener las declaraciones de la ontología
    statements = getStatements()
    implicacionesUsadas = 0

    # Algoritmo de Dijkstra modificado
    while len(visited) < len(statements):
        # Obtener las declaraciones factibles
        resultadosFactibles = get_factible_statements(hypothesis_uri, visited)
        
        # Si no hay más declaraciones factibles, salir del bucle
        if len(resultadosFactibles) == 0:
            break
        implicacionesUsadas += 1
        conclusions = []
        # Procesar cada declaración factible
        for result, conclusion in resultadosFactibles:
            conclusions.append(conclusion)
            # Agregar el nodo actual a la lista de visitados
            visited.add(result)
            # Actualizar la distancia y el predecesor si es necesario
            if not conclusion in distance:
                distance[conclusion] = implicacionesUsadas
                predecessor[conclusion] = result
            if conclusion in conclusion_uri:
                conclusion_uri.remove(conclusion)
            if len(conclusion_uri) == 0:
                break
        if len(conclusion_uri) == 0:
            break
        # Agregar las conclusiones al conjunto de hipótesis para la próxima iteración
        hypothesis_uri += conclusions

    # Crear un grafo dirigido
    path = nx.DiGraph()

    # Agrupar nodos por distancia
    grouped_by_distance = {}
    for nodo, dist in distance.items():
        if dist in grouped_by_distance:
            grouped_by_distance[dist].append(nodo)
        else: 
            grouped_by_distance[dist] = [nodo]

    # Obtener las distancias en orden ascendente
    sorted_distances = sorted(grouped_by_distance.keys())

    # Procesar los nodos desde las conclusiones originales de forma 'recursiva'
    finished = False
    currentLayer = ogConclusions.copy()
    while not finished:
        nextLayer = set()
        for node in currentLayer:
            if node in set(map(lambda x: x.split("#")[-1], hypothesis_uri)) or node not in predecessor:
                continue
            if node not in path.nodes():
                path.add_node(node)
            if predecessor[node] not in path.nodes():
                path.add_node(predecessor[node])
            
            if predecessor[node] != "inicio":
                path.add_edge(predecessor[node], node)
                for h in getHypotheses(predecessor[node]):
                    if h not in path.nodes():
                        path.add_node(h)
                    path.add_edge(h.split("#")[-1], predecessor[node])
                    nextLayer.add(h.split("#")[-1])
        
        finished = len(nextLayer) == 0
        currentLayer = nextLayer.copy()
        
    # Renombrar nodos del grafo
    new_labels = {nodo: nodo.split('#')[-1] for nodo in path.nodes()}
    nx.relabel_nodes(path, new_labels, copy=False)

    return path, len(conclusion_uri) == 0


class MathematicalAnalyzer:
    """
    Class that represents a mathematical analyzer that allows searching for similar statements and assigning them to assumptions or implications.
    """

    def __init__(self):
        """
        Initializes the instance of the mathematical analyzer.
        """
        self.assumptions: List[Dict[str, any]] = st.session_state.get('assumptions', [])
        self.implications: List[Dict[str, any]] = st.session_state.get('implications', [])
        self.ontology_utils = RDFOntologyUtils("latexOntologyExampleEnglish.rdf")
        self.kb = get_kb()
        self.similar_statements: List[Dict[str, any]] = st.session_state.get('similar_statements', [])

    def pseudo_proof(self, assumptions: List[str], implication: str):
        proof_graph, found = shortest_path_between_hypothesis_and_conclusion(self.ontology_utils, list(map(lambda x: x["labels"][-1], assumptions)), list(map(lambda x: x["labels"][-1], implication)))
        if not found:
            st.write("Path not found")
        else:
            fig, ax = plt.subplots(figsize=(20, 12))
            N, R, between = sugiyama(proof_graph)
            coordinates = nodes_to_coordinates(R)  # Get the coordinates of the nodes in the resulting graph
            pos = {node: coordinates[node] for node in N.nodes}  # Get the positions of the nodes in the resulting graph
            edge_color = {e: "salmon" if "defini" in e[0].lower() else "skyblue" for e in proof_graph.edges()}
            plotGraphWithLevels(N, R, pos, proof_graph, ax=ax, between=between, edge_colors=edge_color)  # Plot the resulting graph with levels in the second subplot
            
            plt.title("Pseudo-Proof")
            st.pyplot(plt)

    def maximize_conclusions(self, assumptions):
        maximized_graph = maximize_conclusions(list(map(lambda x: x["labels"][-1], assumptions)), self.ontology_utils, visited=set())
        fig, ax = plt.subplots(figsize=(20, 12))
        N, R, between = sugiyama(maximized_graph)
        edge_color = {e: "salmon" if "defini" in e[0].lower() else "skyblue" for e in maximized_graph.edges()}
        coordinates = nodes_to_coordinates(R)  # Get the coordinates of the nodes in the resulting graph
        pos = {node: coordinates[node] for node in N.nodes}  # Get the positions of the nodes in the resulting graph
        plotGraphWithLevels(N, R, pos, maximized_graph, ax=ax, between=between, edge_colors=edge_color)  # Plot the resulting graph with levels in the second subplot

        plt.title("Maximum Hypotheses")
        st.pyplot(plt)

    def minimize_hypotheses(self, implications, assumptions):
        missing_hypotheses = optimizeHypothesis(
            list(map(lambda x: x["labels"][-1], implications)),
            ontologyUtils=self.ontology_utils,
            visited=set(),
            assumed_hypothesis_uris=list(map(lambda x: x["labels"][-1], assumptions)),
            original=True
        )
        st.subheader("URIs of hypotheses to add:")
        st.write(
            "\n".join(
                [f"{i}. tfgALS\\:{x.split('#')[-1]}" for i, x in enumerate(missing_hypotheses, 1)]
            )
        )

    def display_lists(self):
        """
        Displays the lists of assumptions and implications with their LaTeX representation.
        """
        st.subheader("Assumptions")
        if self.assumptions:
            for i, assumption in enumerate(self.assumptions, start=1):
                st.markdown(f"{i}. {assumption['statement']}")
        else:
            st.write("No assumptions.")

        st.subheader("Implications")
        if self.implications:
            for i, implication in enumerate(self.implications, start=1):
                st.markdown(f"{i}. {implication['statement']}")
        else:
            st.write("No implications.")

    def display_buttons(self):
        """
        Displays buttons in a row with aesthetic colors.
        """
        button_css = """
        <style>
        .button-container {
            display: flex;
            justify-content: space-between;
        }
        .button {
            display: inline-block;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 15px;
            box-shadow: 0 9px #999;
            margin-bottom: 1em;
        }
        .button:hover {background-color: #3e8e41}
        .button:active {
            background-color: #3e8e41;
            box-shadow: 0 5px #666;
            transform: translateY(4px);
        }
        .button1 {background-color: #4CAF50;} /* Green */
        .button2 {background-color: #008CBA;} /* Blue */
        .button3 {background-color: #f44336;} /* Red */
        </style>
        """
        st.markdown(button_css, unsafe_allow_html=True)

        col1, col2, col3 = st.columns(3)
        with col1:
            if st.button("Minimize Hypotheses"):
                self.minimize_hypotheses(self.implications, self.assumptions)
        with col2:
            if st.button("Maximize Conclusions"):
                self.maximize_conclusions(self.assumptions)
        with col3:
            if st.button("Pseudo-Proof"):
                self.pseudo_proof(self.assumptions, self.implications)
                
    def display_statements(self):
        if self.similar_statements:
            for i, similar_statement in enumerate([e for e in self.similar_statements if e not in self.assumptions and e not in self.implications]):
                expander = st.expander(similar_statement["statement"])
                with expander:
                    assignment = st.radio(f"Assign to:", ["Nothing", "Assumptions", "Implications"], key=f"radio-{i}")
                    if assignment == "Assumptions" and similar_statement not in self.assumptions:
                        self.assumptions.append(similar_statement)
                    elif assignment == "Implications" and similar_statement not in self.implications:
                        self.implications.append(similar_statement)
            self.display_buttons()
        else:
            st.write("No similar statements found.")

    def search_similar_statements(self, statement, topK, min_confidence):
        i = 0
        res = 1
        with st.spinner("Searching for Statements"):
            self.similar_statements = get_similar(statement, topK, min_confidence)
        st.session_state["similar_statements"] = self.similar_statements
        st.success('Completed')
        self.display_statements()

    def display_interface(self):
        """
        Displays the user interface for the mathematical analyzer.
        """
        st.title("Mathematical Analyzer")

        statement = st.text_input("Enter a mathematical statement:")
        top_k = st.number_input("Number of similar statements to display:", min_value=1, step=1, value=10)
        min_confidence = st.slider("Minimum confidence:", min_value=0.0, max_value=1.0, step=0.01, value=0.4)
        if st.button("Search", type="primary"):
            self.search_similar_statements(statement, topK=top_k, min_confidence=min_confidence)

        if self.similar_statements:
            self.display_statements()
        
        self.display_lists()