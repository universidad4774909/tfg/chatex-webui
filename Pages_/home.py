import streamlit as st


class HomePage:
    @staticmethod
    def start():
        # Header
        col1, col2 = st.columns([1, 4])
        with col1:
            st.image("static\\logUS.png", width=100)  # Add your logo here
        with col2:
            st.header("🔥 CHATEX")
            st.write("Extract information from your own LaTeX data.")

        st.header("Get Started")
        st.write("To start interacting with the Math Assistant, follow these steps:")
        st.write("1. Go to the '📁 Populate Knowledge Base' page in the sidebar.")
        st.write("2. Upload your LaTeX files and click Process Files.")
