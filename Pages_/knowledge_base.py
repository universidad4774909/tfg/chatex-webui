import streamlit as st
from nucliadb_sdk import get_or_create
from sentence_transformers import SentenceTransformer
import os
from config import *


class FileUploader:

    def __init__(self):
        self.knowledge_base = st.sidebar.selectbox("Knowledge Base",
                                                   ["Group 1 Documents", "Group 2 Documents","Group 2 Apuntes"])
        self.knowledge_base = "knowledge_base1" if self.knowledge_base == "Group 1 Documents" else self.knowledge_base
        self.knowledge_base = "knowledge_base2" if self.knowledge_base == "Group 2 Documents" else self.knowledge_base
        self.knowledge_base = "knowledge_base_apuntes" if self.knowledge_base == "Group 2 Apuntes" else self.knowledge_base

    def search_knowledge_base(self):
        """
        Allow the user to insert a query, select a confidence score filter, and return the most similar text from the knowledge base.
        """
        st.subheader("Populate Knowledge Base")
         # Create a file uploader with drag-and-drop functionality
        uploaded_file = st.file_uploader("Drag and drop your LaTeX file here:", type=["tex"])

        if uploaded_file is not None:
            # Save the uploaded file to a temporary location
            temp_file_path = os.path.join("files", uploaded_file.name)
            with open(temp_file_path, "wb") as f:
                f.write(uploaded_file.getvalue())

            # Create a button to navigate to the next page
            if st.button("Upload and Process"):
                st.query_params["file_path"] = temp_file_path
                st.query_params["page"] = "Process File"
            
        # query = st.text_input("Enter your query:")

        # # Confidence score filter
        # confidence_score = st.slider("Select a confidence score filter:", 0.0, 1.0, 0.85, 0.05)
        # topk = st.radio("Select the number of top-k results:", [1, 2, 3, 4])

        # if st.button("Search"):
        #     if query:  # Only process if there's a query
        #         result = self.find_most_similar(query, confidence_score, topk)
        #         texts = result['text']
        #         scores = result['score']
        #         sources = result['source']

        #         st.markdown("### Most Similar Texts")

        #         for text_content, score, source in zip(texts, scores, sources):
        #             st.markdown(f"""
        #                 <div style='margin: 10px 0; padding: 10px; border: 1px solid #aaa; border-radius: 5px;'>
        #                     <strong>Text:</strong> {text_content}<br>
        #                     <strong>Score:</strong> {score}<br>
        #                     <strong>Source:</strong> {source}
        #                 </div>
        #             """, unsafe_allow_html=True)
        #     else:
        #         st.warning("Please enter a query to search.")

    def find_most_similar(self, input, confidence_score, topk):
        model = SentenceTransformer(embedding_model)

        query_vectors = model.encode([input])
        my_kb = get_or_create(self.knowledge_base)
        results = my_kb.search(
            vector=query_vectors[0],
            vectorset="bge",
            min_score=confidence_score,
            page_size=topk)
        d = {"text": [result.text for result in results],
             "score": [result.score for result in results],
             "source": [result.labels[0] for result in results]}

        return d

    def search_run(self):
        self.search_knowledge_base()
