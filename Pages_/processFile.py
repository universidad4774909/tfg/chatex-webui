import json

from DataMining.processLatexFile import LatexProcessor
from DataMining.ontologyWrapper import RDFOntologyUtils
import streamlit as st
from Pages_.entries import EntryPage
@st.cache_resource
def getHierarchy(latexFile:str):
    latexProcessor = LatexProcessor(latexFile)
    return latexProcessor.getHierarchy()

@st.cache_resource
def get_mathematical_statements():
    rdf_file_path = "mutableLatexOntology.rdf"
    ontology_utils = RDFOntologyUtils(rdf_file_path)
    mathematicalStatementsURI = ontology_utils.get_subclasses('http://www.semanticweb.org/usuario/ontologies/2024/1/tfgALS#MathematicalStatement')
    return [c.split("#")[-1] for c in mathematicalStatementsURI]

class FileProcesser:
    def __init__(self):
        pass

    
        

    def  processFile(self):
        # Get the temporary file path from the session state
        temp_file_path = st.query_params.get("file_path")

        if temp_file_path is not None:
            # Process the uploaded LaTeX file 
            st.subheader("Select latex elements to process")
            latexProcessor = LatexProcessor(temp_file_path)
            possibleTags = latexProcessor.listTags()
            # Create a multiselect for the possible tags
            selected_tags = st.multiselect("Select tags:", possibleTags)
            mathematicalObjects =  get_mathematical_statements()
            # Create an expander for each selected tag
            selected_objects = {}
            for tag in selected_tags:
                
                with st.expander(tag):
                    # Create a form with a multiselect for another list of strings
                    selected_options = st.multiselect(f"Select options for {tag}:", mathematicalObjects)
                    selected_objects[tag] = selected_options
                    st.write(f"You selected the following options for {tag}: {selected_options}")
            st.subheader("Apply labels by chapter")
            # Display the processed output
            hierarchy = getHierarchy(temp_file_path)


            # Create a dictionary to store the labels
            labels = {}
            labels_names = list(map(lambda x: x['name'],EntryPage("labels").fetch_entries()))


            # Iterate through the hierarchy and create a form for each chapter
            expander = st.expander(f"General")
            label_key = f"general" 
            label = expander.multiselect('Assign labels:', labels_names, key=label_key)
            labels[label_key] = label

            for idx, (chapter, content) in enumerate(hierarchy.items()):
                if chapter == "outside":
                    continue
                expander = st.expander(f"Chapter {idx}: {chapter}".replace('\\Real','\\mathbb R'))
                label_key = f"chapter_{idx}" 
                label = expander.multiselect('Assign labels:', labels_names, key=label_key)
                labels[label_key] = label
                # Create a recursive function to generate the form
                def generate_form(section=None, subsection=None, sectionIndex = None,subsectionIndex = None):
                    if section is None:
                        if not isinstance(content, str):
                            for section_idx, (section_name, section_content) in enumerate(content.items()):
                                generate_form(section_name, subsection=None, sectionIndex=section_idx)
                    elif subsection is None:
                        expander.subheader(section.replace('\\Real','\\mathbb R'))
                        if not isinstance(content[section], str):
                            for subsection_idx, (subsection_name, subsection_content) in enumerate(content[section].items()):
                                generate_form(section, subsection_name, sectionIndex=sectionIndex, subsectionIndex=subsection_idx)
                    else:
                        if "real" in subsection.lower():
                            print(subsection)
                        parsedSubsection = subsection.replace('\\Real','\\mathbb R')
                        expander.write(parsedSubsection)
                               
                
                # Generate the form
                generate_form()


            # Display the assigned labels
            st.header('Assigned Labels')
            for subsection, label in labels.items():
                st.write(f'{subsection}: {", ".join(label)}')
            if st.button("Process"):
                with open(temp_file_path.replace("files","Processing").replace(".tex",".json"), "w", encoding="utf-8") as f:
                    f.write(json.dumps({
                        'hierarchy': hierarchy,
                        'selectedLabels': labels,
                        'TagsObjectRelation': selected_objects,
                        'ontology': 'latexMutableOntology',
                        'knowledgeDB': 'latexKDB'
                        },indent=4, ensure_ascii=False))
                st.query_params.page = "Process Task"
                del st.query_params["file_path"]    
                st.query_params["config"] = temp_file_path.replace("files","Processing").replace(".tex",".json")


        else:
            st.write("No file uploaded. Please go back to the Populate page.")
            if st.button("Go Back"):
                st.query_params["page"] = "Populate"
