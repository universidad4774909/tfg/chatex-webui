
import json
import streamlit as st
import threading
import asyncio
import time
import urllib.parse
import urllib3
from DataMining.mainMining import processGeneralStatement
from DataMining.processLatexFile import LatexProcessor
# Create a progress bar


# Define an asynchronous task
async def async_processing():
    await processGeneralStatement()  # simulate some work
    return "Async task completed!"








class TaskProcesser():
    def __init__(self,config_file):
        self.progress_bar = st.progress(0)
        self.config_file = config_file
        with open(config_file,"r", encoding="utf-8") as f:
            self.config = json.loads(f.read())
        self.equations="equation" in self.config["TagsObjectRelation"].keys() or "Equation" in self.config["TagsObjectRelation"].keys()
        self.interestingTagsPattern = r'('+'|'.join([k for k in self.config["TagsObjectRelation"].keys() if not k in ["Equation","equation"]])+r')'
        
    def update_progress_bar(self):
        self.progress_bar.progress(round(self.currentSection/len(self.config["hierarchy"][self.chapter_name]),2))
       

    def processTextAndContext(self,textToProcess:str, context_name:str, context_code:str):
        latexProcessor = LatexProcessor(textToProcess,direct_text=True)
        tagsToProcess = latexProcessor.findTag(self.interestingTagsPattern)
        if self.equations:
            contentToProcess = [content for (_,content,_,_) in tagsToProcess]
            equationTags = latexProcessor.findTag(r'('+'|'.join(["Equation","equation"])+r')')
            for (tagType,tagContent,_,_) in equationTags:
                if not any([tagContent in previousTagContent for previousTagContent in contentToProcess]):
                    tagsToProcess.append((tagType,tagContent,None,None))
        statements = []
        statementToName = {}
        for i,(tagType,tagContent,_,_) in enumerate(tagsToProcess,start=1):
            objectiveObjects = self.config["TagsObjectRelation"][tagType]
            st.html(f"<h6>Processing {tagType} into one of ({', '.join(objectiveObjects)})</h6>")
            st.write(tagContent.replace("\\begin{equation}","$").replace("\\end{equation}","$"))
            
            statement, hypothesis, conclusions = processGeneralStatement(tagContent, f"{context_code}.{i}", objectiveObjects, list(self.chapter_labels), statementToName=statementToName)
            statements.append(statement)
            statementToName[statement.statement] = statement.name
            st.write(f"Finished parsing, URI: {urllib.parse.unquote(statement.name)}")
            st.write("Symbols associated:")
            st.table(statement.symbols)
            if len(hypothesis)>0:
                st.write(f"Asume: {', '.join(hypothesis)}")
            else:
                st.write("Sin hipótesis")
            if len(conclusions)>0:
                st.write(f"Implica: {', '.join(conclusions)}")
            else:
                st.write("Sin conclusiones")
        
    
    def processSection(self):
        chapterValues = self.config["hierarchy"][self.chapter_name]
        if isinstance(chapterValues,str): # El capítulo no tiene secciones
            textToProcess = chapterValues
            self.processTextAndContext(textToProcess,self.chapter_name, f"{self.currentChapter}")
            
        elif isinstance(chapterValues,dict):
            self.section_name = list(self.config["hierarchy"][self.chapter_name].keys())[self.currentSection]
            st.write(f"Processing section: {self.section_name}")
            sectionValues = self.config["hierarchy"][self.chapter_name][self.section_name]
            if isinstance(sectionValues,dict): # Hay subsecciones
                for i, (subsection_name, subsection_content) in enumerate(sectionValues.items(),start=1):
                    textToProcess = subsection_content
                    self.processTextAndContext(textToProcess,subsection_name, f"{self.currentChapter}.{self.currentSection+1}.{i}")

            else:
                textToProcess = sectionValues
                self.processTextAndContext(textToProcess,self.section_name, f"{self.currentChapter}.{self.currentSection+1}")
        
        st.session_state["execution"] = False
            
            
    def processSections(self):
        self.currentSection = 0
        sectionsToProcess = len(self.config["hierarchy"][self.chapter_name]) if isinstance(self.config["hierarchy"][self.chapter_name],dict) else 1
        while self.currentSection<sectionsToProcess:
            self.processSection()
            self.currentSection+=1
            self.update_progress_bar()

        self.config["executed"]["lastProcessedChapter"] += 1
        with open(self.config_file,"w", encoding="utf-8") as f:
            f.write(json.dumps(self.config,indent=4,ensure_ascii=False))
        st.query_params["config"] = self.config_file
        st.query_params["page"] = "Process Task"
        st.rerun()

    def processTask(self,task='AgentProcessing'):
        if task=='AgentProcessing':
            if not "execution" in st.session_state or not st.session_state["execution"]:
                st.session_state["execution"] = True
                if "executed" in self.config:
                    self.currentChapter = self.config["executed"]["lastProcessedChapter"]+1
                else:
                    self.currentChapter = 1 # El primer capítulo siempre es outside
                    self.config["executed"] = {"lastProcessedChapter": 1}
                if self.currentChapter >= len(self.config["hierarchy"].keys()):
                    st.write("Processing finished")
                    print("FIN")
                else:
                    self.chapter_name = list(self.config["hierarchy"].keys())[self.currentChapter]
                    self.chapter_labels = set(self.config["selectedLabels"]["general"]).union(self.config["selectedLabels"][f"chapter_{self.currentChapter}"]).union({'.'.join(self.config_file.split("/")[-1].split("\\")[-1].split(".")[:-1])})
                    st.header("Processing Logs")
                    st.write(f"Processing Chapter: {self.chapter_name}")
                    self.processSections()

                
                

            

