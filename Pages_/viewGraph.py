from io import BytesIO
import streamlit as st
import matplotlib.pyplot as plt
import mpld3
from rdflib import Graph
from rdflib.plugins.sparql.processor import SPARQLProcessor
import streamlit.components.v1 as components
import networkx as nx
import os
from DataMining.URIs import BASE_URI, HAS_LABEL_DP_URI
from Pages_.entries import EntryPage

@st.cache_data
def get_total_labels():
    labels = EntryPage("labels").fetch_entries()
    return [l["name"] for l in labels]

@st.cache_data
def get_total_predicates(predicates):
    return [p for p in list(map(lambda x: x.split("#")[-1], set(predicates))) if p in set(["impliesThat","represents","assumesThat", "hasSymbols"])]

def convert_graph_to_gexf(graph):
    gexf_io = BytesIO()
    nx.write_gexf(graph, gexf_io)
    gexf_io.seek(0)
    return gexf_io

class RDFVisualizer:
    def __init__(self):
        pass

    def get_query_input(self, predicates_to_display, labels):
        # Check if there are selected predicates
        if predicates_to_display:
            num_predicates_to_display = len(predicates_to_display)
            # Build the initial part of the SPARQL query
            query_input = "SELECT ?s ?p ?o ?label WHERE {?s ?p ?o . FILTER(?p in ("
            # Add the selected predicates to the query
            for i, predicate in enumerate(predicates_to_display):
                query_input += f"<{BASE_URI}{predicate}>"
                if i < num_predicates_to_display - 1:
                    query_input += ", "
            query_input += ")) "
            
            # Check if there are selected labels
            if labels:
                # Add the FILTER clause to filter by labels
                query_input += f". ?s <{HAS_LABEL_DP_URI}> ?label . FILTER(?label in ("
                num_labels = len(labels)
                for i, label in enumerate(labels):
                    query_input += f'"{label}"'
                    if i < num_labels - 1:
                        query_input += ", "
                query_input += "))"

            query_input += "}"

            return query_input
        else:
            # If no predicates are selected, return None
            return None

    def get_labels(self):
        labels = EntryPage("labels").fetch_entries()
        return [l["name"] for l in labels]

    def execute_query(self, query_input):
        processor = SPARQLProcessor(self.graph)
        result = processor.query(query_input)
        return result

    def visualize_rdf_graph(self, result, representation_method):
        def plot_3d_graph(G):
            fig = plt.figure()  # Create a new figure
            ax = fig.add_subplot(111, projection='3d')  # Add a 3D subplot
            
            if representation_method == "Spring":
                # Adjust the spring layout to make positions further apart  
                pos = nx.spring_layout(G, k=0.1, dim=2)  # Use dim=3 for 3D layout
            elif representation_method == "Planar":
                pos = nx.planar_layout(G)
            # Draw edges
            for edge in G.edges():
                u, v = edge
                x = [pos[u][0], pos[v][0]]
                y = [pos[u][1], pos[v][1]]
                z = [pos[u][2], pos[v][2]]
                ax.plot(x, y, z, color='gray', alpha=0.5)  # Draw edges
            
            # Draw nodes
            for node in G.nodes():
                x = pos[node][0]
                y = pos[node][1]
                z = pos[node][2]
                ax.scatter(x, y, z, color='skyblue', s=10)  # Draw nodes
            
            # Draw edge labels
            edge_labels = {(u, v): d.get('label', '') for u, v, d in G.edges(data=True)}
            for edge, label in edge_labels.items():
                u, v = edge
                x = (pos[u][0] + pos[v][0]) / 2
                y = (pos[u][1] + pos[v][1]) / 2
                z = (pos[u][2] + pos[v][2]) / 2
                ax.text(x, y, z, label, color='black')  # Draw edge labels
            
            # Set axis labels
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
            
            # Adjust plot for interactivity (zooming)
            ax.set_box_aspect([1, 1, 1])  # Equal aspect ratio
            fig_html = mpld3.fig_to_html(fig)
            components.html(fig_html, height=600)

        def plot_2d_graph(G):
            if representation_method == "Spring":
                # Adjust the spring layout to make positions further apart  
                pos = nx.spring_layout(G, k=0.1, dim=2)  # Use dim=3 for 3D layout
            elif representation_method == "Planar":
                pos = nx.planar_layout(G)
            fig, ax = plt.subplots(1)  # Create a new figure and axis
            
            # Draw edges with different colors based on edge labels
            edge_labels = {(u, v): d.get('label', '') for u, v, d in G.edges(data=True)}
            unique_labels = set(edge_labels.values())
            colors = plt.cm.tab10.colors[:len(unique_labels)]  # Use different colors from the 'tab10' colormap
            color_map = dict(zip(unique_labels, colors))
            for edge, label in edge_labels.items():
                nx.draw_networkx_edges(G, pos, edgelist=[edge], ax=ax, node_size=10, edge_color=color_map[label])

            # Draw nodes
            nx.draw_networkx_nodes(G, pos, ax=ax, node_size=10, node_color='skyblue')

            # Create legend
            legend_handles = [plt.Line2D([0], [0], color=color_map[label], lw=2, label=label) for label in unique_labels]
            ax.legend(handles=legend_handles, loc='upper right')

            # Adjust plot for interactivity (zooming)
            html_fig = mpld3.fig_to_html(fig)
            components.html(html_fig, height=600)

        G = nx.DiGraph()
        convertToLocal = lambda x: x.split("#")[-1].replace(">","")
        for row in result["bindings"]:
            subject = convertToLocal(row.get("s").n3())
            predicate = convertToLocal(row.get("p").n3())
            object_ = convertToLocal(row.get("o").n3())
            G.add_edge(subject, object_, label=predicate)
        self.G = G
        plot_2d_graph(G)
        
    def get_representation_method(self):
        method = st.selectbox("Select a representation algorithm:", ["Spring", "Planar"])
        return method

    def represent_graph(self):
        st.title("RDF Graph Visualizer")

        directory_path = "ontologies/"  
        rdf_files = [f for f in os.listdir(directory_path) if f.endswith(".rdf")]
        selected_file = st.selectbox("Select an RDF file:", rdf_files)

        if selected_file:
            self.filename = os.path.join(directory_path, selected_file)
            self.graph = Graph().parse(self.filename, format='xml')
            total_labels = get_total_labels()
            total_predicates = get_total_predicates(list(self.graph.predicates()))

            with st.expander("Labels"):
                labels = st.multiselect("Select labels to display:", total_labels)

            with st.expander("Predicates"):
                predicates_to_display = st.multiselect("Select predicates to display:", total_predicates)

            query_input = self.get_query_input(predicates_to_display, labels)
            representation_method = self.get_representation_method()
            button = st.button("Visualize")
            if button and query_input and representation_method:
                result = self.execute_query(query_input)
                self.visualize_rdf_graph(result, representation_method)
                button_download = st.download_button(
                    label="Download Gephi GEXF Graph",
                    data=convert_graph_to_gexf(self.G),
                    file_name="graph.gexf",
                    mime="application/octet-stream"
                )