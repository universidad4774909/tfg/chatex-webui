import importlib
from .wikidata_agent import EnhancedDBpediaAgent
from dotenv import dotenv_values, load_dotenv
import os
import os
import config
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
#spec = importlib.util.spec_from_file_location("archivo", "../carpeta1/subcarpeta1/archivo.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
# Para invocar la rutina anteponemos el nombre del módulo
Llamus = customLLMus.Llamus
LlamusChat = customLLMus.LlamusChat


dotEnvDict = dotenv_values(".env")
API_KEY = dotEnvDict.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = dotEnvDict.get("LLAMUS_BASE_PATH")
def get_agent():

    llm = Llamus(
        n=30,
        model="llama3.1:70b",
        api_key=API_KEY,
        endpoint=LLAMUS_BASE_PATH
    )

    return EnhancedDBpediaAgent(llm)
