from langchain.callbacks.base import BaseCallbackHandler
from typing import Any

class CustomCallbackHandler(BaseCallbackHandler):
    @property
    def handlers(self):
        """Property to ensure compatibility with LangChain's callback system."""
        return []

    def on_tool_end(self, output: Any, **kwargs: Any) -> Any:
        print("Tool ended with output:", output)
    
    def on_step(self, step: Any, **kwargs: Any) -> Any:
        print("Intermediate step:", step)
    
    def on_tool_start(self, tool: str, tool_input: Any, **kwargs: Any) -> Any:
        print(f"Starting tool {tool} with input: {tool_input}")