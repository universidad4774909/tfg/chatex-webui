import importlib
from typing import Optional, Type, List, Dict, Any
from dotenv import dotenv_values
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.callbacks.manager import (
    AsyncCallbackManagerForToolRun,
    CallbackManagerForToolRun,
)
from langchain_core.output_parsers import StrOutputParser
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
from langchain_core.language_models.llms import LLM
import aiohttp
import asyncio
import xml.etree.ElementTree as ET



import requests

def extract_dbpedia_uris_and_labels(xml_data: str) -> Dict[str, str]:
    """
    Extracts DBpedia URIs and their corresponding labels from the given XML string.

    Args:
        xml_data (str): A string containing the XML data.

    Returns:
        Dict[str, str]: A dictionary with labels as keys and URIs as values.
    """
    # Parse the XML data from the input string
    root = ET.fromstring(xml_data)

    # Dictionary to store the extracted labels and URIs
    results = {}

    # Iterate over each 'Result' element in the XML
    for result in root.findall('Result'):
        # Find the 'Label' element within the current 'Result' element and get its text
        label = result.find('Label').text
        
        # Find the 'URI' element within the current 'Result' element and get its text
        uri = result.find('URI').text
        
        # Add the label and URI to the dictionary
        results[label] = uri

    # Return the dictionary containing labels and URIs
    return results


class DBpediaEntitySearchInput(BaseModel):
    json_input: str = Field(description="""JSON string containing: 
    query:The search query for DBpedia entities.
    main_question: The main question to be solved""")

class DBpediaEntitySearchInputParsed(BaseModel):
    query: str = Field(description="The search query for DBpedia entities")
    main_question: str = Field(description="The main question to be solved")

class DBpediaEntityInfoInput(BaseModel):
    json_input: str = Field(description="""JSON string containing:
    uri: The URI of the DBpedia entity to retrieve information about
    main_question: The main question to be solved""")

class DBpediaEntityInfoInputParsed(BaseModel):
    uri: str = Field(description="The URI of the DBpedia entity to retrieve information about")
    main_question: str = Field(description="The main question to be solved")



class DBpediaEntitySearchTool(BaseTool):
    name = "dbpedia_entity_search"
    description = "Search for DBpedia entities based on a query string and filter results based on relevance to the main question"
    args_schema: Type[BaseModel] = DBpediaEntitySearchInput
    llm: LLM

    def __init__(self, llm: LLM):
        super().__init__(llm=llm)
        self.llm = llm

    def _create_filter_chain(self):
        prompt_template = """
        You are an AI assistant tasked with filtering entities based on their relevance to a main question.
        You should discard any entity you are not sure that is not relevant. We can search it later again if not.
        Take your time and think it deeply in the importance of each entity.
        You will be fired if you provide entities that are not relevant to the question.

        Main question: {main_question}
        Entities and descriptions:
        {entities_descriptions}

        Answer with a comma-separated list of entities that are really relevant to the main question, with a maximum of 5.
        """
        prompt = PromptTemplate(
            input_variables=["main_question", "entities_descriptions"],
            template=prompt_template
        )
        chain = prompt | self.llm | StrOutputParser()
        return chain

    async def _filter_entities(self, entities: Dict[str, str], main_question: str) -> Dict[str, str]:
        filter_chain = self._create_filter_chain()
        entities_descriptions = "\n".join(
            [f"{entity}" for entity in entities]
        )
        
        response = filter_chain.invoke({"main_question":main_question, "entities_descriptions":entities_descriptions})
        
        relevant_entities = response.split(',')
        relevant_entities = [entity.strip() for entity in relevant_entities]

        filtered_entities = {entity: entities[entity] for entity in relevant_entities if entity in entities}

        return filtered_entities

    def _run(
        self,
        json_input: str,
        run_manager: Optional[CallbackManagerForToolRun] = None
    ) -> Dict[str, str]:
        cured_json_input = json_input.split('\n}\n')
        cured_json_input = cured_json_input[0] if len(cured_json_input)==1 else cured_json_input[0]+'}'
        input_data = DBpediaEntitySearchInputParsed.parse_raw(cured_json_input)
        query = input_data.query
        main_question = input_data.main_question
        
        url = f"http://lookup.dbpedia.org/api/search/KeywordSearch?QueryString={query}"
        headers = {"Accept": "application/xml"}
        response = requests.get(url, headers=headers)
        data = response.text
        
        entities = extract_dbpedia_uris_and_labels(data)
        filtered_entities = asyncio.run(self._filter_entities(entities, main_question))
        return f"Observation: {filtered_entities}"

    async def _arun(
        self,
        json_input: str,
        run_manager: Optional[AsyncCallbackManagerForToolRun] = None
    ) -> Dict[str, str]:
        input_data = DBpediaEntitySearchInput.parse_raw(json_input.split('\n}\n')[0]+'}')
        query = input_data.query
        main_question = input_data.main_question
        
        url = f"http://lookup.dbpedia.org/api/search/KeywordSearch?QueryString={query}"
        headers = {"Accept": "application/xml"}
        
        async with aiohttp.ClientSession() as session:
            async with session.get(url, headers=headers) as response:
                data = await response.text()
        
        entities = extract_dbpedia_uris_and_labels(data)
        print("Entities before filtering:\n{entities}")
        filtered_entities = await self._filter_entities(entities, main_question)
        return f"Observation: {filtered_entities}" 



class DBpediaEntityInfoTool(BaseTool):
    name = "dbpedia_entity_info"
    description = "Retrieve information about a specific DBpedia entity using its URI and filter properties based on relevance to the main question"
    args_schema: Type[BaseModel] = DBpediaEntityInfoInput
    llm: LLM

    def __init__(self, llm: LLM):
        super().__init__(llm=llm)
        self.llm = llm

    def _create_filter_chain(self):
        prompt_template = """
        You are an AI assistant tasked with filtering entity properties based on their relevance to a main question.
        Return only properties that are really relevant for the question and that you are sure about that.
        
        Main question: {main_question}
        Entity properties:
        {properties}

        Respond with a comma-separated list of property URIs that are relevant to the main question. Return the full URI including https:// if it was included.
        """
        prompt = PromptTemplate(
            input_variables=["main_question", "properties"],
            template=prompt_template
        )
        chain = prompt | self.llm | StrOutputParser()
        return chain

    async def _filter_properties(self, properties: Dict[str, str], main_question: str) -> Dict[str, str]:
        filter_chain = self._create_filter_chain()
        properties_str = "\n".join(properties.keys())
        
        response = filter_chain.invoke({"main_question": main_question, "properties": properties_str})
        
        relevant_properties = response.split(',')
        relevant_properties = [prop.strip() for prop in relevant_properties]

        filtered_properties = {prop: properties[prop] for prop in relevant_properties if prop in properties}

        return filtered_properties

    def _run(
        self,
        json_input: str,
        run_manager: Optional[CallbackManagerForToolRun] = None
    ) -> Dict[str, Dict[str, Any]]:
        cured_json_input = json_input.split('\n}\n')
        cured_json_input = cured_json_input[0] if len(cured_json_input)==1 else cured_json_input[0]+'}'
        input_data = DBpediaEntityInfoInputParsed.parse_raw(cured_json_input)
        uri = input_data.uri
        main_question = input_data.main_question

        url = "https://dbpedia.org/sparql"
        query = """
        SELECT ?property ?value
        WHERE {
          <%s> ?property ?value .
        }
        """
        query = query % uri

        headers = {"Accept": "application/sparql-results+json"}
        
        try:
            response = requests.post(url, data={"query": query, "format": "json"}, headers=headers)
            response.raise_for_status()
            data = response.json()
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")
            return {}

        properties = {}
        for binding in data.get("results", {}).get("bindings", []):
            prop = binding.get("property", {}).get("value", "")
            value = binding.get("value", {}).get("value", "")
            properties[prop] = value

        filtered_properties = asyncio.run(self._filter_properties(properties, main_question))
        return f"Observation: {filtered_properties}"

    async def _arun(
        self,
        json_input: str,
        run_manager: Optional[AsyncCallbackManagerForToolRun] = None
    ) -> Dict[str, Dict[str, Any]]:
        input_data = DBpediaEntityInfoInputParsed.parse_raw(json_input.split('\n}\n')[0]+'}')
        uri = input_data.uri
        main_question = input_data.main_question

        url = "https://dbpedia.org/sparql"
        query = """
        SELECT ?property ?value
        WHERE {
          <%s> ?property ?value .
        }
        """
        query = query % uri

        headers = {"Accept": "application/sparql-results+json"}
        
        try:
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data={"query": query, "format": "json"}, headers=headers) as response:
                    response.raise_for_status()
                    data = await response.json()
        except aiohttp.ClientError as e:
            print(f"An error occurred: {e}")
            return {}

        properties = {}
        for binding in data.get("results", {}).get("bindings", []):
            prop = binding.get("property", {}).get("value", "")
            value = binding.get("value", {}).get("value", "")
            properties[prop] = value

        filtered_properties = await self._filter_properties(properties, main_question)
        return f"Observation: {filtered_properties}"