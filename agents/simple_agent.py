import importlib
import json
import networkx as nx
import matplotlib.pyplot as plt
from dotenv import dotenv_values
from langchain.agents import AgentExecutor, Tool, create_react_agent
from langchain.prompts import PromptTemplate
from langchain.schema import AgentAction, AgentFinish
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
#spec = importlib.util.spec_from_file_location("archivo", "../carpeta1/subcarpeta1/archivo.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
# Para invocar la rutina anteponemos el nombre del módulo
Llamus = customLLMus.Llamus
LlamusChat = customLLMus.LlamusChat

dotEnvDict = dotenv_values(".env")
API_KEY = dotEnvDict.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = dotEnvDict.get("LLAMUS_BASE_PATH")

from typing import Any, Dict, List, Tuple, Union
import re
from .dbpedia import  DBpediaEntitySearchTool, DBpediaEntityInfoTool

# Initialize the LLM
# llm = OpenAI()
llm = Llamus(
        n=30,
        model="llama3.1:70b",
        api_key=API_KEY,
        endpoint=LLAMUS_BASE_PATH
    )

# Initialize the custom tools
dbpedia_search_tool =  DBpediaEntitySearchTool(llm=llm)
dbpedia_info_tool = DBpediaEntityInfoTool(llm=llm)

# Create the list of tools
tools = [dbpedia_search_tool, dbpedia_info_tool]

# Custom callback handler
class CustomCallbackHandler():
    def on_agent_action(self, action: AgentAction, **kwargs) -> None:
        print(f"Action: {action.tool}\nAction Input: {action.tool_input}")

    def on_agent_finish(self, finish: AgentFinish, **kwargs) -> None:
        print(f"Final Answer: {finish.return_values['output']}")


# Create the Prompt Template
template = """You are an expert agent in querying DBpedia. Your task is to answer questions using information from DBpedia.

Current context:
{context}

Question: {question}

Available tools:
- dbpedia_entity_search: Search for relevant DBpedia entities.
- dbpedia_entity_info: Look up information associated with DBpedia entities.

{tools} {tool_names}

To answer, follow these steps:
1. Identify what information you need to answer the question.
2. Search for relevant DBpedia entities using the dbpedia_entity_search tool.
3. Look up information associated with the found entities using the dbpedia_entity_info tool.
4. Evaluate which concepts are relevant to the answer.
5. If you have a factual explanation based on DBpedia's knowledge graph, provide a detailed response.
6. If you need more information, continue searching and querying until you have enough to answer the question but never repaet a query.



After Action Input wait for the action to be completed, don't continue generating.
No observation should be made before the Action is completed.
You don't make observations. They will be given to you after the tool call is completed. Please stop generating after giving Action nput
Don't repeat queries.
You always have to explain your next action with a thought!

Use the following format:

Thought: Consider what to do next
Action: The action to take (must be one of [ dbpedia_entity_search, dbpedia_entity_info])
Action Input: The input for the action
Observation: The result of the action
... (this Thought/Action/Action Input/Observation process can repeat N times but one step per query)
Thought: I now have the information needed to answer the question
Final Answer: The final answer to the user's question

Examples:

Begin:
<begin-llm>
Thought: I need to find out what professions Nicholas Ray and Elia Kazan are known for. I'll start with Nicholas Ray.
Action: dbpedia_entity_search
Action Input: {{
  "query": "Nicholas Ray",
  "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
}}
<end-llm>

<begin-llm>
Thought: Now I need to find information about Elia Kazan.
Action: dbpedia_entity_search
Action Input: {{
  "query": "Elia Kazan",
  "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
}}
<end-llm>

<begin-llm>
Thought: The filtered search has provided relevant information about both Nicholas Ray and Elia Kazan in relation to their professions. I should now look up more detailed information about Nicholas Ray to confirm his profession.
Action: dbpedia_entity_info
Action Input: {{
    "uri": "http://dbpedia.org/resource/Nicholas_Ray"
    "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
}}
<end-llm>
<begin-llm>
Observation: {{'Elia_Kazan': 'http://dbpedia.org/resource/Elia_Kazan'}}
Thought: Now I'll get detailed information about Elia Kazan to confirm his profession as well.
Action: dbpedia_entity_info
Action Input: {{
    "uri": "http://dbpedia.org/resource/Elia_Kazan"
    "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
}}
<end-llm>

<begin-llm>
Observation: {{'http://dbpedia.org/property/profession': 'Film Director'}}
Thought: I now have the information needed to answer the question
Final Answer: The profession that Nicholas Ray and Elia Kazan have in common is film director.
<end-llm>


<begin-llm>
Observation: {{'http://dbpedia.org/property/nationality': 'USA'}}
Thought: I now have the information needed to answer the question
Final Answer: The profession that Nicholas Ray and Elia Kazan have in common they are american.
<end-llm>

Begin:

Thought: {agent_scratchpad}"""

prompt = PromptTemplate(
    template=template,
    input_variables=["context", "question", "agent_scratchpad", "tools", "tool_names"],
    input_types={
        "context": str,
        "question": str,
        "agent_scratchpad": str,
    }
)

# Create the ReAct agent
agent = create_react_agent(
    llm,
    tools,
    prompt,
    stop_sequence=True
)

# Create the AgentExecutor
agent_executor = AgentExecutor.from_agent_and_tools(
    agent=agent, 
    tools=tools, 
    verbose=True, 
    return_intermediate_steps=True,
    handle_parsing_errors=True,
    max_iterations =20,
    max_execution_time = 3600
)


def extract_all_properties(intermediate_steps: List[Tuple[Any, str]]) -> Dict[str, Dict[str, str]]:
    all_properties = {}

    for step in intermediate_steps:
        action, observation = step
        
        if action.tool == 'dbpedia_entity_info':
            try:
                action_input = json.loads(action.tool_input.split('\nObservation:')[0])
                entity_uri = action_input['uri']
                
                observation_dict = dict(set(map(tuple,json.loads(observation.split("Observation: ", 1)[-1].replace("'", '"')).items())))
                
                all_properties[entity_uri] = observation_dict
            except json.JSONDecodeError:
                print(f"Failed to parse JSON for step: {step}")
            except KeyError:
                print(f"Missing expected key in step: {step}")

    return all_properties

def create_property_graph(properties: Dict[str, Dict[str, str]]) -> nx.DiGraph:
    G = nx.DiGraph()
    
    for entity, props in properties.items():
        G.add_node(entity.split("/")[-1], type='entity')
        for prop, value in props.items():
            G.add_node(value.split("/")[-1], type='value')
            G.add_edge(entity.split("/")[-1], value.split("/")[-1], label=prop.split("/")[-1])
    
    return G

def plot_graph(G: nx.DiGraph):
    plt.figure(figsize=(15, 10))
    pos = nx.spring_layout(G, k=1.5, iterations=100)
    
    nx.draw_networkx_nodes(G, pos, node_size=3000, node_color='lightblue')
    nx.draw_networkx_edges(G, pos, edge_color='gray', arrows=True)
    
    nx.draw_networkx_labels(G, pos, font_size=8, font_weight="bold")
    
    edge_labels = nx.get_edge_attributes(G, 'label')
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_size=6)
    
    plt.title("DBpedia Property Graph", fontsize=16)
    plt.axis('off')
    plt.tight_layout()
    plt.show()
    print("stop")

# Example usage
if __name__ == "__main__":
    question = "What are the capitals of France, Bolivia and Canada? What are the populations of these countries and in which continent are them?"
    context = "s"  # Additional context can be provided here if needed
    result = agent_executor.invoke({"question": question, "context": context})
    print(f"\nQuestion: {question}")
    print(f"Answer: {result['output']}")
    
    all_properties = extract_all_properties(result["intermediate_steps"])
    
    print("\nExtracted Properties:")
    for entity, props in all_properties.items():
        print(f"Entity: {entity}")
        for prop, value in props.items():
            print(f"  {prop}: {value}")
        print()
    
    G = create_property_graph(all_properties)
    plot_graph(G)