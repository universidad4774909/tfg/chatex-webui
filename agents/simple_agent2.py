# simple_agent2.py
import importlib
import json
import networkx as nx
import matplotlib.pyplot as plt
from dotenv import dotenv_values
from langchain.agents import AgentExecutor, Tool, create_react_agent
from langchain.prompts import PromptTemplate
from langchain.schema import AgentAction, AgentFinish
from typing import Any, Dict, List, Tuple, Union
import re
from .wikidata_agent import EnhancedDBpediaAgent
from .dbpedia import DBpediaEntitySearchTool, DBpediaEntityInfoTool  # Modified import

# Initialize the LLM
spec = importlib.util.spec_from_file_location("customLLMus", "../LlamusWrapper/customLLMus.py")
customLLMus = importlib.util.module_from_spec(spec)
spec.loader.exec_module(customLLMus)
Llamus = customLLMus.Llamus
LlamusChat = customLLMus.LlamusChat
dotEnvDict = dotenv_values(".env")
API_KEY = dotEnvDict.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = dotEnvDict.get("LLAMUS_BASE_PATH")

llm = Llamus(
    n=30,
    model="llama3.1:70b",
    api_key=API_KEY,
    endpoint=LLAMUS_BASE_PATH
)

# Initialize DBpedia Tools
dbpedia_search_tool = DBpediaEntitySearchTool(llm=llm)
dbpedia_info_tool = DBpediaEntityInfoTool(llm=llm)

# Create a list of tools to be used by the agent
tools = [
    Tool(
        name="DBpedia Entity Search",
        func=dbpedia_search_tool._run,  # Changed to reference the correct function
        description="Searches for entities in DBpedia based on a query and filters results based on relevance."
    ),
    Tool(
        name="DBpedia Entity Info",
        func=dbpedia_info_tool._run,  # Changed to reference the correct function
        description="Retrieves detailed information about a specific DBpedia entity using its URI."
    )
]


# Initialize the Enhanced DBpedia Agent
agent = EnhancedDBpediaAgent(llm)

# Example usage
if __name__ == "__main__":
    question = "What is the capital of France, Colombia and Venezuela?"
    agentExecutor = agent.create_agent()
    result = agentExecutor.invoke({"question": question, "context": ""})
    print(f"\nQuestion: {question}")
    print(f"Answer: {result['output']}")
    print(f"Relevant Entities: {', '.join(result['relevant_entities'])}")

    # Visualize the knowledge graph (optional)
    # plot_graph(agent.knowledge_graph)
