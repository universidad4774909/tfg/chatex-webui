import logging
import networkx as nx
import json
from langchain.agents import AgentExecutor, create_react_agent, Tool
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
from langchain_core.output_parsers import CommaSeparatedListOutputParser
from langchain.schema import AgentAction, AgentFinish
from typing import Union
import re

from callbacks import CustomCallbackHandler
from dbpedia import DBpediaEntitySearchTool, DBpediaEntityInfoTool

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class EnhancedDBpediaAgent:
    def __init__(self, llm):
        self.knowledge_graph = nx.DiGraph()
        self.llm = llm
        self.property_filter_chain = self.create_property_filter_chain()

        # Initialize DBpedia tools
        self.dbpedia_search_tool = DBpediaEntitySearchTool(llm=self.llm)
        self.dbpedia_info_tool = DBpediaEntityInfoTool(llm=self.llm)

    def create_property_filter_chain(self):

        property_filter_prompt = PromptTemplate(
            input_variables=["property_names", "question"],
            template="""You are an AI assistant tasked with filtering relevant properties for a DBpedia entity based on a given question.

Available properties: {property_names}

Question: {question}

Your task is to select the properties that are most likely to be relevant in answering the question. Consider the following:
1. Which properties directly relate to the main topics or entities mentioned in the question?
2. Which properties might provide context or background information useful for answering the question?
3. Are there any properties that, while not directly mentioned, could provide valuable insights?
4. Never return a property that wasn't listed in available properties.

Please return a comma-separated list of the most relevant property names, without any additional explanation.
Please return the properties as they are given to you, including http:// for links and final slashes if they were included

Relevant properties:"""
        )

        output_parser = CommaSeparatedListOutputParser()

        return LLMChain(
            llm=self.llm,
            prompt=property_filter_prompt,
            output_parser=output_parser
        )

    def filter_properties(self, entity_info, question):
        property_names = list(entity_info.keys())
        relevant_property_names = self.property_filter_chain.run(
            property_names=", ".join(property_names),
            question=question
        )
        
        relevant_properties = {
            prop: value for prop, value in entity_info.items()
            if prop in relevant_property_names
        }
        
        if 'http://www.w3.org/2000/01/rdf-schema#label' in entity_info and 'http://www.w3.org/2000/01/rdf-schema#label' not in relevant_properties:
            relevant_properties['http://www.w3.org/2000/01/rdf-schema#label'] = entity_info['http://www.w3.org/2000/01/rdf-schema#label']
        
        return relevant_properties

    def update_knowledge_graph(self, entity, properties):
        self.knowledge_graph.add_node(entity)
        for prop, value in properties.items():
            self.knowledge_graph.add_edge(entity, value, label=prop)

    def get_graph_context(self):
        context = {}
        for node in self.knowledge_graph.nodes():
            context[node] = {
                prop: value
                for _, value, prop in self.knowledge_graph.out_edges(node, data='label')
            }
        return json.dumps(context, indent=2)

    def mark_relevant_nodes(self, relevant_entities):
        for entity in relevant_entities:
            if entity in self.knowledge_graph.nodes():
                self.knowledge_graph.nodes[entity]['relevant'] = True

    def create_agent(self):
        # Create Tool instances for DBpedia search and info tools
        tools = [
            Tool(
                name="DBpedia Entity Search",
                func=self.dbpedia_search_tool._run,  # Reference the correct method
                description="Search for relevant DBpedia entities based on a query."
            ),
            Tool(
                name="DBpedia Entity Info",
                func=self.dbpedia_info_tool._run,  # Reference the correct method
                description="Retrieve detailed information about a specific DBpedia entity."
            )
        ]
        
        template = """You are an expert agent in querying DBpedia. Your task is to answer questions using information from DBpedia.

        Current context (Knowledge Graph):
        {context}

        Question: {question}

        Available tools:
        - dbpedia_entity_search: Search for relevant DBpedia entities.
        - dbpedia_entity_info: Look up information associated with DBpedia entities.

        {tools} {tool_names}

        To answer, follow these steps:
        1. Identify what information you need to answer the question.
        2. Search for relevant DBpedia entities using the dbpedia_entity_search tool.
        3. Look up information associated with the found entities using the dbpedia_entity_info tool.
        4. Evaluate which concepts are relevant to the answer.
        5. If you have a factual explanation based on DBpedia's knowledge graph, provide a detailed response.
        6. If you need more information, continue searching and querying until you have enough to answer the question.


        After Action Input wait for the action to be completed, don't continue generating.
        No observation should be made before the Action is completed.
        You don't make observations. They will be given to you after the tool call is completed. Please stop generating after giving Action nput
        When not n
        Use the following format:

        Thought: Consider what to do next
        Action: The action to take (must be one of [ dbpedia_entity_search, dbpedia_entity_info])
        Action Input: The input for the action
        Observation: The result of the action
        ... (this Thought/Action/Action Input/Observation process can repeat N times but one step per query)
        Thought: I now have the information needed to answer the question
        Final Answer: The final answer to the user's question

        Examples:

        Begin:
        <begin-llm>
        Thought: I need to find out what professions Nicholas Ray and Elia Kazan are known for. I'll start with Nicholas Ray.
        Action: dbpedia_entity_search
        Action Input: {{
        "query": "Nicholas Ray",
        "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
        }}
        <end-llm>

        <begin-llm>
        Thought: Now I need to find information about Elia Kazan.
        Action: dbpedia_entity_search
        Action Input: {{
        "query": "Elia Kazan",
        "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
        }}
        <end-llm>

        <begin-llm>
        Thought: The filtered search has provided relevant information about both Nicholas Ray and Elia Kazan in relation to their professions. I should now look up more detailed information about Nicholas Ray to confirm his profession.
        Action: dbpedia_entity_info
        Action Input: {{
            "uri": "http://dbpedia.org/resource/Nicholas_Ray"
            "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
        }}
        <end-llm>

        <begin-llm>
        Thought: Now I'll get detailed information about Elia Kazan to confirm his profession as well.
        Action: dbpedia_entity_info
        Action Input: {{
            "uri": "http://dbpedia.org/resource/Elia_Kazan"
            "main_question": "What profession do Nicholas Ray and Elia Kazan have in common?"
        }}
        <end-llm>

        <begin-llm>
        Thought: I now have the information needed to answer the question
        Final Answer: The profession that Nicholas Ray and Elia Kazan have in common is film director.
        <end-llm>

        Begin:

        Thought: {agent_scratchpad}"""

        prompt = PromptTemplate(
            template=template,
            input_variables=["context", "question", "agent_scratchpad", "tools", "tool_names"],
            input_types={
                "context": str,
                "question": str,
                "agent_scratchpad": str,
            }
        )

        agent = create_react_agent(
            llm=self.llm,
            tools=tools,
            prompt=prompt,
            output_parser=self.parse_output,
            stop_sequence=True
        )

        return AgentExecutor.from_agent_and_tools(agent=agent, tools=tools, callbacks=[CustomCallbackHandler()], verbose=True, return_intermediate_steps=True, early_stopping_method='generate')

    def parse_output(self, llm_output: str) -> Union[AgentAction, AgentFinish]:
        logger.debug(f"Parsing LLM output:\n{llm_output}")

        if "Final Answer:" in llm_output:
            logger.info("Final Answer detected")
            final_answer = llm_output.split("Final Answer:")[-1].strip()
            relevant_entities = []
            if "Relevant Entities:" in final_answer:
                answer, entities = final_answer.split("Relevant Entities:")
                relevant_entities = [e.strip() for e in entities.split(',')]
                self.mark_relevant_nodes(relevant_entities)
                logger.debug(f"Relevant entities: {relevant_entities}")
            else:
                answer = final_answer
                logger.debug("No relevant entities found")
            
            logger.info(f"Returning AgentFinish with answer: {answer[:50]}...")
            return AgentFinish(
                return_values={"output": answer, "relevant_entities": relevant_entities},
                log=llm_output,
            )
        
        regex = r"Action: (\w+)\nAction Input: (.*)"
        match = re.search(regex, llm_output, re.DOTALL)
        
        if match:
            action = match.group(1)
            action_input = match.group(2)
            logger.info(f"Action detected: {action}")
            logger.debug(f"Action input: {action_input[:50]}...")
            return AgentAction(tool=action, tool_input=action_input.strip(), log=llm_output)
        
        logger.warning("Unable to determine the next action")
        logger.debug("LLM output did not match expected patterns")
        return AgentFinish(
            return_values={"output": "Unable to determine the next action."},
            log=llm_output,
        )

    def run(self, question: str, initial_context: str = ""):
        callback_handler = CustomCallbackHandler()
        agent_executor = self.create_agent()
        result = agent_executor.invoke({
            "question": question,
            'context': self.get_graph_context() if self.knowledge_graph.number_of_nodes() > 0 else initial_context
        }, config={"callbacks": [callback_handler]})
        
        if len(result['intermediate_steps']) > 0 and 'DBpediaEntityInfo' in result['intermediate_steps'][-1][0].tool:
            entity_info = result['intermediate_steps'][-1][1]
            filtered_properties = self.filter_properties(entity_info, question)
            self.update_knowledge_graph(entity_info['label'], filtered_properties)
        
        return result

    def run_tool(self, tool_name, tool_input):
        if tool_name == 'DBpediaEntitySearch':
            return self.dbpedia_search_tool._run(tool_input)
        elif tool_name == 'DBpediaEntityInfo':
            return self.dbpedia_info_tool._run(tool_input)
        else:
            raise ValueError(f"Unknown tool: {tool_name}")
