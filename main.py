import streamlit as st

from Pages_.chatbot import YourDataChat
from Pages_.dbPedia import DBpediaQueryPage
from Pages_.filterGraph import MathematicalAnalyzer
from Pages_.home import HomePage
from Pages_.knowledge_base import FileUploader
from Pages_.processFile import FileProcesser
from Pages_.processTask import TaskProcesser
from Pages_.entries import EntryPage
from Pages_.viewGraph import RDFVisualizer
import debugpy
import streamlit as st

if "execution" not in st.session_state:
    st.session_state["execution"] = False

if not debugpy.is_client_connected():
    debugpy.listen(5679)
    debugpy.wait_for_client()


PAGES = ["🏠 Home", "📁 Populate Knowledge-Base", "🔥 Chatex", "🕸️ Visualize Graph", "🏷️ Labels", "🕸️ Knowledge Queries", "🌐 DBpedia Query", "AgentProcessing", "Process File", "Process Task", "Populate"]

def main():
    st.sidebar.title("Pages")
    pageSelect = st.sidebar.selectbox("", PAGES[:7])  # Include DBpedia Query in sidebar
    if "page" in st.query_params and st.query_params["page"] in PAGES:
        page = st.query_params["page"]
    else: 
        page = pageSelect
        
    if page == "🏠 Home":
        HomePage().start()

    elif page == "📁 Populate Knowledge-Base" or page == "Populate":
        file_uploader = FileUploader()
        file_uploader.search_run()

    elif page == "Process File":
        file_processer = FileProcesser()
        file_processer.processFile()

    elif page == "🏷️ Labels":
        entries = EntryPage("labels")
        entries.run()

    elif page == "🕸️ Visualize Graph":
        visualizer = RDFVisualizer()
        consulta_input = visualizer.represent_graph()

    elif page == "🕸️ Knowledge Queries":
        analyzer = MathematicalAnalyzer()
        consulta_input = analyzer.display_interface()

    elif page == "Process Task":
        if "config" in st.query_params:
            task_processer = TaskProcesser(st.query_params["config"])
            task_processer.processTask()
        else:
            st.error("No config provided")
        
    elif page == "🔥 Chatex":
        YourDataChat.how_to_gpt()

    elif page == "🌐 DBpedia Query":
        dbpedia_page = DBpediaQueryPage()
        dbpedia_page.render()

def main2():
    st.set_page_config(layout="wide")
    YourDataChat.how_to_gpt()


if __name__ == "__main__":
    main()
    # main2()
